# Lab 4 Skeleton
#
# Based on of_tutorial by James McCauley

from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt #import POX packet library

log = core.getLogger()

class Firewall (object):
  """
  A Firewall object is created for each switch that connects.
  A Connection object for that switch is passed to the __init__ function.
  """
  def __init__ (self, connection):
    # Keep track of the connection to the switch so that we can
    # send it messages!
    self.connection = connection

    # This binds our PacketIn event listener
    connection.addListeners(self)

  def do_firewall (self, packet, packet_in):
	# The code in here will be executed for every packet.
  	print("TESTING")
	if packet.find("arp"): #check if packet is ARP
		print packet
		print packet_in
		msg = of.ofp_flow_mod() #if ARP, begin to re-initialize flow table rules"""
		msg.match = of.ofp_match.from_packet(packet) #check if field msg matches packet
		msg.idle_timeout = 30
		msg.hard_timeout = 60
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD)) #if match, add flood to actions"""
		#msg = of.ofp_packet_out()
        	msg.data = packet_in
        	self.connection.send(msg)

	elif packet.find("icmp"):
		print packet
		print packet_in
		msg = of.ofp_flow_mod()
		msg.match = of.ofp_match.from_packet(packet)
		msg.idle_timeout = 30
		msg.hard_timeout = 60
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
		msg.data = packet_in
		self.connection.send(msg)

	else:
		print "DROPPED"
		msg = of.ofp_flow_mod()
		msg.match = of.ofp_match.from_packet(packet)
		msg.idle_timeout = 30
		msg.hard_timeout = 60
		self.connection.send(msg)

	"""if packet.type == packet.ARP_TYPE: #check for ARP protocol
		print packet #information
		print packet_in
		msg = of.ofp_flow_mod() #switch remembers modified flow table fields
		msg.match = of.ofp_match.from_packet(packet)
		#msg.match = of.ofp_match(dl_type = 0x0806, nw_proto = 1) #check for 
		msg.idle_timeout = 30
		msg.hard_timeout = 60
		#msg.priority = 40
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
		msg.priority = 40
		msg.data = packet_in
		self.connection.send(msg)
	elif packet.find(pkt.ICMP):
		print packet
		print packet_in
		msg1 = of.ofp_flow_mod()
		msg1.match = of.ofp_match.from_packet(packet)
		#msg1.match = of.ofp_match(dl_type = 0x0800, nw_proto = 1)
		msg1.idle_timeout = 30
		msg1.hard_timeout = 60
		#msg1.priority = 50
		msg1.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
		msg1.priority = 50
		msg1.data = packet_in
		self.connection.send(msg1)
	elif packet.find(pkt.ICMP) == None:
		print "DROPPED"
		msg2 = of.ofp_flow_mod()
		msg2.match = of.ofp_match.from_packet(packet)
		#msg2.match = of.ofp_match(dl_type = 0x0800, nw_proto = 6)
		msg2.idle_timeout = 30
		msg2.hard_timeout = 60
		msg2.priority = 30
		self.connection.send(msg2)"""

  def _handle_PacketIn (self, event):
    """
    Handles packet in messages from the switch.
    """

    packet = event.parsed # This is the parsed packet data.
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return

    packet_in = event.ofp # The actual ofp_packet_in message.
    self.do_firewall(packet, packet_in)

def launch ():
  """
  Starts the component
  """
  def start_switch (event):
    log.debug("Controlling %s" % (event.connection,))
    Firewall(event.connection)
  core.openflow.addListenerByName("ConnectionUp", start_switch)
