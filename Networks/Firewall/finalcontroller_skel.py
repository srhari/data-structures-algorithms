# Final Skeleton
#
# Hints/Reminders from Lab 4:
# 
# To send an OpenFlow Message telling a switch to send packets out a
# port, do the following, replacing <PORT> with the port number the 
# switch should send the packets out:
#
#    msg = of.ofp_flow_mod()
#    msg.match = of.ofp_match.from_packet(packet)
#    msg.idle_timeout = 30
#    msg.hard_timeout = 30
#
#    msg.actions.append(of.ofp_action_output(port = <PORT>))
#    msg.data = packet_in
#    self.connection.send(msg)
#
# To drop packets, simply omit the action.
#

from pox.core import core
import pox.openflow.libopenflow_01 as of

log = core.getLogger()

class Final (object):
  """
  A Firewall object is created for each switch that connects.
  A Connection object for that switch is passed to the __init__ function.
  """
  def __init__ (self, connection):
    # Keep track of the connection to the switch so that we can
    # send it messages!
    self.connection = connection

    # This binds our PacketIn event listener
    connection.addListeners(self)

  def do_final (self, packet, packet_in, port_on_switch, switch_id):
	# This is where you'll put your code. The following modifications have 
	# been made from Lab 4:
	#   - port_on_switch represents the port that the packet was received on.
	#   - switch_id represents the id of the switch that received the packet
	#      (for example, s1 would have switch_id == 1, s2 would have switch_id == 2, etc...)
	# All switches are to be connected to every other switch (all 3 hosts should be able to connect to each other)
	# Flood all non-IP traffic
	# All IP-traffic should have specified ports (source + destination ports based on source + destination IP)
	# Block all IP and ICMP traffic from the untrusted host to the server
	#print "Hello, World!"
	#if packet is not IP or ICMP, flood it
	ipv4 = packet.find('ipv4')
	icmp = packet.find('icmp')
	#check for IP packets
	if ipv4:
		print packet
		print ipv4.protocol
		print ipv4.srcip
		print ipv4.dstip
		if ipv4.protocol == 1: #icmp check
			if ipv4.srcip == '172.16.10.100': #block from untrusted
				print "ICMP DROPPED"
				msg = of.ofp_flow_mod()
				msg.match = of.ofp_match.from_packet(packet)
				msg.idle_timeout = 60
				msg.hard_timeout = 60
				self.connection.send(msg)
			else: #icmp not from untrusted, route it through ports
				if switch_id != 5:			
					if port_on_switch == 9:		
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 8))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					else:
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 9))
						msg.data = packet_in
						self.connection.send(msg)
				elif switch_id == 5:
					if ipv4.dstip == '10.0.1.10':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 1))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '10.0.2.20':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 2))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '10.0.3.30':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 3))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '10.0.4.10':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 4))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '172.16.10.100':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 2))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection	
		else: #ip but not icmp
			if ipv4.srcip == '172.16.10.100' and ipv4.dstip == '10.0.4.10': #block connection from h5 to h4
				print "IPv4 DROPPED"
				msg = of.ofp_flow_mod()
				msg.match = of.ofp_match.from_packet(packet)
				msg.idle_timeout = 60
				msg.hard_timeout = 60
				self.connection.send(msg)
			else: #same as ICMP
				if switch_id != 5:			
					if port_on_switch == 9:		
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 8))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					else:
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 9))
						msg.data = packet_in
						self.connection.send(msg)
				elif switch_id == 5:
					if ipv4.dstip == '10.0.1.10':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 1))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '10.0.2.20':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 2))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '10.0.3.30':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 3))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '10.0.4.10':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 4))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
					if ipv4.dstip == '172.16.10.100':
						msg = of.ofp_flow_mod()
						msg.match = of.ofp_match.from_packet(packet)
						msg.idle_timeout = 30 #timeouts
						msg.hard_timeout = 60
						msg.actions.append(of.ofp_action_output(port = 5))
						msg.data = packet_in #send with data
						self.connection.send(msg) #send through connection
	else: #not IP, simply flood. 
		print "FLOODING"
		print packet
		msg = of.ofp_flow_mod()
		msg.match = of.ofp_match.from_packet(packet)
		msg.idle_timeout = 60
		msg.hard_timeout = 60
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
		msg.data = packet_in
		self.connection.send(msg)
	""" 
	if ipv4 is None:
		print "FLOODING"
		#print packet.type
		msg = of.ofp_flow_mod() #modify flow table
		msg.match = of.ofp_match.from_packet(packet) #check for packet match
		msg.idle_timeout = 30 #timeouts
		msg.hard_timeout = 60
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD)) #actions = flood
		msg.data = packet_in #send with data
		self.connection.send(msg) #send through connection
	else:
		print "IP"#drop untrusted ICMPs from/to untrusted host
		if (icmp) and (ipv4.srcip is "172.16.10.100" or ipv4.dstip is "172.16.10.100"):
			print "Untrusted icmp DROPPED"
			msg = of.ofp_flow_mod()
			msg.match = of.ofp_match.from_packet(packet)
			msg.idle_timeout = 30
			msg.hard_timeout = 60
			self.connection.send(msg)
		#drop untrusted IPs(non-ICMPs) untrusted host <---> server
		if (not icmp) and (ipv4.srcip is "172.16.10.100" or ipv4.srcip is "10.0.4.10") and (ipv4.dstip is "172.16.10.100" or ipv4.dstip is "10.0.4.10"): 
			print "Untrusted ip DROPPED"
			#print packet.type
			msg = of.ofp_flow_mod() #modify flow table
			msg.match = of.ofp_match.from_packet(packet) #check for packet match
			msg.idle_timeout = 30 #timeouts
			msg.hard_timeout = 60
			#msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD)) #actions = flood
			#msg.data = packet_in #send with data
			self.connection.send(msg) #send through connection
		#IP (ICMP-inclusive) packets not on the core switch
		elif switch_id is not 5:
			msg = of.ofp_flow_mod()
			msg.idle_timeout = 30 #timeouts
			msg.hard_timeout = 60
			msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
			msg.data = packet_in #send with data
			self.connection.send(msg) #send through connection
		else:
			msg = of.ofp_flow_mod()
			msg.idle_timeout = 30
			msg.hard_timeout = 60
			if ipv4.dstip is "10.0.1.10":
				msg.actions.append(of.ofp_action_output(port = 1))
			if ipv4.dstip is "10.0.2.20":
				msg.actions.append(of.ofp_action_output(port = 2))
			if ipv4.dstip is "10.0.3.30":
				msg.actions.append(of.ofp_action_output(port = 3))
			if ipv4.dstip is "10.0.4.10":
				msg.actions.append(of.ofp_action_output(port = 4))
			if ipv4.dstip is "172.16.10.100":
				msg.actions.append(of.ofp_action_output(port = 5))
			msg.data = packet_in
			self.connection.send(msg)
	ipv4 = packet.find('ipv4')
	icmp = packet.payload.find('icmp')

	if ipv4 is None:# and packet.find("icmp") is None:
		print "FLOODING"
		print packet.type
		msg = of.ofp_flow_mod() #modify flow table
		msg.match = of.ofp_match.from_packet(packet) #check for packet match
		msg.idle_timeout = 30 #timeouts
		msg.hard_timeout = 60
		msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD)) #actions = flood
		msg.data = packet_in #send with data
		self.connection.send(msg) #send through connection
	else:
		if icmp:
    		#if ipv4.protocol is ICMP_PROTOCOL:
			print "found icmp!"
			print packet.type
			#msg = of.ofp_flow_mod()
			#msg.match = of.ofp.match.from_packet(packet)
			if ipv4.srcip is "172.16.10.100/24" or ipv4.dstip is "172.16.10/100/24": #icmp source or destination = untrusted host, dropped
				msg = of.ofp_flow_mod()
				msg.match = of.ofp.match.from_packet(packet)
				msg.idle_timeout = 30
				msg.hard_timeout = 60
				self.connection.send(msg)
			elif switch_id is not 5: 
			 #and switch_id is 1:
				msg = of.ofp_flow_mod()
				msg.idle_timeout = 30 #timeouts
				msg.hard_timeout = 60
				msg.actions.append(of.ofp_action_output(port = of.OFPP_ALL))
				msg.data = packet_in #send with data
				self.connection.send(msg) #send through connection
				else:	
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 8))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
        		#if on core switch, route packet to appropriate destination
			elif switch_id is 5: 
				if ipv4.dstip is "10.0.1.10/24":
					m#sg = of.ofp_packet_out(in_port = 
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 5))	
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
				if ipv4.dstip is "10.0.2.20/24":
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 2))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
				if ipv4.dstip is "10.0.3.30/24":
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 3))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
				if ipv4.dstip is "10.0.4.10/24":
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 4))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
		#if not core switch, route packet a certain direction		 
			else: 
			 #and switch_id is 1:
				msg = of.ofp_flow_mod()
				msg.idle_timeout = 30 #timeouts
				msg.hard_timeout = 60
				msg.actions.append(of.ofp_action_output(port = of.OFPP_ALL))
				msg.data = packet_in #send with data
				self.connection.send(msg) #send through connection
				else:	
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 8))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
    #if packet is IP
		else:
			print "found ip!"
			print packet.type
			#print ip.srcip
			#msg = of.ofp_flow_mod()
			#if switch_id = 4 and port_on_switch = 4: #if origin is untrusted host
			#msg.match = of.ofp_match.from_packet(packet)
			if ipv4.srcip is ("172.16.10.100/24" or "10.0.4.10/24") and ipv4.dstip is ("10.0.4.10/24" or "172.16.10.100/24"): #untrusted <--> server, DROP
				msg = of.ofp_flow_mod()
				msg.match = of.ofp.match.from_packet(packet)
				msg.idle_timeout = 10
				msg.hard_timeout = 60
				self.connection.send(msg)
			
			#check source ip, destination ip, source port. route packet to correct destination port
			elif switch_id is not 5:
				msg = of.ofp_flow_mod()
				msg.idle_timeout = 30 #timeouts
				msg.hard_timeout = 60
				msg.actions.append(of.ofp_action_output(port = of.OFPP_ALL))
				msg.data = packet_in #send with data
				self.connection.send(msg) #send through connection
				else:	
					msg - of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 8))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
			#same logic as ICMP packets from here on
			elif switch_id is 5:
				if ipv4.dstip is "10.0.1.10/24":
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 5))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
				if ipv4.dstip is "10.0.2.20/24":
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 2))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
				if ipv4.dstip is "10.0.3.30/24":
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 3))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
				if ipv4.dstip is "10.0.4.10/24":
					msg = of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 4))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection
		 
			#check source ip, destination ip, source port. route packet to correct destination port
			else:
				msg = of.ofp_flow_mod()
				msg.idle_timeout = 30 #timeouts
				msg.hard_timeout = 60
				msg.actions.append(of.ofp_action_output(port = of.OFPP_ALL))
				msg.data = packet_in #send with data
				self.connection.send(msg) #send through connection
				else:	
					msg - of.ofp_flow_mod()
					msg.idle_timeout = 30 #timeouts
					msg.hard_timeout = 60
					msg.actions.append(of.ofp_action_output(port = 8))
					msg.data = packet_in #send with data
					self.connection.send(msg) #send through connection"""

  def _handle_PacketIn (self, event):
    """
    Handles packet in messages from the switch.
    """
    packet = event.parsed # This is the parsed packet data.
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return

    packet_in = event.ofp # The actual ofp_packet_in message.
    self.do_final(packet, packet_in, event.port, event.dpid)

def launch ():
  """
  Starts the component
  """
  def start_switch (event):
    log.debug("Controlling %s" % (event.connection,))
    Final(event.connection)
  core.openflow.addListenerByName("ConnectionUp", start_switch)
