import java.util.Arrays;

class Strings {
	public static void main(String[] args) {
		int a[] = {4,2,3,1};
	}
	
	// checks if string b is a rotation of string a, using only one call to isSubstring. isSubstring method provided.
	static boolean isRotation(String a, String b) {
		if (a.equals(b)) return true;
		String compound = b + b;
		//if (isSubstring(compound, a)) return true;
		return false;
	}
	
	// compresses the string, or if the compressed string is larger than the original, return the original.
	// Ex. aaabcccccddeee --> a3b1c5d2e3
	static String stringCompression(String s) {
		
		// counting sort on the string
		char[] sChar = s.toCharArray();
		int[] ascii = new int[52];
		char current = sChar[0];
		int currentCount = 0;
		String sb = "";
		for (int i = 0; i < sChar.length; i++) {
			if (sChar[i] == current) currentCount++;
			else {
				sb += (String.valueOf(current) + String.valueOf(currentCount));
				current = sChar[i];
				currentCount = 1;
			}
		}
		sb += (String.valueOf(current) + String.valueOf(currentCount));
		
		if (sb.length() <= s.length()) return sb;
		else return s;	
	}
	
	// 3 kinds of edits: insert, remove, replace. Check if two strings are one edit away from each other. Case sensitive!
	static boolean oneAway(String a, String b) {
		// convert both to char arrays
		char[] aString = a.toCharArray();
		char[] bString = b.toCharArray();
		
		// counting sort on the first string, then iterate through second string and substract
		int[] ascii = new int[128];
		for (int i = 0; i < aString.length; i++) ascii[(int)aString[i]]++;
		for (int i = 0; i < bString.length; i++) {
			ascii[(int)bString[i]]--;
			if (ascii[(int)bString[i]] < -1) return false; // b string is more than one insert off.
		}
				
		int oneCount = 0, negOneCount = 0;
		for (int i = 0; i < ascii.length; i++) {
			if (ascii[i] > 1) return false;
			if (ascii[i] == 1) {
				if (oneCount == 1) return false;
				else oneCount++;
			} else if (ascii[i] == -1) {
				if (negOneCount == 1) return false;
				else negOneCount++;
			}
		}
		return true;
	}
	
	// Given a string, check if it is a permutation of a palindrome.
	static boolean isPalindromePermutation(String s) {
		// (EVEN length string): even count of all unique characters means the string can be rearranged into a palindrome.
		// (ODD length string): even count of all but one unique characters, and odd count of one means the string can be rearranged into a palindrome.
		
		char[] charS = s.toCharArray(); // convert string to character array
		char[] charCount = new char[102]; // 128 - 26 (upper and lower case letters shared)
		
		
		// counting sort on string, upper and lower case will hash to the same locations.
		for (int i = 0; i < charS.length; i++) {
			if (Character.isLetter(charS[i])) {
				if (Character.isUpperCase(charS[i])) charCount[(int)charS[i]]++; // locations 65-90
				if (Character.isLowerCase(charS[i])) charCount[(int)charS[i]-32]++; // locations 65-90
			} else {
				if ((int)charS[i] > 102) charCount[(int)charS[i]-26]++; // locations 123-127 hashed to 97-101
				else charCount[(int)charS[i]]++; // less than 96, can hash normally
			}
			
		}
		
		// even length string OR odd length string?
		if (s.length() % 2 == 0) {
			for (int i = 0; i < charCount.length; i++) if (charCount[i] % 2 != 0) return false;
			return true;
		} else {
			int counter = 0;
			for (int i = 0; i < charCount.length; i++) {
				if (charCount[i] % 2 != 0) {
					counter++;
					if (counter > 1) return false;
				}
			}
			return true;
		}
	}
	
	// Given a string, replace spaces with "%20" (given "true" length of string without extra space at end)
	static String URLify(String s, int length) {
		int spaceCount = 0;
		char[] charS = s.toCharArray(); // convert to char array
		for (int i = 0; i < length; i++) if (charS[i] == ' ') spaceCount++; // count spaces
		int falseLength = length+spaceCount*3; // calculate expanded length
		charS[falseLength] = '\0';
		for (int i = length; i > 0; i--) {
			if (charS[i] != ' ') {
				charS[falseLength - 1] = charS[i];
				falseLength -= 1;
			} else {
				charS[falseLength - 1] = '0';
				charS[falseLength - 2] = '2';
				charS[falseLength - 3] = '%';
				falseLength -= 3;
			}
			
		}
		return charS.toString();
		
	}
	
	// Given two strings, check if one string is a permutation of the other.
	static boolean checkPermutations(String a, String b) {
		if (a.length() != b.length()) return false;
		int aCount[] = new int[128]; //create an array of 128 possible characters to store a's characters
		
		// apply a counting sort on String a
		for (int i = 0; i < a.length(); i++) aCount[a.charAt(i)]++;
		
		// iterate through String b and check for matching character counts
		for (int i = 0; i < b.length(); i++) {
			int c = (int) b.charAt(i);
			aCount[c]--;
			if (aCount[c] < 0) return false;
		}
		return true;
	}
	
	// Given a string, return its reverse.
	static String reverseString(String s) {
		int i = 0, j = s.length()-1;
		StringBuilder sb = new StringBuilder(s);
				
		while (i < j) {
			sb.setCharAt(i, s.charAt(j)); sb.setCharAt(j, s.charAt(i));
			i++; j--;
		}
				
		return sb.toString();
	}
	
}