import java.util.*;

class MoreStrings extends Strings {
	public static void main(String[] args) {
		//System.out.println(addBinary("0100", "01"));
		//String[] array = {"abab", "aba", "abc"};
		//System.out.println(longestCommonPrefix(array));
		System.out.println("REVERSED STRING: " + reverseWords("  hello  there. "));
	}
	
	// given two binary strings, return their sum.
	static String addBinary(String a, String b) {
		if ((a.equals("") || a.equals(null)) && (!b.equals("") || !b.equals(null))) return b;
		else if ((b.equals("") || b.equals(null)) && (!a.equals("") || !a.equals(null))) return a;
		else if ((a.equals("") || a.equals(null)) && (b.equals("") || b.equals(null))) return "";
		else {
			char[] result = new char[Math.max(a.length(), b.length())];
			int carryBit = 0;
			for (int i = a.length()-1, j = b.length()-1; i >= 0 || j >= 0; i--, j--) {
				int c1 = i < 0 ? 0 : (a.charAt(i) - '0');
				int c2 = j < 0 ? 0 : (b.charAt(j) - '0');
				result[Math.max(i, j)] = (char) ((c1 + c2 + carryBit) % 2 + (int) '0');

				if ((c1 + c2 + carryBit) / 2 > 0) carryBit = 1;
				else carryBit = 0;
			}
			return (carryBit > 0 ? "1" : "") + new String(result);
		}
	}
	
	static String longestCommonPrefix(String[] strs) {
			
		if (strs.length == 0) return "";
		if (strs.length == 1) return strs[0];
		
		// find the shortest of the strings
		String shortest = strs[0];
		for (int i = 0; i < strs.length; i++) {
			if (strs[i].equals("")) return "";
			if (strs[i].length() < shortest.length()) shortest = strs[i];
		}
		System.out.println("SHORTEST: " + shortest);
		
		// low and high are dependent on the shortest string
		int low = 0, high = shortest.length();
		String prefix = "";
					
		// call binary search function
		return longestCommonPrefixBuilder(strs, shortest, prefix, low, high);
		
		
	}
		
	static String longestCommonPrefixBuilder(String[] strs, String s, String prefix, int low, int high) {
		
		int mid = (low+high)/2;
		System.out.println("LOW: " + low + " MID: " + mid + " HIGH: " + high);
		
		//check if the current string is a substring of all the strings.
		for (int i = 0; i < strs.length; i++) {
			// if substring's length is 1 and all substrings don't match, return ""
			if (!(s.substring(low, high).equals(strs[i].substring(low, high))) && s.substring(low, high).length() == 1) {
				System.out.println("LENGTH 1, NOT A SUBSTRING --> \"\"");
				return "";
			// if substring's length is > 1 and all substrings don't match, check with the left half of the substring.
			} else if (!s.substring(low, high).equals(strs[i].substring(low, high))) {
				System.out.println("CALLING ON LEFT HALF.");
				return longestCommonPrefixBuilder(strs, s, prefix, low, mid);
			}
		}
		// all matches clears for loop. 
		System.out.println("ALL MATCHES.");
		// if the substring == original string, no need to continue. return the prefix + substring.
		if (s.substring(low, high).length() == s.length()) {
			System.out.println("SUBSTRING LENGTH = S LENGTH: RETURN");
			return prefix + s.substring(low, high);
		// if substring.length < string.length, there could be possible matches on the right half of the original string.
		// add the matching substring to the prefix and then recursively call on the right half of the original string.
		} else return prefix + s.substring(low, high) + longestCommonPrefixBuilder(strs, s, prefix, high, s.length());
	}
	
	// intakes a string and reverses the order of the words, but preserves the order of the letters in each word.
	static String reverseWordOrder(String s) {
		if (s.equals(" ") || s.equals("")) return s;
		s = s.trim().replaceAll(" +", " ");
		String[] words = s.split(" "); StringBuilder sb = new StringBuilder();
		arrayPrint(words); System.out.println("ARRAY LENGTH: " + words.length);
		for (int i = words.length-1; i >= 0; i--) {
			sb.append(words[i] + " ");
		}
		return sb.toString().trim();
		//return result.substring(0,result.length()-1);
	}
	
	// intakes a string and preserves the order of the words, but reverses the order of the letters in each word.
	static String reverseWords(String s) {
		if (s.equals(" ") || s.equals("")) return s;
		// s = s.trim().replaceAll(" +", " ");
		String[] words = s.split(" "); StringBuilder sb = new StringBuilder();
		for (int i = 0; i < words.length; i++) sb.append(reverseString(words[i]) + " ");
		return sb.toString().trim();
	}
	
	static void arrayPrint(String[] s) {
		for (int i = 0; i < s.length; i++) System.out.print(s[i] + "::");
		System.out.println();
	}
}