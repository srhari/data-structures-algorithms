import java.util.ArrayList;

class Matrix {
	public static void main(String[] args) {
		/*int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
		printMatrix(matrix);
		int[] a = printSpiral(matrix);
		for (int i = 0; i < a.length; i++) System.out.print(a[i] + " ");
		System.out.println();*/
		ArrayList<ArrayList<Integer>> a = pascalMatrix(15);
		printArrayList(a);
	}
		
	static void printCharArray(int[] a) {
		for (int i = 0; i < a.length; i++) System.out.print(a[i]);
		System.out.println("\n");
	}
	
	// if an element in a matrix is 0, its entire row and column are set to 0.
	static int[][] zeroMatrix(int[][] matrix) {
		// mark locations with 0 in the matrix in first pass
		// in second iteration, use locations to clear rows and columns to 0s
		int xDim = matrix.length, yDim = matrix[0].length;
		int[] x = new int[xDim];
		int[] y = new int[yDim];
		
		for (int i = 0; i < xDim; i++) {
			for (int j = 0; j < yDim; j++) {
				if (matrix[i][j] == 0) {
					y[j]++;
					x[i]++;
				}
			}
		}
		
		for (int i = 0; i < x.length; i++) if (x[i] != 0) matrix[i] = new int[yDim];
		for (int j = 0; j < y.length; j++) if (y[j] != 0) for (int k = 0; k < xDim; k++) matrix[k][j] = 0;
		return matrix;
	}
	
	// rotate a matrix 90 degrees
	static int[][] rotate90(int[][] matrix) {
		int layer = matrix.length/2, last = matrix.length-1, first = 0;
		while (layer > 0) {
			for (int i = first; i < last; i++) {
				int temp = matrix[first][i], offset = i - first;
				matrix[first][i] = matrix[last-offset][first];
				matrix[last-offset][first] = matrix[last][last-offset];
				matrix[last][last-offset] = matrix[i][last];
				matrix[i][last] = temp;
			}
			first++;
			last-=2;
			layer--;
		}
		return matrix;
	}
	
	static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[i][j]);
				System.out.print(" ");
			}
			System.out.println();
		}
	}
	
	static void printArrayList(ArrayList<ArrayList<Integer>> aList) {
		for (int i = 0; i < aList.size(); i++) {
			System.out.println(aList.get(i).toString());
		}
	}
	
	// diagonally iterate through a matrix
	static int[] printDiagonal(int[][] matrix) {
		// create an array of length (mxn)
		int[] array = new int[matrix.length * matrix[0].length];
				
		int x = 0, y = 0, i = 0, flag = 1;
		int xhop = 1, yhop = 1;
		
		while (i < array.length) {
			array[i] = matrix[x][y];
			i++;
			if (x == 0 || y == matrix[0].length-1) { // determine if a hop is necessary
				x = xhop;
				y = 0;
				if (xhop <= matrix.length-1) xhop++; // the next hop will be further down if within dimensions
				else {
					y = yhop; // yhop
					x = matrix.length-1;
					if (yhop < matrix[0].length-1) yhop++;
				}
			} else { // no hop --> proceed diagonally upward or downward
				x--;
				y++;
			}
		}
		return array;
	}
	
	// zig zag iterate through a matrix
	static int[] printZigZag(int[][] matrix) {
		
		// create an array of length mxn
		int[] array = new int[matrix.length * matrix[0].length];
		int i = 0, x = 0, y = 0, direction = 1;
		
		while (i < array.length) {
			array[i] = matrix[x][y];
			i++;
			if (direction == 1) {
				if (x > 0 && y < matrix[0].length-1) {
					x--;
					y++;
					continue;
				}
			} else {
				if (y > 0 && x < matrix.length-1) {
					x++;
					y--;
					continue;
				}
			}
			if ((x == 0 && y < matrix[0].length-1) || (x == matrix.length-1 && y < matrix[0].length-1)) {
				y++; // jump right, switch direction
				direction *= -1;
			} else if ((x < matrix.length-1 && y == matrix[0].length-1) || (y == 0 && x < matrix.length-1)) {
				x++; // jump down, switch direction
				direction *= -1;
			}
		}
		return array;
	}
	
	// spiral iterate through matrix
	static int[] printSpiral(int[][] matrix) {
		
		// create an array of length mxn
		int[] array = new int[matrix.length * matrix[0].length];
		int i = 0, x = 0, y = 0, direction = 1;
		int yLow = 0, xLow = 0, yHigh = matrix[0].length-1, xHigh = matrix.length-1;
		
		
		while (i < array.length) {
			array[i] = matrix[x][y];
			i++;
			
			if (direction == 1) {
				if (y < yHigh) y++;
				else if (x < xHigh) x++;
				else {
					direction *= -1;
					xLow++;
					yHigh--;
				}
			} if (direction == -1) {
				if (y > yLow) y--;
				else if (x > xLow) x--;
				else {
					direction *= -1;
					xHigh--;
					yLow++;
					y++;
				}
			}
			
		}
		return array;
	}
	
	// given an integer, generate the first n rows of Pascal's triangle and return as an asymmetrical matrix.
	static ArrayList<ArrayList<Integer>> pascalMatrix(int n) {
		if (n < 1) {
			System.out.println("n must be at least one for a valid Pascal's triangle.");
			return new ArrayList<ArrayList<Integer>>();
		} else {
			if (n == 1) {
				ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>();
				ArrayList<Integer> a = new ArrayList<Integer>();
				a.add(1);
				matrix.add(a);
				return matrix;
			} else if (n == 2){
				ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>();
				ArrayList<Integer> a = new ArrayList<Integer>();
				ArrayList<Integer> b = new ArrayList<Integer>();
				a.add(1);
				matrix.add(a);
				b.add(1);
				b.add(1);
				matrix.add(b);
				return matrix;
			} else {
				// acquire resultant matrix from recursive call. create array to store new row, and initialize two variables to point to first two locations in last row in matrix
				ArrayList<ArrayList<Integer>> matrix = pascalMatrix(n-1);
				ArrayList<Integer> aList = new ArrayList<Integer>();
				ArrayList<Integer> lastList = matrix.get(matrix.size()-1);
				int y = 0;
				
				// populate edges of the array -- same from previous row
				for (int i = 0; i < lastList.size()+1; i++) {
					if (i == 0) aList.add(lastList.get(0));
					else if (i == lastList.size()) aList.add(lastList.get(lastList.size()-1));
					else {
						aList.add(lastList.get(y) + lastList.get(y+1));
						y++;
					}
				}
				
				// push the array onto the matrix and return the matrix
				matrix.add(aList);
				return matrix;
			}
		}
	}

}