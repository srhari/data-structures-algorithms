import java.util.*;

class Arrays {
	
	static final int MAX = 10000;
	public static void main(String[] args) {
		int a[] = {0,1,0,3,12};
		//arrayPairSum(a);
		//arrayPrint(twoSum(a, 17, 0, a.length-1));
		//arrayPrint(removeDuplicates(a));
		//arrayListPrint(getRow(10));
		moveZeroes(a);
		arrayPrint(a);
	}
	
	// Given an array of 2n integers, grouping the integers into pairs should result in the sum of min(ai, bi) for all i from 1 to n to be as large as possible.7
	// Solution: Sort the array using a merge sort algorithm. Then, add up every other number for the maximum sum.
	static int arrayPairSum(int[] nums) {
		int sum = 0;
		mergeSort(nums, 0, nums.length-1);
		for (int i = 0; i < nums.length; i++) { System.out.print(nums[i] + " "); } System.out.println();
		for (int i = 0; i < nums.length; i+=2) { sum += nums[i]; }
		System.out.println("SUM: " + sum);
		return sum;
	}
	
	static void mergeSort(int[] nums, int left, int right) {
		System.out.println("LEFT: " + left + " RIGHT: " + right);
		if (left >= right) return;
		int mid = (left+right)/2;
		mergeSort(nums, left, mid);
		mergeSort(nums, mid+1, right);
		merge(nums, left, right);
	}
	
	// intakes two arrays. merges them and returns the result.
	static void merge(int[] nums, int left, int right) {
		int i = 0, j = 0, k = left, mid = (left+right)/2;
		int[] leftArray = new int[(mid-left)+1];
		int[] rightArray = new int[right-mid];
		for (int a = 0; a < leftArray.length; a++) leftArray[a] = nums[left+a];
		for (int b = 0; b < rightArray.length; b++) rightArray[b] = nums[mid+b+1];
		System.out.print("LEFT ARRAY: "); arrayPrint(leftArray);
		System.out.print("RIGHT ARRAY: "); arrayPrint(rightArray);
		while (i < leftArray.length && j < rightArray.length) {
			if (leftArray[i] <= rightArray[j]) nums[k++] = leftArray[i++];
			else nums[k++] = rightArray[j++];
			System.out.println("k: " + k);
		}
		
		while (i < leftArray.length) { nums[k++] = leftArray[i++]; System.out.println("k: " + k); }
		while (j < rightArray.length) { nums[k++] = rightArray[j++]; System.out.println("k: " + k); }
		System.out.print("SORTED: "); arrayPrint(nums);
	}
	
	static void arrayPrint(int[] array) {
		for (int i = 0; i < array.length; i++) System.out.print(array[i] + " ");
		System.out.println();
	}
	
	static int sumArray(int[] array, int left, int right) {
		int sum = 0;
		for (int i = left; i <= right; i++) sum+=array[i];
		return sum;
	}
	
	// Given an array of sorted integers, find two numbers such that they add up to a specified number, and return their indexes as an array.
	static int[] twoSum(int[] numbers, int target, int left, int right) {
		int[] indexes = new int[2];
		if (left >= right) return indexes;
		if (numbers[left]+numbers[right] == target) { indexes[0] = left+1; indexes[1] = right+1; return indexes; }
		else if (numbers[left]+numbers[right] < target) return twoSum(numbers, target, left+1, right);
		else if (numbers[left]+numbers[right] > target) return twoSum(numbers, target, left, right-1);
		return indexes;
	}
	
	// Given an array of n positive integers and a positive integer s, find the minimal length of a contiguous subarray of which the sum ≥ s. If there isn't one, return 0 instead.
	static int minSubArrayLen(int s, int[] nums) {
		int ans = MAX, left = 0, sum = 0;
		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
			while (sum >= s) {
				ans = Math.min(ans, i+1-left);
				sum -= nums[left++];
			}
		}
		System.out.print(" sum: " + sum + " left: " + left);
		System.out.println();
		return (ans != MAX) ? ans : 0;
	}
	
	// rotate an array.
	static int[] rotate(int[] nums, int k) {
		if (k >= nums.length) k %= nums.length; // simplify rotation indexes > length of array
		if (k == 0) return nums;
		int pivot = nums.length-k, leftLength = pivot, rightLength = nums.length-pivot, i = 0;
		System.out.println("Pivot: " + pivot + ", leftLength: " + leftLength + ", rightLength: " + rightLength);
		
		// move the right sub array to the front and the left sub array to the rear. If both lengths are equal, this can be done by swapping the respective elements in the two arrays. If they are different lengths, it's a bit more complicated :)
		for ( ; i < leftLength && i < rightLength; i++) swap(nums,i,i+pivot);
		
		// case 1: both sides are of equal length. The initial swap would have finished the rotation.
		if (leftLength == rightLength) return nums;
		
		// case 2: any elements from i to rightStart need to go to the end
		if (leftLength > rightLength) {
			int h = i;
			while (h < pivot) {
				int pos = i;
				while (pos < nums.length-1) { swap(nums,pos,pos+1); pos++; }
				h++;
			}
		}
		
		// case 3: any elements from the right side that weren't swapped need to be inserted in the middle.
		if (leftLength < rightLength) {
			int j = nums.length-(rightLength-leftLength), l = leftLength;
			while (j < nums.length) {
				int pos = j;
				while (pos > l) { swap(nums,pos,pos-1); pos--; }
				j++; l++;
			}
		}
		return nums;
	}
	
	static void swap(int[] nums, int a, int b) {
		int temp = nums[b];
		nums[b] = nums[a];
		nums[a] = temp;
	}
	
	//Given a non-negative index k where k ≤ 33, return the kth index row of the Pascal's triangle.
	static List<Integer> getRow(int rowIndex) {
		if (rowIndex < 0 || rowIndex > 33) return new ArrayList<Integer>();
		int i = 0; List<Integer> lst = new ArrayList<Integer>();
		lst.add(1); i++;
		if (rowIndex == 0) return lst;
		lst.add(1); i++;
		if (rowIndex == 1) return lst;
		
		// while loop to track which pascal iteration the algorithm is currently calculating.
		while (i <= rowIndex) {
			System.out.println("PASCAL ITERATION: " + i);
			lst = constructPascalList(lst);
			i++;
		}
		return lst;
	}
	
	// intakes a list, then constucts the next pascal iteration list.
	static List<Integer> constructPascalList(List<Integer> lst) {
		int left = 0, right = 1, swapper = 1, s = lst.size();
		while (right < s) lst.add(lst.get(left++)+lst.get(right++));
		while (s < lst.size()) arrayListSwap(lst,swapper++,s++);
		arrayListSwap(lst,lst.size()-1,swapper);
		lst.subList(swapper+1, lst.size()).clear();
		return lst;
	}
	
	// swaps the values at two specified indexes in an array list.
	static void arrayListSwap(List<Integer> lst, int indexOne, int indexTwo) {
		int temp = lst.get(indexOne);
		lst.set(indexOne, lst.get(indexTwo));
		lst.set(indexTwo, temp);
	}
	
	static void arrayListPrint(List<Integer> lst) {
		for (int i = 0; i < lst.size(); i++) System.out.print(lst.get(i) + " ");
	}
	
	// remove duplicate values from a sorted array
	static int removeDuplicates(int[] nums) {
		int currentValue = nums[0], position = 1;
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] > currentValue) {
				nums[position] = nums[i];
				currentValue = nums[i];
				position++;
			}
		}
		return position;
	}
	
	// move all 0s to the end while maintaining the order of the non-0s, O(n^2) solution
	static void moveZeroes(int[] nums) {
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 0) {
				for (int j = i; j < nums.length; j++) {
					if (nums[j] != 0) { swap(nums,i,j); break; }
				}
			}
		}
	}
	
	// move all 0s to the end while maintaining the order of the non-0s, O(n) solution	
	static void moveZeroes(int[] nums) {
		int position = 0, i = 0;
		while (i < nums.length) { if (nums[i] != 0) nums[position++] = nums[i]; i++; }
		while (position < nums.length) nums[position++] = 0;
	}



}