#include <stdio.h>
#include <ctype.h>
#define MAXLINE 100
int main(int argc, char *argv[]) {
	
}

double atof(char s[]){ //convert string s to double
	double val, power;
	int i, sign;
	
	for (i=0; isspace(s[i]); i++) //skips leading white space
		;
	sign = (s[i] == '-')?-1: 1; //assigns sign
	if (s[i] == '+' || s[i] == '-') //increments starting point
		i++;
	for (val = 0.0; isdigit(s[i]); i++) //digit conversion
		val = 10.0 * val + (s[i] - '0');
	if (s[i] == '.') //increment if '.' detected
		i++;
	for (power = 1.0; isdigit(s[i]); i++){ //digit conversion, power set
		val = 10.0 * val + (s[i] - '0');
		power *= 10.0;
	}
	return sign * val / power;
}

double atof_extend(char s[]){
	double val, power;
	int i, sign, esign, exp;
	int power(int base, int exp);
	
	for (i=0; isspace(s[i]); i++) //skips leading white space
		;
	sign = (s[i] == '-')?-1: 1; //assigns sign
	if (s[i] == '+' || s[i] == '-') //increment starting point
		i++;
	for (val = 0.0; isdigit(s[i]); i++) //digit conversion
		val = 10.0 * val + (s[i] - '0');
	if (s[i] == '.') //increment if '.' detected
		i++;
	for (power = 1.0; isdigit(s[i]); i++){
		val = 10.0 * val + (s[i] - '0');
		power *= 10.0;
	}
	if (s[i] == 'e' || s[i] == 'E')
		i++;
	if (s[i] == '+' || s[i] == '-'){
		esign = s[i];
		i++;
	}
	for (exp = 0; isdigit(s[i]); i++)
		exp = 10.0 * exp + (s[i] - '0');
	if (esign == '-')
		return sign * (val / power) / power(10, exp);
	else
		return sign * (val / power) * power(10, exp);
}