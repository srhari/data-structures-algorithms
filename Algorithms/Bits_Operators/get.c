#include <stdio.h>
#include <ctype.h>

#define SIZE 1000 //constant

int getch(void);
void ungetch(int);

int main(void) {
	int n, s, array[SIZE], getint(int *), getfloat(float *); //declare variables, array, pointers
	float Array[SIZE];
	for (n = 0; n<SIZE && getint(&array[n]) != EOF; n++) //iterate through array, extract integers using pointer, increment n
		printf("storing in n = %d, getint %d\n", n, array[n]);
	for (m = 0; m<SIZE && getfloat(&Array[m]) != EOF; m++) //iterate through float array, extract floats using pointer, increment n
		;
	for (; m>=0; m--) //print elements of array, decrement n
		printf("%f", array[m]);
	printf("storing in n = %d, getint %d\n", n, array[n]); //final element int array
	for (s = 0; s<=n; s++)
		printf("%d", array[s]); //print all elements 
	return 0;
}

/* getint: get next integer from input into *pn */
int getint(int *pn){
	int c, sign;
	while (isspace(c = getch())) //do nothing while leading spaces detected
		;
	if (!isdigit(c) && c != EOF && c != '+' && c != '-'){ //until end of file, or digits read, make sure c is not '+' or '-' or a digit
		ungetch(c); //if it's not a number, ungetch it
		return 0;
	}
	
	sign = (c == '-') ? -1 : 1; //assign sign
	if (c == '+' || c == '-') //if '+' or '-' detected, getch it
		c = getch();
	for (*pn = 0; isdigit(c); c = getch()) //if digit detected, convert digit to character and getch it
		*pn = 10 * *pn + (c - '0');
	*pn *= sign; //sign
	if (c != EOF) //ungetch if end of file not reached
		ungetch(c);	
	return c; //return 
}

#define BUFSIZE 100

char buf[BUFSIZE];
int bufp = 0;

int getch(void){ 
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c){
	if (bufp >= BUFSIZE)
		printf("ungetch: too many characters\n");
	else
		buf[bufp++] = c;
}

/* getfloat: get next float from input *qn */
float getfloat(float *qn){
	int c, sign;
	
	while (isspace(c = getch())) //do nothing while leading spaces detected
		;
	if (!isdigit(c) && c != EOF && c != '+' && c != '-'){ //until end of file, or digits read, make sure c is not '+' or '-' or a digit
		ungetch(c); //if it's not a number, ungetch it
		return 0;
	}
	
	sign = (c == '-') ? -1 : 1; //assign sign
	if (c == '+' || c == '-') //if '+' or '-' detected, getch it
		c = getch();
	for (*qn = 0.0; isdigit(c); c = getch()) //if digit detected, convert digit to character and getch it
		*qn = 10.0 * *pn + (c - '0');
	if (c == '.')
		c = getch();
	
	for (power = 1.0; isdigit(c); c = getch())
		*pn = 10.0 * *pn + (c - '0');
		power *= 10.0;
	*pn *= sign / power; //sign
	if (c != EOF) //ungetch if end of file not reached
		ungetch(c);	
	return c; //return 	
}