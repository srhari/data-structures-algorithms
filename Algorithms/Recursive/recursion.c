#include <stdio.h>
#include <math.h>
#include <string.h>

#define MAXLINE 100

int mgetline(char line[], int maxline);
void itoa(int n, char s[]);
void reverse(char s[]);

int main(void) {
	int n = 1723;
	char s[MAXLINE];
	reverse(s);
	itoa(n, s);
	printf("%s", s);
	return 0;
}

int mgetline(char s[], int lim){
	int i, c;
	for (i = 0; i<lim-1 && (c=getchar()) != EOF && c!= '\n'; ++i) //iterate through line
		s[i] = c; //assign c as next read character in line
	if (c == '\n') //detect end of line, add \n and \0
		s[i++] = '\n';
	s[i] = '\0';
	
}
void itoa(int n, char s[]){ //recursive integer number to array of characters
	static int i;
	if (n/10) //if the number is divisible by 10, recurr on n/10
		itoa(n/10,s);
	else{ //if not
		i = 0;
		if (n<0) //if n is negative set a '-' character first
			s[i++] = '-';
	}
	s[i++] = abs(n) % 10 + '0'; //%10 for digit, add to '0' character to convert to character
	s[i] = '\0'; //end with \0
}

void reverse(char s[]){
	void reverser(char s[], int i, int len); //reverse simply calls reverser
	reverser(s, 0, strlen(s)); //use s[], start i at 0, provide length of array using strlen
}

void reverser(char s[], int i, int len){
	int c, j;
	j = len-(i+1); //jth position starts at end and increments toward middle of string
	
	if (i<j){ //as long as i has not crossed j
		c = s[i]; //swap
		s[i] = s[j];
		s[j] = c;
		reverser(s, ++i, len); //recurr on reverser, but increase i by 1 to swap the next two values
	}
}