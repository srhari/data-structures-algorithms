#include <stdio.h>
#include <string.h>

#define MAXLINE 100

void itob(int n, char s[], int b);
void reverse(char s[]);
int strindex(char s[], char t[]);
int str_index(char s[], char t[]);

int main(void){
	int number, base;
	char str[MAXLINE];
	number = 42425;
	base = 10;
	itob(number, str, base);
	printf("%s", str);
	return 0;
}

void itob(int n, char s[], int b){
	int i, j, sign;
	if ((sign = n)<0) //store number in sign
		n = -n;
	i = 0;
	
	do{ 
		j = n%b; //remainder = number/base
		s[i++] = (j<=9)?j+'0':j+'a'-10; //convert number to hexadecimal
	}while((n/=b)>0);
		
	if (sign<0) //append '-' if negative number
		s[i++]='-';
	s[i]='\0'; //add \0 to end of array
	reverse(s);	//reverse constructed string to get result
	
}

void reverse(char s[]){
	int i, j, c;
	for (i = 0, j = strlen(s)-1; i<j; i++, j--)
		c = s[i], s[i] = s[j], s[j] = c;
}

int strindex(char s[], char t[]){ //return position of first character in string with respect to entire string
	int i, j, k;
	for (i=0; s[i]!='\0'; i++){ //iterate through entire line s[]
		for (j=i, k=0; t[k]!='\0' && s[j]==t[k]; j++, k++) //check for end of string, check if position in line matches internal position in string array. iterate through string t[]
			;
		if (k>0 && t[k]=='\0') //if so, return the external position i if end of t[] detected
			return i;
	}
	return -1; //else return -1
	
}

int str_index(char s[], char t[]){ //return position of the rightmost occurrence of string in external string, or -1 if none
	int i, j, k, index = -1;
	for (i=0; s[i]!='\0'; i++){ //iterate through line s[]
		for (j=i, k=0; t[k]!='\0' && s[j]==t[k]; j++, k++)
			;
		if (k>0 && t[k]=='\0')
			index = i;
	}
	return index;
}