/* n sorting. */
/* counting sort, radix sort. */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>

int* array_gen(int);
int* counting_sort(int*, int, int);
void radix_sort(int*, int, int);
int* radii(int*, int);
int getMax(int*, int);
int len;
int* bucket_sort(int*, int);
int choose_bucket(int, int);

int main(int argc, char *argv[]) {
	int i, *a;
	clock_t t;
	a = array_gen(100); //set integer pointer as equivalent to returned pointer.
	printf("ARRAY LENGTH: %d\n", len);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);
	printf("\n\n");
	
	/*t = clock();
	counting_sort(a, 100, 100);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("COUNTING SORT in %f seconds: ", time_taken);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);*/
	
	/*t = clock();
	radii(a, 100);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("RADIX SORT in %f seconds: ", time_taken);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);*/
	
	t = clock();
	bucket_sort(a, 100);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("BUCKET SORT in %f seconds: ", time_taken);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);
	
	free(a);
	return 0;
}

int* array_gen(int size){
	len = size; //set global variable equal to specified array size
	int* array = malloc(size * sizeof(*array)); //allocate memory for the array
	srand((unsigned) time (NULL)); //seed the random number generator
	if (size > 500){
		fprintf(stderr, "Array size too large. Please make it less than 500.\n");
		exit(0);
	}else{
		for (int i = 0; i < size; i++){ //iterate through array, generate random number [1,500] for each slot. 
			array[i] = (rand() % 100) + 1;
		}
		return array;
	}
}

int* counting_sort(int* a, int size, int range){
	int counter[range]; //allocate array of Range elements (count array) initialize all to 0
	for (int i = 0; i < range; i++) counter[i] = 0;
	for (int i = 0; i < size; i++) counter[a[i]]++; //increment counts of respective elements in count array
	//for (int i = 0; i < size; i++) printf("%d ", counter[i]);
	for (int i = 0, j = 0; /*i < range, */j < size; i++){ //iterate through counter array and original array, restore elements in original -- now sorted
		while (counter[i] != 0){
			a[j] = i;
			j++;
			counter[i]--;
		}
	}
	
	return a;
}

void radix_sort(int* a, int size, int expo){
	//int count = log10(max)+1;
	//printf("COUNT: %d\n", count);
	//printf("MAX: %d\n", max);
	int output[size]; //initialize output array
	int i, count[10] = { 0 }; //count array
	
	for (i = 0; i < size; i++) count[(a[i]/expo)%10]++;
	
	for (i = 1; i < 10; i++) count[i] += count[i-1];
	
	for (i = size-1; i >= 0; i--){
		output[count[(a[i]/expo) % 10] - 1] = a[i];
		count[(a[i]/expo)%10]--;
	}
	
	for (i = 0; i < size; i++) a[i] = output[i];
}

int* radii(int* a, int size){
	int m = getMax(a, size);
	for (int expo = 1; m/expo > 0; expo *= 10) radix_sort(a, size, expo);
	return a;
}


int getMax(int* a, int size){
	int max = a[0];
	for (int i = 1; i < size; i++) if (a[i] > max) max = a[i];
	return max;
}

int* bucket_sort(int* a, int size){
	int buckets[10][size]; //initialize 2D array, 10 buckets
	int j, k = 0, max = getMax(a, size); //compute max
	for (int i = 0; i < 10; i++) for (int j = 0; j < size; j++) buckets[i][j] = -1; //initialize to -1s
	
	//add each value to specific bucket
	for (int i = 0, j = 0; i < size; i++){
		int k = choose_bucket(max, a[i]);
		while (buckets[k][j] != -1) j++;
		buckets[k][j] = a[i];
		j = 0;
	}
	
	//int final[size]; //initialize sorted array of elements
	//iterate through each bucket
	for (int i = 0; i < 10; i++){
		for (j = 0; buckets[i][j] != -1; j++) ; //calculate size of each bucket
		printf("#s from bucket %d: %d\n", i, j);
		counting_sort(buckets[i], j+1, getMax(buckets[i], j+1)); //call counting sort on each bucket
		for (int l = 0; l < j; l++) a[k++] = buckets[i][l]; //copy values back into original array
	}
	
	//print
	return a;
	//for (int i = 0; i < 10; i++) for (int j = 0; j < size; j++) printf(" %d ", buckets[i][j]);
	//printf("\n"); 	
}

//accounts for 10 interval ranges, decides which range a number should be placed into
int choose_bucket(int max, int num){
	printf("MAX: %d, NUM: %d\n", max, num);
	int interval = (int)ceil(max/10.0), stat = interval, count = 0;
	printf("INTERVAL: %d\n", interval);
	while (num > stat){
		count++;
		stat += interval;
	}
	printf("COUNT: %d\n", count);
	return count;	
}
