#include <stdio.h>
#include <string.h>

#define MAXLINES 5000 /* max #lines to be sorted */
char *lineptr[MAXLINES]; /* pointers to text lines */

int readlines(char *lineptr[], int nlines);
void writelines(char *lineptr[], int nlines);

void qsort(void *lineptr[], int left, int right, int (*comp)(void *, void *));
void r_qsort(void *lineptr[], int left, int right, int (*comp)(void *, void *));
int numcmp(char *, char *);

/* sort input lines */
int main(int argc, char *argv[]) {
	int nlines; //number of input lines read
	int numeric = 0; // 1 if numeric sort
	int reverse = 0; // 1 for reverse sort
	
	if (argc == 2){ //change states if extra arguments detected
		if (strcmp(argv[1], "-n") == 0)
			numeric = 1;
		if (strcmp(argv[1], "-r") == 0)
			reverse = 1;
	}else if (argc == 3){
		if ((strcmp(argv[1], "-n") == 0 && strcmp(argv[2], "-r") == 0) || (strcmp(argv[1], "-r") == 0 && strcmp(argv[2], "-n") == 0))
			numeric = 1, reverse = 1;
		else if (strcmp(argv[1], "-n") == strcmp(argv[2], "-n") == 0)
			numeric = 1;
		else if (strcmp(argv[1], "-r") == strcmp(argv[2], "-r") == 0)
			reverse = 1;
	}
	
	if ((nlines = readlines(lineptr, MAXLINES)) >= 0){ //lines left to read??
		if (reverse == 1)
			r_qsort((void **) lineptr, 0, nlines-1, (int (*)(void*, void*))(numeric ? numcmp : strcmp));
		else
		/*(reverse ? r_qsort((void **) lineptr, 0, nlines-1, (int (*)(void*, void*))(numeric ? numcmp : strcmp) : qsort((void **) lineptr, 0, nlines-1, (int (*)(void*, void*))(numeric ? numcmp : strcmp)); //normal or reverse sort*/
			qsort((void **) lineptr, 0, nlines-1, (int (*)(void*, void*))(numeric ? numcmp : strcmp)); //qsort(range), either sort numerically or by strings
		writelines(lineptr, nlines);
		return 0;
	}else{
		printf("input too big to sort\n");
		return 1;
	}
}

/* qsort: sort v[left]...v[right] into increasing order */
void qsort(void *v[], int left, int right, int (*comp)(void*, void*)){ //qsort intakes an array of pointers, two integers, and a function w/two pointer arguments.
	int i, last;
	void swap(void *v[], int, int); //declare swap
	
	if (left >= right) //do nothing if array contains fewer than 2 elements
		return;
	swap(v, left, (left + right)/2); //swap first element with middle element (pointers point to the same addresses, which contain different elements)
	last = left; //set last to initial value
	for (i = left+1; i <= right; i++) //iterate through *v[]
		if ((*comp)(v[i], v[left]) < 0) //if v[left] > v[i], swap them and increment last
			swap(v, ++last, i);
	swap(v, left, last); //swap first element with last element
	qsort(v, left, last-1, comp); //call qsort on left half
	qsort(v, last+1, right, comp); //call qsort on right half
}

/* r_qsort same as qsort but in reverse order (high --> low) */
void r_qsort(void *v[], int left, int right, int (*comp)(void*, void*)){ 
	int i, last;
	void swap(void *v[], int, int); //same
	
	if (left >= right) //same
		return;
	swap(v, left, (left + right)/2); //same
	last = left; //same
	for (i = left+1; i <= right; i++) //same
		if ((*comp)(v[i], v[left]) > 0) //if v[left] < v[i], swap them and increment last
			swap(v, ++last, i);
	swap(v, left, last); //same
	qsort(v, left, last-1, comp); //same
	qsort(v, last+1, right, comp); //same
}

#include <stdlib.h>

/* numcmp: compare s1 and s2 numerically */
int numcmp(char *s1, char *s2){
	double v1, v2;
	
	v1 = atof(s1);
	v2 = atof(s2);
	if (v1 < v2)
		return -1;
	else if (v1 > v2)
		return 1;
	else
		return 0;
}

/* swap, exchanges two pointers */
void swap(void *v[], int i, int j){
	void *temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}