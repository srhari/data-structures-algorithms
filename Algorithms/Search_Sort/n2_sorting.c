/* Basic n^2 sorting algorithms */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int* array_gen(int);
int len;
int* bubble_sort(int*, int);
int* selection_sort(int*, int);
int* insertion_sort(int*, int);

int main(void){
	int i, *a;
	clock_t t;
	a = array_gen(500); //set integer pointer as equivalent to returned pointer.
	printf("ARRAY LENGTH: %d\n", len);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);
	printf("\n\n");
	
	/*t = clock();
	bubble_sort(a, len); //call bubble sort then re-print the array
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("BUBBLE SORT in %f seconds: ", time_taken);
	for (i = 0; i < len; i++) printf("%d ", a[i]);
	printf("\n\n");
	
	t = clock();
	selection_sort(a, len);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("SELECTION SORT in %f seconds: ", time_taken);
	for (i = 0; i < len; i++) printf("%d ", a[i]);
	printf("\n\n");*/
	
	t = clock();
	insertion_sort(a, len);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("INSERTION SORT in %f seconds: ", time_taken);
	for (i = 0; i < len; i++) printf("%d ", a[i]);
	
	free(a); //free memory
	
	return 0; 
}


int* array_gen(int size){
	len = size; //set global variable equal to specified array size
	int* array = malloc(size * sizeof(*array)); //allocate memory for the array
	srand((unsigned) time (NULL)); //seed the random number generator
	if (size > 500){
		fprintf(stderr, "Array size too large. Please make it less than 500.\n");
		exit(0);
	}else{
		for (int i = 0; i < size; i++){ //iterate through array, generate random number [1,500] for each slot. 
			array[i] = (rand() % 100) + 1;
		}
		return array;
	}
}

int* bubble_sort(int* a, int size){
	int temp;
	for (int i = 0; i < size-1; i++){ //nested for loop to make all comparisons
		for (int j = 0; j < size-1; j++){
			if (a[j] > a[j+1]){ //if left side of the 'bubble' is larger, swap the two numbers. 
				temp = a[j+1];
				a[j+1] = a[j];
				a[j] = temp;
			}
		}
	}
	return a;
}

int* selection_sort(int* a, int size){
	int temp;
	for (int i = 0; i < size-1; i++){
		for (int j = i+1; j < size; j++){ //if j < i, swap the two. 
			if (a[j] < a[i]){
				temp = a[j];
				a[j] = a[i];
				a[i] = temp;
			}
		}
	}
	return a;
}

int* insertion_sort(int* a, int size){
	int temp;
	for (int i = 1; i < size; i++){
		for (int k = i, j = i-1; j >= 0; j--, k--){
			if (a[k] < a[j]){  
				temp = a[k];
				a[k] = a[j];
				a[j] = temp;
			}
			
		}
	}
	return a;
}