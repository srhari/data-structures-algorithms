#include <stdio.h>
int binsearch(int x, int v[], int n);
int binsearch_two(int x, int v[], int n);

int main(int argc, char *argv[]) {
	int array[]={2,4,6,7,9,29,45,46,49,50,51};
	//printf("%d", binsearch(9,array,10));
	printf("%d", binsearch_two(9,array,10));
}

int binsearch(int x, int v[], int n){
	int low, high, mid;
	
	low=0; high=n-1;
	while (low <= high){
		mid = (low+high)/2;
		if (x < v[mid])
			high = mid - 1;
		else if (x > v[mid])
			low = mid + 1;
		else
			return mid;
	}
	return -1;
}

int binsearch_two(int x, int v[], int n){
	int low, high, mid;
	low = 0;
	high = n-1;
	
	mid = (low+high)/2;
	while (low<high && x!=v[mid]){
		if (x>v[mid])
			low = mid+1;
		else 
			high = mid-1;
		mid = (low+high)/2;
	}
	
	if (x==v[mid])
		return mid;
	else 
		return -1;
}