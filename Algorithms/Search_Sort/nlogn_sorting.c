/* nlogn sorting algorithms */
/* merge sort, quick sort, heap sort */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>

int j = 0, k, max, min;
int* array_gen(int);
int len; //global length variable

//heap sort global pointers
struct node* root = NULL; //pointer to root of heap
struct node* current = NULL;

//heap sort functions
struct node* create_Node(struct node*, int);
void heapify(int*, int, struct node*);
void max_heapify(struct node*);
void heap_print(struct node*);
int* heap_sort(int*, struct node*);
void swap_node(struct node*, struct node*);
void remove_root(struct node*);

//merge sort functions
int* merge_sort(int*, int);
void merge_split(int*, int*, int, int);
void merge(int*, int*, int, int);

//quick sort functions
int* quick_sort(int*, int);
void quick_split(int*, int, int);
int partition(int*, int, int, int, int);
void swap(int*, int, int);

//initialize Node structure which stores integer, left and right pointers
typedef struct node{
	int num;
	struct node* left;
	struct node* right;
}Node;

int main(void){
	int* a;
	clock_t t;
	a = array_gen(15); //generate random array
	for (int i = 0; i < len; i++) printf("%d ", a[i]);
	printf("\n\n");
	
	//int b[10] = {10, 5, 4, 7, 9, 3, 1, 6, 2, 8};
	//len = 10;
	/*t = clock();
	merge_sort(a, len);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("MERGE SORT in %f seconds: ", time_taken);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);
	printf("\n\n");*/
	
	/*t = clock();
	quick_sort(a, len);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("QUICK SORT in %f seconds: ", time_taken);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);*/
	
	/*t = clock();
	heapify(a, 0, root);
	heap_sort(a, root);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC;
	printf("HEAP SORT in %f seconds: ", time_taken);
	for (int i = 0; i < len; i++) printf("%d ", a[i]);*/

	return 0;
}

//generate array of random integers
int* array_gen(int size){
	len = size; //set global variable equal to specified array size
	int* array = malloc(size * sizeof(*array)); //allocate memory for the array
	srand((unsigned) time (NULL)); //seed the random number generator
	if (size > 500){
		fprintf(stderr, "Array size too large. Please make it less than 500.\n");
		exit(0);
	}else{
		for (int i = 0; i < size; i++){ //iterate through array, generate random number [1,500] for each slot. 
			array[i] = (rand() % 50) + 1;
		}
		return array;
	}
}

//create a basic unsorted tree with all array elements
void heapify(int* a, int i, struct node* p){
	if (i > len) return; //array covered, stop recurring
	if (p == NULL){
		p = create_Node(p, a[i++]); //will initialize the root
	}else if (p->left == NULL) p->left = create_Node(p->left, a[i++]); //left and right children
	else if (p->right == NULL) p->right = create_Node(p->right, a[i++]);
	else{
		heapify(a, i, current->left); //call on the left and right children
		heapify(a, i, current->right);
	}
}

//transforms a heap into max heap
void max_heapify(struct node* p){
	if (p->left == NULL && p->right == NULL) return; //halt recursion after reaching leaf nodes (no children)
	
	//left and right children -- recur down and swap upwards
	if (p->left != NULL && p->right != NULL){
		max_heapify(p->left);
		max_heapify(p->right);
		if (p->left->num > p->num){
			swap_node(p, p->left);
			max_heapify(p->left);
		}
		if (p->right->num > p->num){
			swap_node(p, p->right);
			max_heapify(p->right);
		}
	//left child only
	}else if (p->left != NULL){
		max_heapify(p->left);
		if (p->left->num > p->num){
			swap_node(p, p->left);
			max_heapify(p->left);
		}
	//right child only
	}else{
		max_heapify(p->right);
		if (p->right->num > p->num){
			swap_node(p, p->right);
			max_heapify(p->right);
		}
	}
}

//deletes root from a heap, then reconfigures the heap.
void remove_root(struct node* p){
	if (p->left != NULL){
		swap_node(p, p->left);
		remove_root(p->left);
	}else if (p->right != NULL){
		swap_node(p, p->right);
		remove_root(p->right);
	}else p = NULL;
}

//extracts from max heap to reconstruct a sorted array. 
int* heap_sort(int* a, struct node* p){
	for (int i = 0; i < len; i++){
		max_heapify(p);
		a[i] = p->num;
		remove_root(p);
	}
	
	return a;
}


//basic function - creates and returns new node
struct node* create_Node(struct node* p, int j){
	p = (struct node *) malloc(sizeof(struct node)); //allocate memory for struct
	p->num = j; //set struct value
	p->left = p->right = NULL; //set pointers
	printf("STRUCT CREATED! VALUE: %d\n", j);
	//j++; //increment j index
	return p; //return struct pointer
}

//print the values of the heap
void heap_print(struct node* p){
	if (p != NULL){
		printf("HEAP_PRINT: %d\n", p->num);
		heap_print(p->left);
		heap_print(p->right);
	}
}

//swaps the number values between two structs. 
void swap_node(struct node* a, struct node* b){
	int temp = a->num;
	a->num = b->num;
	b->num = temp;
}

int* merge_sort(int *a, int size){
	int temp[size]; //declare temporary array for storage
	merge_split(a, temp, 0, size-1); //call sorting algorithm
	//for (int i = 0; i < size; i++) printf(" %d", temp[i]);
	//printf("\n\n");
	return a; //return a pointer to the new array "temp"
}

void merge_split(int* a, int* temp, int leftStart, int rightEnd){
	//printf("IN MERGE_SPLIT...\n");
	if (leftStart >= rightEnd) return;
	int mid = (leftStart+rightEnd)/2; //define the midpoint of the array to split
	merge_split(a, temp, leftStart, mid); //split the array into two halves recursively
	merge_split(a, temp, mid+1, rightEnd);
	merge(a, temp, leftStart, rightEnd); //merging sorts the two halves of the array into a combined sorted array
}

void merge(int* a, int* temp, int leftStart, int rightEnd){
	//printf("MERGING...\n");
	int flag;
	int leftEnd = (leftStart + rightEnd)/2, rightStart = leftEnd+1, size = rightEnd - leftStart + 1;
	int left = leftStart, right = rightStart, index = leftStart;
	
	//sorting algorithm here, sorts into temp array
	while (left <= leftEnd && right <= rightEnd){
		if (a[left] <= a[right]){
			temp[index] = a[left];
			left++;
			//if (left > leftEnd) flag = 0; //set flag to 0 or 1 depending on which side is left over
		}else{
			temp[index] = a[right];
			right++;
			//if (right > rightEnd) flag = 1;
		}
		index++;
	}
	
	//copy remaining elements from the left or right side to the temp array. only one of these two lines will execute. 
	for (int i = right; i <= rightEnd; i++, index++) temp[index] = a[i];
	for (int j = left; j <= leftEnd; j++, index++) temp[index] = a[j];
	
	//copy from temp array back to main array a
	for (int k = leftStart; k < leftStart+size; k++) a[k] = temp[k];
	
}

int* quick_sort(int* a, int size){
	quick_split(a, 0, size-1);
	return a;
}

void quick_split(int* a, int left, int right){
	if (left >= right) return; //if size 1 block, return
	int size = right-left+1, pivot_index = (left + right)/2, pivot = a[pivot_index]; //pivot_index = rand() % size, pivot = a[pivot_index];
	int partitionPoint = partition(a, left, right, pivot, pivot_index); //generate a partition point
	quick_split(a, left, partitionPoint-1); //recur on the left and right sides
	quick_split(a, partitionPoint+1, right);
}

int partition(int* a, int left, int right, int pivot, int pivot_index){
	int leftPointer = left, rightPointer = right; //assign pointers
	swap(a, pivot_index, right); //move the pivot to the end of the array to get it out of the way
	rightPointer--; //decrement the right Pointer by 1 to skip the pivot
	while (leftPointer <= rightPointer){
		while (a[leftPointer] < pivot) leftPointer++; //skip if conditions not met
		while (a[rightPointer] >= pivot) rightPointer--;
		if (leftPointer >= rightPointer) break; //if the two pointers meet, end the partition step.
		else swap(a, leftPointer, rightPointer); //swaps values in array specified by two pointers
	}
	swap(a, leftPointer, right); //swap the pivot with the left pointer. This should sort the array around the pivot 
	return leftPointer; //return the index of the pivot. 
}

//basic swapping function
void swap(int* a, int num1, int num2){
	int temp = a[num1];
	a[num1] = a[num2];
	a[num2] = temp;
}