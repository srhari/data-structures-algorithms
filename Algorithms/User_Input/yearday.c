#include <stdio.h>
static char daytab[2][13] = { //pre-initialize static array
	{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
	{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

int day_of_year(int year, int month, int day);
void month_day(int year, int yearday);

int main(void){
	int day, mo, dat;
	day = day_of_year(1988, 2, 29);
	printf("%d\n", day);
	month_day(1988, 60);
	return 0;
}

int day_of_year(int year, int month, int day){ //set day of the year from month & day
	int i, leap;
	
	leap = year%4 == 0 && year%100 != 0 || year%400 == 0; //assign 0 or 1 to leap year
	for (i = 1; i < month; i++) //increment through {0} or {1} array, add up days 
		day += daytab[leap][i];
	return day; //return
}

void month_day(int year, int yearday, int *pmonth, int *pda){
	int i, leap;
	
	leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
	for (i = 1; yearday > daytab[leap][i]; i++)
		yearday -= daytab[leap][i];
	printf("Month: %d, Day: %d\n", i, yearday);
}