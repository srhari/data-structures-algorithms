#include <stdio.h>
#define MAXLINE 1000
int mgetline(char line[], int maxline);
void escape(char s[], char t[]);

int main(int argc, char *argv[]) {
	char s1[MAXLINE], s2[MAXLINE];
	ngetline(s1, MAXLINE);
	expand(s1, s2);
	printf("%s", s2);
	return 0;
}


int mgetline(char s[], int lim){
	int i, c;
	for (i=0; i<lim-1 && (c=getchar())!=EOF; ++i)
		s[i]=c;
	s[i] = '\0';
}

int ngetline(char s[], int lim){
	int i, c;
	for (i=0; i<lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
		s[i]=c;
	s[i]='\0';
}

void escape(char s[], char t[]){
	int i = 0, j = 0;
	
	while (t[i] != '\0'){
		switch(t[i]){
			case '\t':
				s[j] = '\\';
				++j;
				s[j]='t';
				break;
			case '\n':
				s[j] = '\\';
				++j;
				s[j] = 'n';
				break;
			default:
				s[j] = t[i];
				break;
		}
		++i;
		++j;
	}
	s[j]='\0';
}

void expand(char[] s1, char[] s2){
	int i = 0, j = 0, c;
	
	while ((c = s1[i++]))!='\0'){ //iterate through array s1
		if (s1[i] == '-' && s1[i+1] >= c){ //detect '-' and later character after
			i++; //increment i
			while (c<s1[i]) //all characters between c and '-'
				s2[j++]=c++; //increment c and add to j, increment j
		}
		else
			s2[j++]=c; //add character as normal if not '-'		
	}
	
	s2[j]='\0'; //last element of array is always \0

}