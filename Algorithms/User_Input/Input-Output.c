#include <stdio.h>
#define LINE_LENGTH 100

void fold();
void remove_comments();

int main(int argc, char *argv[]) {
	fold();
}

void fold(){ //Folds long input lines into two or more shorter lines, outputs. 
	int i;
	char c;
	for (i=0; (c=getchar())!=EOF&&(c!='\n'); ++i){ //Gather input stream
		if (i%LINE_LENGTH==0){ //if #characters % CONSTANT == 0, insert '\n'.	
			putchar('\n');
			putchar(c);
		}else{
			putchar(c);	
		}
	}
}

void remove_comments(){ //Removes all comments from a C program
	char c, d, e, f, g;
	while ((c=getchar())!=EOF){ //Iterate through all characters of C program
		if (c=='/'){ //check for double '//'
			if ((d=getchar())=='/') //if found, OMIT putchar(), skip to next line.
				c='\n';
			if ((d=getchar())=='*'){ //check for '/*'				
				if ((e=getchar())=='*'){
					if ((f=getchar())=='/'){
						g=getchar(); //if found, OMIT putchar() until '*/' is found
						putchar(g);
					}else{
						f=getchar();
						putchar(f);
					}
				}
			}else{
				putchar(d);
			}
		}else{
			putchar(c);
		}
	}
}
	
	