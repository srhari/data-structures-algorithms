/* tail - prints the last n lines of its input. By default, n is 10, but it can be changed by an optional argument, so that tail -n prints the last n lines. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFLINES 10 //default # of lines to print
#define LINES 100 //maximum # of lines to print
#define MAXLEN 100 //maximum length of an input line

void error(char *);
int mgetline(char *, int);

int main(int argc, char *argv[]) {
	char *p;
	char *buf; //pointer to large buffer
	char *bufend; //pointer to end of buffer
	
	char line[MAXLEN];
	char *lineptr[LINES]; //pointer to # of lines read
	
	int first, i, last, len, n, nlines;
	
	if (argc == 1) //if 1 argument read only, default to 10
		n = DEFLINES;
	else if (argc == 2 && (*++argv)[0] == '-') //if 2 arguments read and first element of incremented pointer is '-', string --> integer
		n = atoi(argv[0]+1);
	else
		error("Usage: tail [-n]"); //if conditions not met
	if (n < 1 || n > LINES) //if n is not within the range 0-100, default to 100
		n = LINES;
	for (i = 0; i < LINES; i++) //set pointer to point to line n
		lineptr[i] = NULL;
		
	if ((p = buf = malloc(LINES * MAXLEN)) == NULL) //allocated memory space is empty?
		error("tail: cannot allocate buf"); //error
	bufend = buf + LINES + MAXLEN; //increment pointer to end of buffer
	
	last = 0;
	nlines = 0;
	
	while ((len = mgetline(line, MAXLEN)) > 0){ //lines left to get??
		if (p+len+1 >= bufend) //
			p = buf;
		lineptr[last] = p;
		
		strcpy(lineptr[last], line);
		if (++last >= LINES)
			last = 0;
			
		p += len+1;
		nlines++;
	}
	
	if (n > nlines)
		n = nlines;
	first = last-n;
	if (first < 0)
		first += LINES;
	for (i = first; n-->0; i = (i+1)%LINES)
		printf("%s", lineptr[i]);
	
	return 0;
}

/* mgetline: read a line into s and return length */
int mgetline(char s[], int lim){
	int c, i;
	
	for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; i++)
		s[i] = c;
	if (c == '\n'){
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

/* error: print error messages and exit */
void error(char *s){
	printf("%s\n", s);
	exit(1);
}