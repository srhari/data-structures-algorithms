#include <stdio.h>
#define MAXLINE 1000

int mgetline(char s[], int lim);
void strcat(char *, char *);
int strend(char *, char *);
void strncpy(char *, char *, int n);
int strncmp(char *, char *, int n);
int strlen(char *);

int main(void) {
	int len;
	char s[MAXLINE], t[MAXLINE];
	putchar('s');
	putchar(':');
	mgetline(s, MAXLINE);
	
	putchar('t');
	putchar(':');
	mgetline(t, MAXLINE);
	
	strcat(s, t);
	printf("%s", s);
	return 0;
}

int mgetline(char s[], int lim){
	int c, i;
	
	for (i = 0; i<lim-1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n'){
		s[i] = c;
		++i;
	}
	
	s[i] = '\0';
	return i;
}

/* concatenate t to end of s, s must be big enough */
void strcat(char *s, char *t){
	while (*s != '\0')
		s++;
	s--;
	while ((*s++ = *t++) != '\0')
		;
}

/* returns 1 if the string t occurs at the end of string s, zero otherwise */
int strend(char *s, char *t){
	int len = strlen(t); //evaluate length of t to be found
	
	while (*s != '\0') //iterate through string s
		++s;
	--s; //end up just before \0
	
	while (*t != '\0') //iterate through string t
		++t;
	--t; //end up just before \0
	
	while (len > 0){ //if positive length
		if (*t == *s){ //compare pointers
			--t; //decrement both pointers and the length
			--s;
			--len;
		}else //if any unequal values found, return 0
			return 0; 
	}
	if (len == 0) //if length reaches 0 and all values equal, return 1 AFFIRMATIVE
		return 1;
}

/* copy at most n characters from string t to string s. pad with \0s if t has fewer than n characters. */
void strncpy(char *s, char *t, int n){
	int len = strlen(t) //evaluate length of t to be found
	
	while (*s != '\0') //iterate to end of *s, before \0
		++s;
	--s;
	
	while ((*s++ = *t++) != '\0' && n > 0) //copy t to s, decrement n
		--n;
	if (n > 0) //if n remaining after t, copy n '\0's to *s
		for (; n > 0; --n)
			*s++ = '\0'
}	

/* compare at most n characters of string s to string t. return <0 if s<t, 0 if s==t, or >0 if s>t. */
int strncmp(char *s, char *t, int n){
	for (; *s == *t; s++, t++) //iterate through *s and *t
		if (*s == '\0' || --n <= 0) // if end of *s reached or n characters compared
			return 0;
	return *s - *t
	
}

int strlen(char *s){
	char *p = s;
	while (*s != '\0')
		++s;
	return s - p;
}