#include <stdio.h>
#include <limits.h>
//limits.h package imported - defined bounds for short, int (signed & unsigned).

int direct_computation();
int for_loop(int lim);
int htoi(char s[]);
int array_length(char s[]);
int exponential(int a, int b);
void squeeze(char a[], char b[]);

int main() {
	//printf("%d", SCHAR_MIN);
	//direct_computation();
	//for_loop(10);
	//printf("%d", htoi("0x123"));
	squeeze("Adonis", "Alfonso");
}

int direct_computation(void){ //manipulates sign, converts to unsigned char, ~0 (bits in 1s), >>1 right shift to remove sign bit
	printf("\n");
	printf("Minimum Signed Char %d\n", -(char)((unsigned char)~0>>1)-1);
	printf("Maximum Signed Char %d\n", (char)((unsigned char)~0>>1));
	printf("Minimum Signed Short %d\n", -(short)((unsigned short)~0>>1)-1);
	return 0;
}

int for_loop(int lim){ //loop equivalent
	int i;
	char c;
	char s[1000];
	for (i=0; i<lim-1; ++i){
		if ((c=getchar())!='\n'){
			if (c!=EOF)
				s[i]=c;
		}
	}
	return 0;
}

int htoi(char s[]){
	int i, decimal=0;
	for (i=0; s[i]!='\0'; ++i){ //iterate through input array
		if ((s[i]=='0') && (s[i+1]=='x'||s[i+1]=='X')) //if '0' detected, if next char is 'x' or 'X', ignore both. 
			i=i+2;
		else{ //else, replace each character with respective integer by subtraction, allocate into array	
			if (s[i]>='A' && s[i]<='z')
				s[i]=s[i]-'7';
			else if (s[i]>='0' && s[i] <= '9')
				s[i]=s[i]-'0';
			else{ //invalid case
				printf("Invalid input. Re-run the program with hexadecimal format."); 
				return 0;
			}
			decimal = decimal + s[i]*exponential(16, (array_length(s)-i)); //construct decimal integer
			return decimal; //return decimal integer
		}
	}
	return 0;
}

int array_length(char s[]){ //compute array length
	int j, length=0;
	for (j=0; s[j]!='\0'; ++j)
		;
	return j;
}

int exponential(int base, int exponent){ //calculate exponents recursively
	if (exponent==0)
		return 1;
	else
		return (base * exponential(base, exponent-1));
}

void squeeze(char s1[], char s2[]){
	int i, j;
	for (i=0; s1[i]!='\0'; ++i){ //iterate through s1
		for (j=0; s2[j]!='\0'; ++j){ //iterate through s2
			if (s1[i]==s2[j]) //if s1 matches s2
				s1[i]=s1[i+1]; //remove s1 from array
		}
	}
	printf("%s", s1);
}