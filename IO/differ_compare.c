/* Compares two files, prints the first line where they differ. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLINE 1000

char a[1000];
char b[1000]; //initialize two arrays to store lines

int main(int argc, char* argv[]) {
	//evin lewis, eoin morgan, david warner
	FILE* fp; //initialize two file pointers. 
	FILE* fpp;
	fp = fopen(argv[1], "r"); //open first file provided by argument for read-only
	if (fp == NULL){
		fprintf(stderr, "Can't open %s\n", argv[1]);
		exit(0);
	}else printf("FILE 1 OPENED SUCCESSFULLY.\n");
	fpp = fopen(argv[2], "r"); //open second file
	if (fpp == NULL){
		fprintf(stderr, "Can't open %s\n", argv[2]);
		exit(0);
	}else printf("FILE 2 OPENED SUCCESSFULLY.\n");
	
	//if (fgets(a,MAXLINE,fp) != NULL) printf("FGETS ");
	//if (fgets(b,MAXLINE,fpp) != NULL) printf("FGETS\n");
	
	while (fgets(a,MAXLINE,fp) != NULL && fgets(b,MAXLINE,fpp) != NULL){
		printf("INSIDE WHILE...\n");
		if (strcmp(a,b) == 0) ;
		else{
			printf("%s\n", a);
			printf("%s\n", b);
		}
	}
	
	fclose(fp);
	fclose(fpp);
}