/* Converts upper case to lower case, and lower case to upper case, depending on the name it is invoked with. */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char* a = "l";
	char* b = "u";
	
	if (strcmp(argv[1], a) == 0){
		int c;
		while ((c = getchar()) != EOF) putchar(tolower(c));
		return 0;
	}else if (strcmp(argv[1], b) == 0){
		int c;
		while ((c = getchar()) != EOF) putchar(toupper(c));
		return 0;
	}else printf("INVALID ARGUMENT -- CALL \"convert_alpha l\" or \"convert_alpha u\" to convert to lower and upper case, respectively.");
}

