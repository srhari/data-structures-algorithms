public class DoublyLinkedNode{

	private int data;
	private DoubleLinkedNode nextNode;
	private DoubleLinkedNode prevNode;

	public DoublyLinkedNode(int data){
		this.data = data;
	}

	public int getData(){
		return data;
	}
	
	public DoublyLinkedNode getNextNode(){
		return nextNode;
	}

	public void setNextNode(DoublyLinkedNode nextNode){
		this.nextNode = nextNode;
	}

	public DoublyLinkedNode getPrevNode(){
		return prevNode;
	}

	public void setPrevNode(DoublyLinkedNode prevNode){
		this.prevNode = prevNode;
	}

	public String toString(){
		return this.data.toString();
	}
}
