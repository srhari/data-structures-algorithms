public class LinkedList{
	
	private Node head;
	
	public void insertAtHead(int data){
		Node newNode = new Node(data);
		this.head = newNode;
	}

	public String toString(){
		String result = "{";
		Node current = this.head;
		while (current != null){
			result += current.toString() + ",";
			current = current.getNextNode();
		}

		result += "}";
		return result;
	}

	public int length(){
		int length = 0;
		Node current = this.head;
		
		while (current != null){
			length++;
			current = current.getNextNode();
		}
		return length;
	}

	public void deleteFromHead(){
		this.head = this.head.getNextNode();
	}

	public Node find (int data){
		Node current = this.head;

		while (current != null){
			if (current.getData() == data){
				return current;
			}
			current = current.getNextNode();
		}
		return null;
	}

	public void appendNodes(LinkedList l, int numberOfNodes){
		int length = 0;
		Node current = this.head;
		while (current != null){
			length++;
			current = current.getNextNode();
		}
		
		while (current != null){
			Node temp = new Node(current.data);
			this.head = temp;
			current = current.getNextNode();
		}
		Node a = this.head, t;

		for (int count = 0; count < length; count++){
			a = a.getNextNode();
		}
		t = a.next;

		for (count = 0; count < numberOfNodes && a != null; count++){
			Node temp = t;
			t = t.next;
			free(temp);
		}
		a.next = t;
	}
}
		
			
		
		
		
