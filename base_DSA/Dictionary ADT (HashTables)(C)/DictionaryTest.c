/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B pa5
 * June 1st, 2015
 * DictionaryTest.c
 * test file for Dictionary.c, tests ADT operations / functions
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Dictionary.h"

#define MAX_LEN 180

int main(int argc, char* argv[]){
   Dictionary A = newDictionary();
   char* k;
   char* v;
   char* word1[] = {"Steph","Kyrie","LeBron","Kobe","DRose","Birdman","KD"};
   char* word2[] = {"Spurs","Warriors","Cavaliers","Heat","Pacers","Grizzlies","Clippers"};
   int i;

   for(i=0; i<7; i++){
      insert(A, word1[i], word2[i]);
   }

   printDictionary(stdout, A);

   for(i=0; i<7; i++){
      k = word1[i];
      v = lookup(A, k);
      printf("key=\"%s\" %s\"%s\"\n", k, (v==NULL?"not found ":"value="), v);
   }

   insert(A, "Kobe", "Carmelo"); // error: duplicate keys

   delete(A, "Steph");
   delete(A, "DRose");
   delete(A, "KD");
   delete(A, "Kobe");

   printDictionary(stdout, A);

   for(i=0; i<7; i++){
      k = word1[i];
      v = lookup(A, k);
      printf("key=\"%s\" %s\"%s\"\n", k, (v==NULL?"not found ":"value="), v);
   }

   delete(A, "one");  // error: key not found

   printf("%s\n", (isEmpty(A)?"true":"false"));
   printf("%d\n", size(A));
   makeEmpty(A);
   printf("%s\n", (isEmpty(A)?"true":"false"));
   insert(A, "CP3", "James Harden");
   printf("&s\n", (isEmpty(A)?"true":"false"));

   freeDictionary(&A);

   return(EXIT_SUCCESS);
}
