/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B pa5
 * June 1st, 2015
 * Dictionary.c, dictionary ADt implementation using hastables
 */

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include "Dictionary.h"

const int tableSize = 100;

/*creating type Node*/
typedef struct Node{
	char* key;
	char* value;
	struct Node* next;
} Node;
typedef Node* ptNode;

/*constructor for type Node*/
ptNode newNode(char* x, char* y){
	ptNode N = malloc(sizeof(Node));
	assert(N!=NULL);
	N->key = x;
	N->value = y;
	N->next = NULL;
	return(N);
}

/*frees heap memory for type Node*/
void freeNode(ptNode* pN){
	if(pN!=NULL && *pN!=NULL){
		free(*pN);
		*pN = NULL;
	}
}

/*creates type list*/
typedef struct List{
	ptNode front;
	int size;
}List;
typedef List* ptList;

/*constructs type List*/
ptList newList(void){
	ptList L = malloc(sizeof(List));
	assert(L!=NULL);
	L->front = NULL;
	L->size = 0;
	return L;
}

/*free list heap memory*/
void freeList(ptList* pL){
	free(*pL);
	*pL = NULL;
}

/*creates type Dictionary*/
typedef struct DictionaryObj{
	ptList* table;
	int numItems;
}DictionaryObj;

unsigned int rotate_left(unsigned int value, int shift){
	int sizeInBits = 8*sizeof(unsigned int);
	shift = shift & (sizeInBits - 1);
	if(shift == 0)
	return value;
	return (value << shift) | (value >> (sizeInBits - shift));
}

unsigned int pre_hash(char* input){
	unsigned int result = 0xBAE86554;
	while (*input){
		result ^= *input++;
		result = rotate_left(result, 5);
	}
	return result;
}

/*hash function*/
int hash(char* key){
	return pre_hash(key)%tableSize;
}

/*constructs dictionary ADT */
Dictionary newDictionary(void){
	int i;
	Dictionary D = malloc(sizeof(DictionaryObj));
	assert(D!=NULL);
	D->table = calloc(tableSize+1,sizeof(ptList));
	D->numItems = 0;
	for(i=0; i<tableSize; i++){
		D->table[i] = newList();
	}
	return D;	
}

/*free's heap memory for the Dicitonary ADT*/
void freeDictionary(Dictionary *pD){
		free(*pD);
		*pD = NULL;
}

/*determines if list is empty or not*/
int isEmpty(Dictionary D){
	if( D == NULL){
		fprintf(stderr,"Stack Error: calling isEmpty() on NULL DictionaryRef\n");
		exit(EXIT_FAILURE);
	}
	return (D->numItems == 0);
}

/*empties the dictionary*/
void makeEmpty(Dictionary D){
        if( D == NULL){
                fprintf(stderr,"Stack Error: calling makeEmpty() on NULL DictionaryRef\n");
                exit(EXIT_FAILURE);
        }
        int i;
        for(i=0; i<tableSize; i++){
                D->table[i]->size = 0;
                freeNode(&D->table[i]->front);
        }
        D->numItems = 0;

}

/*returns list size*/
int size(Dictionary D){
	if(D == NULL){
		fprintf(stderr,"Stack Error: calling size() on NULL DictionaryRef\n");
		exit(EXIT_FAILURE);
	}
	return (D->numItems);
}

/*returns value associated with corresponding key*/
char* lookup(Dictionary D, char* k){
	ptNode N = D->table[hash(k)]->front;
	while(N!=NULL){
		if( strcmp(N->key, k) == 0 ){
			return N->value;
		}
		N = N->next;
	}
	return NULL;
}

/*inserts new key and value into the hashtable*/
void insert(Dictionary D, char* k , char* v){
	int h = hash(k);
	
	ptNode P = newNode(k,v);
	if( D == NULL){
		fprintf(stderr,"Stack Error: calling insert() on NULL DictionaryRef\n");
		exit(EXIT_FAILURE);
	}
	if( lookup(D,k) != NULL){
		fprintf(stderr,"Cannot insert() dublicate key\n");
		exit(EXIT_FAILURE);
	}
	if( D->table[h]->front == NULL){
		D->table[h]->front = P;
	}
	else{
		P->next = D->table[h]->front;
		D->table[h]->front = P;
	}
	D->numItems++;
	D->table[h]->size++;
}

/*deletes key*/
void delete(Dictionary D, char* k){
	int h = hash(k);
	ptNode N = D->table[h]->front;
	ptNode P = NULL;
	if( D == NULL){
		fprintf(stderr,"Stack Error: calling delete() on NULL DictionaryRef\n");
		exit(EXIT_FAILURE);
	}
	if( lookup(D,k) == NULL){
		fprintf(stderr,"Cannot delete() non-existant key\n");
		exit(EXIT_FAILURE);
	}
	if( D->table[h]->size == 1){
		freeNode(&N);
	}
	else if (N->key == k){
		P = N;
		D->table[h]->front = N->next;
		freeNode(&P);
	}
	else{
		while(N->next->key != k){
			N = N->next;
		}
		P = N->next;
		N->next = P->next;
		freeNode(&P);
	}
	D->numItems--;
	D->table[h]->size--;
}

/*Prints the dictionary to the out file in order from table[0]...table[m-1] */
void printDictionary(FILE* out, Dictionary D){
	if( D == NULL){
		fprintf(stderr,"Stack Error: calling printDictionary() on NULL DictionaryRef\n");
		exit(EXIT_FAILURE);
	}
	ptNode N = NULL;
	int i;
	for( i=0; i<tableSize; i++){
		N = D->table[i]->front;
		while( N!=NULL){
			fprintf(out,"%s %s\n",N->key,N->value);
			N = N->next;
		}
	}
}
