/* Srihari Dasarathy (sdasarat) 1414679
 * CS12M Lab5 DictionaryTest.c
 * May 10th, 2015
 * DictionaryTest.c, test file for class Dictionary
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "Dictionary.h"

#define MAX_LEN 180

int main (int argc, char* argv[]){
	Dictionary A = newDictionary();
	char* k;
	char* v;
	char* word1[] = {"Kyrie Irving", "James Harden", "LeBron James", "Blake Griffin", "DeAndre Jordan"};
	char* word2[] = {"Ricky Rubio", "Kevin Love", "Derrick Rose", "Mike Conley", "LaMarcus Aldridge"};
	int i;
	for (i=0; i<5; i++){
		insert(A, word1[i], word2[i]);
	}

	printDictionary(stdout, A);
	
	for (i = 0; i < 5; i++){
		k = word1[i];
		v = lookup(A, k);
		printf("key=\"%s\" %s\"%s\"\n", k, (v==NULL?"not found ":"value="),v);
	}

	printf("%d\n", size(A));

	delete(A, "Kyrie Irving");
	delete(A, "LeBron James");
	delete(A, "James Harden");

	printDictionary(stdout, A);

	for(i = 0; i < 5; i++){
		k = word1[i];
		v = lookup(A, k);
		printf("key=\"%s\" %s\"%s\"\n", k, (v==NULL?"not found ":"value="), v);
	}
	insert(A, "LeBron James", "Allen Iverson");

	printf("%s\n", (isEmpty(A)?"true":"false"));
	makeEmpty(A);
	printf("%s\n", (isEmpty(A)?"true":"false"));

	freeDictionary(&A);
	return(EXIT_SUCCESS);
}
