/* Srihari Dasarathy (sdasarat) 1414679
 * CS12M Lab5
 * May 6th, 2015
 * Dictionary.c, ADT Data file for Dictionary class
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<ctype.h>
#include"Dictionary.h"

#define INIT_LENGTH 50

typedef struct NodeObj{ //type NodeObj
	char key[INIT_LENGTH];
	char value[INIT_LENGTH];
	struct NodeObj* next;
	struct NodeObj* prev;
}
NodeObj;
typedef NodeObj* Node;

Node newNode(char* x, char* y){ //constructor for Node type
	Node N = malloc(sizeof(NodeObj));
	assert(N != NULL);
	strcpy(N->key,x);
	strcpy(N->value,y);
	N->next = NULL;
	N->prev = NULL;
	return(N);
}

//destructor for Node type
void freeNode(Node* pN){
	if (pN != NULL && *pN != NULL){
		free(*pN);
		*pN = NULL;
	}
}

//StackObj
typedef struct DictionaryObj{
	Node head;
	Node back;
	int numKeys;
}
DictionaryObj;

//newStack, constructor for type Stack
Dictionary newDictionary(void){
	Dictionary D = malloc(sizeof(DictionaryObj));
	assert(D != NULL);
	D->head = NULL;
	D->back = NULL;
	D->numKeys = 0;
	return D;
}

//destructor for type Dictionary
void freeDictionary(Dictionary* pD){
	if (pD != NULL && *pD != NULL){
		if (!isEmpty(*pD)){
			makeEmpty(*pD);
			free(*pD);
			*pD = NULL;
		}
	}
}

//find a particular key in the dictionary
Node findKey(Dictionary D, char* index){
	Node N = D->head;
	for (int i = 0; i < D->numKeys; i++){
		if (strcmp(N->key,index)==0){
			return N;
		}
		N = N->next;
	}
	return NULL;
}

//find the size (key/value) of the Dictionary
int size(Dictionary D){
	return(D->numKeys);
}

//search in dictionary
char* lookup(Dictionary D, char* index){
	Node N = findKey(D, index);
	if (N == NULL){
		return NULL;
	}
	return N->value;
}

//determines if dictionary is empty or not
int isEmpty(Dictionary D){
	if (D == NULL){
		fprintf(stderr, "Dictionary Error: calling isEmpty() on NULL Dictionary reference\n");
		exit(EXIT_FAILURE);
	}
	return(D->numKeys==0);
}

//pushes x on top of S, "stacks" x as the topmost reference
void insert(Dictionary D, char* index, char* foreign){
	Node O;
	if (lookup(D,index) != NULL){
		fprintf(stderr, "Dictionary Error: calling insert() on NULL Dictionary reference\n");
		exit(EXIT_FAILURE);
	}
	O = newNode(index,foreign);
	if (D->numKeys == 0){
		O->next = NULL;
		O->prev = NULL;
		D->head = O;
		D->back = O;
	}else{
		O->prev = D->back;
		O->next = NULL;
		D->back->next = O;
		D->back = O;
	}
	D->numKeys++;
}

//deletes and returns item at top of S
void delete(Dictionary D, char* index){
	Node nNode, pNode, dNode;
	if (lookup(D,index) == NULL){
		fprintf(stderr, "Dictionary Error: calling delete() on NULL Dictionary reference\n");
		exit(EXIT_FAILURE);
	}
	dNode = findKey(D,index);
	if (D->numKeys==1){
		D->head = NULL;
	}else if (dNode->prev == NULL){
		//fprintf(stderr, "Stack Error: calling pop() on empty Stack\n");
		//exit(EXIT_FAILURE);
		nNode = dNode->next;
		nNode->prev = NULL;
		D->head = nNode;
	}else if (dNode->next == NULL){
		pNode = dNode->prev;
		pNode->next = NULL;
	}else{
		pNode = dNode->prev;
		nNode = dNode->next;
		nNode->prev = dNode->prev;
		pNode->next = dNode->next;
	}
	freeNode(&dNode);
	D->numKeys--;
}
	/*N = S->top;
	returnValue = S->top->key;
	S->top = S->top->next;
	S->numKeys--;
	freeNode(&N);
	return returnValue;
}*/

/*//returns item at top of S
int peek(Stack S){
	if (S == NULL){
		fprintf(stderr, "Stack Error: calling peek() on NULL Stack reference\n");
		exit(EXIT_FAILURE);
	}
	if (S->numKeys==0){
		fprintf(stderr, "Stack Error: calling peek() on empty stack\n");
		exit(EXIT_FAILURE);
	}
	return S->top->key;
}*/

//resets S to empty state
void makeEmpty(Dictionary D){
	int i, count = D->numKeys;
	Node N, nNode;
	N = D->head;
	for (i = 0; i < count; i++){
		nNode = N->next;
		delete(D, N->key);
		N = nNode;
	}
	D->head = NULL;
	D->back = NULL;
	D->numKeys = 0;
}

//prints a text representation of S to the file pointed to by out
void printDictionary(FILE* out, Dictionary D){
	Node N;
	if (D==NULL){
		fprintf(stderr, "Dictionary Error: calling printDictionary() on NULL Dictionary reference\n");
		exit(EXIT_FAILURE);
	}
	for (N = D->head; N != NULL; N = N->next){
		fprintf(out, "%s %s \n", N->key,N->value);
	}
	//fprintf(out, "\n");
}


	
