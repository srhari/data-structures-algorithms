#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include<assert.h>
#include "Dictionary.h"

#define LENGTH 20

typedef struct NodeObj{
     char key[LENGTH];
     char value[LENGTH];
     struct NodeObj* next;
     struct NodeObj* prev;
} NodeObj;

// Node
typedef NodeObj* Node;

//constructor of the Node Type
Node newNode(char* x, char* y){
    Node N=malloc(sizeof(NodeObj));
    assert(N!=NULL);
    strcpy(N->key,x);
    strcpy(N->value,y);
    N->next=NULL;
    N->prev=NULL;
    return(N);
   }

//freNode
//decontructore for the Node type 
void freeNode(Node* pN){
   if(pN!=NULL && *pN!=NULL){
      free(*pN);
      *pN=NULL;
    }  
}

typedef struct DictionaryObj{
    Node head;
    Node back;
   int numItems;
} DictionaryObj;

Dictionary newDictionary(void){
    Dictionary D=malloc(sizeof(DictionaryObj));
    assert(D!=NULL);
    D->head=NULL;
    D->back=NULL;
    D->numItems = 0;
    return D;
}

//freeDictionary
//destructor for dictionary type 
void freeDictionary(Dictionary* pD){
  if(pD!=NULL && *pD!=NULL){
     if(!isEmpty(*pD)) makeEmpty(*pD);
     free(*pD);
     *pD=NULL;
  } 
}

Node findKey(Dictionary D, char* keyIndex){
    Node N = D->head;

    for (int i = 1; i < D->numItems + 1; i++) {
        if (strcmp(N->key, keyIndex) == 0) return N;
        N = N->next;
    }
    return NULL;
}


//isEmpty
//returns 1 (true) if D is Empty, 0 (false) otherwise 
int isEmpty(Dictionary D){
    if(D==NULL){
    fprintf(stderr,"Dict Error: calling isEmpty() on NULL Dict reference\n");
    exit(EXIT_FAILURE);
    } 
    return(D->numItems==0);
}

// size()
// returns the number of (key, value) pairs in D
// pre: none
int size(Dictionary D) {
    return (D->numItems);
}

char* lookup(Dictionary D, char* keyIndex){
  Node N=findKey(D,keyIndex);
   if(N==NULL){
    return NULL;
   }
    return N->value;
}
  
// insert()
// inserts new (key,value) pair into D
// pre: lookup(D, k)==NULL
void insert(Dictionary D, char* keyIndex, char* newValue){
    Node I;

    if (lookup(D,keyIndex) != NULL){
        fprintf(stderr, "Dict Error: calling insert() on NULL Dict reference\n");
    }
    // the real work begins here
    I=newNode(keyIndex, newValue);


    if (D->numItems == 0)
    {
        I->next = NULL;
        I->prev = NULL;
        D->head = I;
        D->back = I;
    }
    else
    {
        // insert at back
        I->prev =D->back;
        I->next = NULL;
        D->back->next = I;
        D->back = I;

    }
    
    // ends real work here
    D->numItems++;
}

// delete()
// deletes pair with the key k
// pre: lookup(D, k)!=NULL
void delete(Dictionary D, char* keyIndex){
    Node nextNode, prevNode, delNode;

    if (lookup(D,keyIndex) == NULL){
        fprintf(stderr, "Dict Error: calling delete() on NULL dict reference\n");
    }

    delNode = findKey(D, keyIndex);    // get the reference to the matching node

    if (D->numItems == 0)          // Delete last element ==> empty list
    {
        D->head = NULL;
    }
    else if (delNode->prev == NULL)  // Delete FIRST element
    {
        //System.out.println("Delete: " + D.key);
        nextNode = delNode->next;
        nextNode->prev = NULL;
        D->head = nextNode;  // The second is the new "first element"
    }
    else if (delNode->next == NULL)  // Delete LAST element
    {
        //System.out.println("Delete: " + D.key);
        prevNode = delNode->prev;
        prevNode->next = NULL;
    }
    else                              // Delete a "middle element"
    {
        //System.out.println("Delete: " + D.key);
        prevNode = delNode->prev;
        nextNode = delNode->next;
        nextNode->prev = delNode->prev;
        prevNode->next = delNode->next;
    }
    freeNode(&delNode);
    D->numItems--;
}

// makeEmpty()
// re-sets D to the empty state.
// pre: none
void makeEmpty(Dictionary D){
    
    int i, count;
    Node N, nextNode;

    count = D->numItems;

    N = D->head;

    for (i = 1; i < count+1; i++)
    {
        nextNode = N->next;
        delete(D, N->key); 
        N = nextNode;
    }
    
    D->head = NULL;
    D->back = NULL;
    D->numItems = 0;
}


// printDictionary()
// pre: none
// prints a text representation of D to the file pointed to by out
void printDictionary(FILE* out, Dictionary D){
    Node N;

    if (D == NULL){
        fprintf(stderr,"Dictionary Error: calling printDictionary() on NULL Dictionary reference\n");
        exit(EXIT_FAILURE);
    }
    for (N = D->head; N != NULL; N = N->next) fprintf(out, "\nkey:%s value:%s \n", N->key, N->value);
    fprintf(out, "\n");
}
