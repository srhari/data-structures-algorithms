import java.util.Arrays;

public class QuickSort{
	
	public void sort(int[] data){
		quicksort(data, 0, data.length-1);
	}

	public void quicksort(int[] data, int start, int end){
		if (start < end){
			int pivotIndex = partition(data, start, end);
			quicksort(data, start, pivotIndex-1);
			quicksort(data, pivotIndex+1, end);
		}
	}

	private int partition(int[] data, int start, int end){
		int pivot = data[end];
		int i = start;
		for (int j = start; j <= end-1; j++){
			if (data[j] < pivot){
				int temp = data[i];
				data[i] = data[j];
				data[j] = temp;	
				i++;
			}
		}
		data[end] = data[i];
	}
}
