class Stack{
	
	private DoublyLinkedNode top;
	
	public boolean isEmpty(){
		return top == null;
	}

	public void push(int item){
		DoublyLinkedNode node = new DoublyLinkedNode(item);
		node.setPrevNode(this.top);
		if (top != null){
			top.setNextNode(node);
		}
		top = node;
	}

	public int peek(){
		return this.top.getData();
	}

	public int pop(){
		int result = this.top.getData();
		if (this.top.getPrevNode() != null){
			this.top.getPrevNode().setNextNode(null);
		}
		this.top = this.top.getPrevNode();
		return result;
	}
}
