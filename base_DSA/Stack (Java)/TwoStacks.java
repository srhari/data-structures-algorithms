class TwoStacks{
	
	Object[] array = new Object[100];
	int top1 = -2, int top2 = -1;

	public void pushFirst(Object data){
		array[top1 += 2] = data;
	}

	public void pushSecond(Object data){
		array[top2 += 2] = data;
	}

	public Object peekFirst(){
		if (top1 >= 0){
			return array[top1];
		throw new RunTimeException("First stack is empty");
	}

	public Object peekSecond(){
		if (top2 >= 0){
			return array[top2];
		throw new RunTimeException("Second stack is empty");
	}

	public Object popFirst(){
		if (top1 < 0){
			throw new RunTimeException("First stack is empty");
		top1 -= 2;
		return array[top1 + 2];
	}

	public Object popSecond(){
		if (top2 < 0){
			throw new RunTimeException("Second stack is empty");
		top2 -= 2;
		return array[top2 + 2];
	}

	public boolean isFirstEmpty(){
		return top1 < 0;
	}

	public boolean isSecondEmpty(){
		return top2 < 0;
	}

}	
