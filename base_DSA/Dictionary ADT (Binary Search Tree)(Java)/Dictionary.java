/* Srihari Dasarathy (sdasarat) 1414679
 * CS12M Lab7
 * May 29th, 2015
 * Dictionary.java, Dictionary object using BST implementation
 */

public class Dictionary implements DictionaryInterface {

	private class Node{//initialization instance fields of class Node
		String key;
		String value;
		Node left;
		Node right;

		public Node(String alpha, String beta){//Node constructor
			key = alpha;
			value = beta; 
			left = null;
			right = null;
		}
	}

	//instance fields referencing head and #items
	private Node root;   
	private int numItems;

	public Dictionary(){
		root=null;
		numItems = 0;
	}

	Node getMin(Node R){
	Node A = null;
	   if(R != null){
		A = R;
		getMin(R.left);
	   }
	   return A;
	}

	Node getParent(String k){
		Node C = root;
		Node B = root;
		while( C!= null ){ 
			if(k.compareTo(C.key)==0){ 
				return B;
			}else{	
				B = C; 
				C = (k.compareTo(C.key)<0)?(C.left):(C.right);
			}
		}
		return null;
	}

	// returns Node reference to specified index
	private Node findKey(String k){
		Node C = root;

		while(C != null){ 
			if(k.compareTo(C.key)==0){
				 return C;
			}else{
				 C = ( k.compareTo(C.key)<0)?(C.left):(C.right);
			}
		}
		return C;
	}

	public Node delete(String k, Node N){
		Node M;
            	if(N == null){
                	return N;
		}  
		int guard = k.compareTo(N.key);
            	if(guard < 0){ 
			N.left = delete(k, N.left);
		}else if(guard > 0){ 
			N.right = delete(k, N.right);
            	}else if(N.left != null && N.right != null){
			M=getMin(N.right);
                	N.key = M.key;
			N.value=M.value;
                	N.right = delete( N.key, N.right );
            	}else{
                	N = (N.left != null) ? N.left : N.right;
		}
            	return N;
        }

	public boolean isEmpty(){
	if(numItems == 0) 
		return true;
	else
		return false;
	}

	public int size() {
		return numItems;
	}

	public String lookup(String keyIndex){
		Node N = findKey(keyIndex);
		if (N == null){
			return null;
		}
		return N.value;
	}

	public void insert(String k, String v) throws KeyCollisionException{
		Node N = new Node(k, v);
		Node P = null;
		Node C = root;
		if (lookup(k)!=null){
			throw new KeyCollisionException("Dictionary Error: insert() called on invalid key with collision: " + k);
		}
		while( C != null ){ 
			P = C;         
			C = (k.compareTo(C.key)<0)?(C.left):(C.right);
		}
		if(P == null){
			 root = N;
		}else if(k.compareTo(P.key)<0){
			P.left = N;
		}else{
			P.right = N;                 
		}
		numItems++;
	}

	public void delete(String k) throws KeyNotFoundException{
		Node N, M, P, succ; 
		if (lookup(k)==null){
			throw new KeyNotFoundException("Dictionary Error: delete() called on invalid key with no valid entry: " + k);
		}
		N = findKey(k);
		if(N == root){ 
			root = (delete(k, root));
		}else{
			N = delete(k,root);
		}
		numItems--;
	}

	public void makeEmpty(){
		root = null;
		numItems = 0;
	}

	public String toString(){
		StringBuffer sb = new StringBuffer();
		Node N = root;
		for (int i = 0; i < numItems; i++) {
		    sb.append(N.key).append(" ").append(N.value).append("\n");
			N = N.right;
		}
		return new String(sb);
	}

	public boolean equals(Object rhs){
		boolean eq = false;
		Dictionary R = null;
		Node N = null;
		Node O = null;

		if (rhs instanceof Dictionary){
			R = (Dictionary)rhs;
			eq = (this.numItems == R.numItems);

			N = this.root;
			O = R.root;
			while (eq && N != null){
				eq = (N.key == O.key);
				N = N.right;
				O = O.right;
			}
		}
		return eq;
	}

}

