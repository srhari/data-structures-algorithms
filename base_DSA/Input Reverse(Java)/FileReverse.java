/* Srihari Dasarathy
 * sdasarat #1414679
 * CS12B lab2
 * April 14th, 2015
 * FileReverse.java : Reads input, passes tokens and prints tokens backwards to output file. 
 */


import java.io.*;
import java.util.Scanner;

class FileReverse{
    public static void main(String[] args) throws IOException{
		
	Scanner in = null;
	PrintWriter out = null;
	String line = null;
	String[] token = null;
	int i, n, lineNumber = 0;

	if (args.length < 2){
	    System.out.println("Usage: FileReverse infile outfile");
	    System.exit(1);
	}

	in = new Scanner(new File(args[0]));
	out = new PrintWriter(new FileWriter(args[1]));

	while (in.hasNextLine()){
	    lineNumber++;
	    line = in.nextLine().trim() + " ";
	    token = line.split("\\s+");
	    //System.out.println();
	    n = token.length;
	    for(i = 0; i < n; i++){
	       String z = stringReverse(token[i],token[i].length());
	       out.println(z);
	    }
	    //stringReverse(line,n);
	    /*if (line.equals(" ")){
	        System.out.println();
	    }*/
	    System.out.println();
	}    
	
	in.close();
	out.close();
    }


    public static String stringReverse(String s, int n){
	//if (n > 0){
	if (n == 0){
	    return "";
	}
	    //System.out.print(s.substring(n-1));
	return s.charAt(n-1) + stringReverse(s,n-1);
    }
}
	
