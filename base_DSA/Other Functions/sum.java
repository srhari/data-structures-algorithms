public class sum{
    public static void main(String[] args){
	int n = 10;
	System.out.println(sum(n));
    }

    static int sum(int n){ //sum of integers from 1 to n
	if (n > 0){
	    return sum(n-1) + n;
	}
	else{
	    return 0;
	}
    }
}
