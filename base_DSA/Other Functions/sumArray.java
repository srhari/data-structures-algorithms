public class sumArray{
    public static void main(String[] args){
	int[] A = {1,2,3,4,5,6,7,8,9,10};
	int n = 8;
	int p = 0;
	int q = 9;
	System.out.println(one_sumArray(A,n));
	System.out.println(two_sumArray(A,n));
	System.out.println(three_sumArray(A,p,q));
    }

    static int one_sumArray(int[] A, int n){ //sum of leftmost elements in an array, ended by n
	if (n <= 0){
	    return 0;
	}else{
	    return one_sumArray(A, n-1) + A[n-1]; 
        }
    }
    
    static int two_sumArray(int[] A, int n){ //sum of rightmost elements in an array, begun by n
	if (n <= 0){
	    return 0;
	}else{
	    return A[A.length-n] + two_sumArray(A, n-1);
	}
    }

    static int three_sumArray(int[] A, int p, int r){ //splits the array into two subArrays, calculates two sums and returns the total sum 
	if (p > r){
	    return 0;
	}else if (p == r){
	    return A[p];
	}else{
	    int q = (p+r)/2;
	    return three_sumArray(A,p,q) + three_sumArray(A,q+1,r);
	}
    }
}
	    
	    
	


