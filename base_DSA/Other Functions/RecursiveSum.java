public class RecursiveSum{
	
	static int sum(int a, int b){
		if (a == 0){
			return b;
		return sum(a-1, b+1);
	}

	
	static int sumSquare(int n){	
		if (n == 0){
			return 0;
		return sumSquare(n-1) + (n*n);
	}

	static int factorialCalc(int n){
		if (n == 1){
			return 1;
		return n * factorialCalc(n-1);
	}

	
