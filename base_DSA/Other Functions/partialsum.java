public class partialsum{
    public static void main(String[] args){
        int n = 5;
	int m = 10;
	System.out.println(sum(n,m));
    }

    static int sum(int n, int m){ //sum of integers from n to m
	if (n > m){
	    return 0;
	}else if (n <= m){
	    return sum(n+1,m) + n;
	}else{
	    return 0;
	}
    }
}
