public class Anagram{
	
	public boolean isAnagram(String a, String b){
		if (a.length() != b.length()){
			return false;
		}else if (a.length() == b.length() == 0){
			return false;
		}else if(a.equals(b)){
			return false;
		}else if(a.length != 0){
			for (int i = 0; i < a.length(); i++){
				if (b.indexOf(a.charAt(i)) == -1){
					return false;
				}
			}
			
		}
		return true;
	}
