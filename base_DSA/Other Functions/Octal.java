public class Octal{
    static void displayOctal(int n){
	if (n > 0){
 	    if (n/8 > 0){
		displayOctal(n/8);
	    }
	    System.out.println(n%8);
	}
    }

    public static void main(String[] args){
	int n = 100;
	displayOctal(n);
    }
}
