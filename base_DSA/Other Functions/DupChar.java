public class DupChar{
	
	public static boolean hasDuplicateChars(String s){
		int[] chars = new int[128];
		char c;
		for (int i = 0; i < s.length(); i++){
			c = s.charAt(i);
			chars[c] = chars[c] + 1;
			if (chars[c] > 1){
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args){
		hasDuplicateChars("anaconda");
		hasDuplicateChars("viper");
	}
}
