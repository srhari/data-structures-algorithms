import java.util.Random;

class Employee{

	//String[] employee_list = new String[5];
	int eNumber;
	String firstName;
	String lastName;
	String email;
	 //only instance variable, in case of overlapping usage

	public Employee(String firstName, String lastName, String email, int eNumber){ //constructor creating employee object
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.eNumber = eNumber;
	}

	public static Employee[] store_in_array(int size){ //receives an object of type employee, stores it in an array, then returns the completed array.
		Employee[] employee_list = new Employee[size];
		Random random = new Random();
		for (int i = 0; i < size; i++){
			int eN = random.nextInt(1000000000);
			Employee A = new Employee("FirstName" + eN, "LastName" + eN, eN + "@facebook.com", eN);
			employee_list[i] = A;
			System.out.println(A.eNumber);
		}
		return employee_list;
	}

	public static Employee[] insertion_sort(Employee[] employee_list){ //receives the array of employees, sorts by ID number using insertion method. then returns the sorted array of objects.
		System.out.println("Sorted using Insertion Sort");
		Employee eTemp; 
		for (int i = 0; i < employee_list.length; i++){
			int j = i;
			while (j >= 1 && employee_list[j - 1].eNumber > employee_list[j].eNumber){
				eTemp = employee_list[j - 1];
				employee_list[j - 1] = employee_list[j];
				employee_list[j] = eTemp;
				j = j - 1;
			}
		}
		return employee_list;
	}
}
		
		
		
