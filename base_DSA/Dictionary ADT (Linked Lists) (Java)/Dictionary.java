/* Srihari Dasarathy
 * sdasarat 1414679
 * CS12B PA3
 * April 27th, 2015
 * Dictionary.java: 
 */

public class Dictionary implements DictionaryInterface{
    private class Node{
        String key;
        String value;
        Node next;
        Node(String k, String v){
            key = k;
            value = v;
            next = null;
        }
    }
    private Node head;
    private int numCount;
    private Node findKey(String key){
        Node N = head;
        for(; N != null; N = N.next){
            if(N.key==key){
                return N;
            }
        }
    return null;
    }
    public boolean isEmpty(){
      return(numCount == 0);
    }
    public int size(){
      return numCount;
    }
    public String lookup(String key){
        Node O = findKey(key);
        if(O != null){
            return O.value;
        }else{
            return null;
        }
    }
    public void insert(String key, String value) throws KeyCollisionException{
        if (lookup(key) != null){
            throw new KeyCollisionException("cannot insert duplicate keys");
        }
        if (head == null){
            Node O = new Node(key, value);
            O.next = head;
            head = O;
        }else{
            Node P = head;
            for(; P.next != null;P = P.next);
            P.next = new Node(key, value);
        }
        numCount++;
    }
    public void delete(String key) throws KeyNotFoundException{
        if (lookup(key) == null){
            throw new KeyNotFoundException("cannot delete non-existent key");
        }
        if (head.key == key){
            Node N = head;
            head = head.next;
            N.next = null;
        }else{
            Node P = head;
            Node Q = head;
            for(;P != null;P = P.next){
                if(P.key==key){
                    Q.next = P.next;
                    P.next = null;
                }else{
                    Q = P;
                }
            }
        }
        numCount--;
    }
    public void makeEmpty(){
        head = null;
        numCount = 0;
    }
    public String toString(){
        StringBuffer sb = new StringBuffer();
        Node N = head;
        for(;N != null;N = N.next){
            sb.append(N.key).append(" ").append(N.value).append("\n");
        }
        return new String(sb);
    }
}
