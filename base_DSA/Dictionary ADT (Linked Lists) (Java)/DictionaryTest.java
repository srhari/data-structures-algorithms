/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B April 28th, 2015
 * PA3
 * DictionaryTest.java, test file for Dictionary ADT
 */

public class DictionaryTest{
    public static void main(String[] args){
	Dictionary A = new Dictionary();
	Dictionary B = new Dictionary();

	System.out.println(A.isEmpty());
	System.out.println(B.isEmpty()); //tests whether or not the list is empty, should return true because A is declared but not initialized.
	A.insert("1","z"); //add to dictionary A
	B.insert("5","f"); //add to dictionary B
	System.out.println(B.isEmpty());
	B.makeEmpty();
	System.out.println(B.isEmpty()); //try again, SHOULD return false
	A.insert("2","w"); //add to A
	A.insert("3","x");
	A.insert("4","v");
	System.out.println(A.isEmpty()); //try again, should return false, checks after multiple consecutive insert statements
	A.insert("2","b"); //tests the KeyCollisionException
	A.delete("10"); //KeyNotFoundException
	System.out.println(B.toString()); //convert to a String
	String l = A.lookup("1"); //Test lookup
	String s = A.lookup("2");
	System.out.println("key = 1 " + (l == null?"not found":("value = " + l)));
	System.out.println("key = 2 " + (s == null?"not found":("value = " + s)));
	System.out.println(A.toString()); //convert to String and print
	A.delete("3"); //delete value at key 3
	System.out.println(A.toString()); //convert to Strings
	System.out.println(B.toString());
	B.makeEmpty(); //test makeEmpty
	System.out.println(B.toString()); //convert to String & print, test makeEmpty
			
    }
}
