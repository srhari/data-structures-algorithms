/* Srihari Dasarathy (sdasarat) 14141679
 * CS12B PA3
 * April 29th, 2015
 * KeyCollisionException.java, throws exception for parent class in Dictionary
 */

public class KeyCollisionException extends RuntimeException{
    public KeyCollisionException(String s){
	super(s);
    }
}
