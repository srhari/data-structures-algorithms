/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B PA3
 * April 29th, 2015
 * KeyNotFoundException.java, throws exception for missing key
 */

public class KeyNotFoundException extends RuntimeException{
    public KeyNotFoundException(String s){
	super(s);
    }
}
