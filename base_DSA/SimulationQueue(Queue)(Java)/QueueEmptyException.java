/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B PA4
 * May 18th, 2015
 * QueueEmptyException, exception file for class Queue
 */

public class QueueEmptyException extends RuntimeException{
	public QueueEmptyException(String s){
		super(s);
	}
}
