/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B PA4
 * May 10th, 2015
 * Simulation.java, Simulates Queues
 */
import java.io.*;
import java.util.Scanner;
public class Simulation{
    
    public static Job getJob(Scanner input) {
	int a = input.nextInt();
	int d = input.nextInt();
	return new Job(a, d);
    }
    public static void simulateProcessor(int proCount, Queue readyQueue, PrintWriter trace) {
	trace.println("*****************************");
	String t = proCount > 1 ? "processors" : "processor";
	trace.println("" + proCount + t + ":");
	trace.println("*****************************");
	// create proCount job queues
	Queue processorQueues[] = new Queue[proCount];
	for (int k=0;k<proCount; k++) {
	    processorQueues[k] = new Queue();
	}
	int time = 0;
	int jobsToSubmit = readyQueue.size();
	while(true) {
	    boolean changed = false;
	    // for each processorQueue, check if a job can be pulled output of the processorQueues and be pushed 
	    // into the doneQueue
	    int emptyCount = 0;
	    for (int i=0;i<proCount;i++) {
		if(processorQueues[i].isEmpty()){
		    emptyCount++;
		    continue;
		}
		Job k = processorQueues[i].peek();
		if (k.getFinish() == time){
		    readyQueue.enqueue(processorQueues[i].dequeue());
		    changed = true;
		}                         
	    }
	    // pick the top entry and if (arrival time == tick) 
	    // then pop the top entry and push it into a processorQueue whose length is the smallest
	    // if two processorQueues have the same length, then choose the one with the lowest index
	    while(jobsToSubmit > 0) {
		Job f = readyQueue.peek();
		if (f.getArrival() == time){
		    readyQueue.dequeue();
		    jobsToSubmit--;
		    Queue minQueue = processorQueues[proCount-1]; 
		    for (int i = proCount-2; i>=0; i--){
			if (processorQueues[i].size() <= minQueue.size()) {
			    minQueue = processorQueues[i];
			}
		    }
		    minQueue.enqueue(f);
		    changed = true;
		} else {
		    break;
		}
	    }
	    for (int i=0;i<proCount;i++) {
		if (processorQueues[i].isEmpty()) continue;
		Job f = processorQueues[i].peek();
		if (f.getFinish() == Job.UNDEF) {
		    //System.output.println("Updating finish time for Job: "+f);
		    f.computeFinishTime(time);
		    //System.output.println("Updated to Job: "+f);
		}
	    }
	    if (changed) {
		// now print stuff to trace
		trace.println("time=" + time);
		trace.println("0: " + readyQueue);
		for (int k = 1; k < proCount+1; k++){
		    trace.println("" + k + ": " + processorQueues[k-1]);
		}
	    }
                          
	    if((jobsToSubmit == 0) && (emptyCount == proCount)) break; 
	    time++;
	}
    }
    public static void mainPWTest(String[] args) throws IOException{
	PrintWriter out0 = new PrintWriter(new FileWriter(args[0]+".rpt"));
	PrintWriter out1 = new PrintWriter(new FileWriter(args[0]+".trc"));
	out0.println("This is rpt");
	out1.println("This is trc");
	out0.close();
	out1.close();
    }
    public static void main(String[] args) throws IOException{
	Scanner input = null;
	PrintWriter output = null;
	PrintWriter trace = null;
	String line = null;
	int n;
	
	input = new Scanner(new File(args[0]));
	output = new PrintWriter(new FileWriter(args[0]+ ".rpt"));
	String traceFile = args[0] + ".trc";
	trace = new PrintWriter(new FileWriter(traceFile));
                
	int numJobs = input.nextInt();
	Queue readyQueue = new Queue();
	output.println(numJobs+" jobs");
	for(int i = 0; i < numJobs; i++){ 
	    Job j = getJob(input);
	    readyQueue.enqueue(j);
	    output.print(j);
	}
	output.println();
	output.println("***********************************************************");
	trace.println("***********************************************************");
	trace.println("" + numJobs + " Jobs:");
	trace.print(readyQueue);
	trace.println();
	for (int proCount=1; proCount < numJobs; proCount++) {  
	    simulateProcessor(proCount, readyQueue, trace);
	    String p = proCount > 1 ? "processors" : "processor";
	    output.print("" + proCount + p +": totalWait = " + readyQueue.getTotalWaitTime() + ",");
	    output.print(" maxWait = " + readyQueue.getMaxWaitTime() + ", avgWait = " + String.format("%.2f", readyQueue.getAvgWaitTime()));
	    output.println();
	    readyQueue.reset();
	}
	input.close();
	output.close();
	trace.close();
    }
}