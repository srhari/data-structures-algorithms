/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B PA4
 * May 10th, 2015
 * QueueTest.java, test file for class Queue
 */

public class QueueTest{
	public static void main(String[] args){
		Queue A = new Queue();
		A.enqueue(new Job(5,5));
	        A.enqueue(new Job(3,3));
	        A.enqueue(new Job(9,9));
		A.enqueue(new Job(7,7));
		A.enqueue(new Job(8,8));
		System.out.println(A);
		System.out.println(A.peek());
		A.dequeue();
		A.dequeue();
		System.out.println(A.peek());
		System.out.println(A);

		Queue B = new Queue();
		System.out.println(A.isEmpty());
		System.out.println(B.isEmpty());
		B.enqueue(new Job(7,7));
		B.enqueue(new Job(9,9));
		A.enqueue(new Job(15,15));
		System.out.println(A);
		System.out.println(A.equals(B));
		A.dequeueAll();
		System.out.println(A.isEmpty());
	}
}
