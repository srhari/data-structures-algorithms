/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B PA4
 * May 18th, 2015
 * QueueInterface , interface file for Queue ADT
 */

public interface QueueInterface{

	public boolean isEmpty();
	public int length();
	public void enqueue(Job newItem);
	public Object dequeue() throws QueueEmptyException;
	public Object peek() throws QueueEmptyException;
	public void dequeueAll() throws QueueEmptyException;
	public String toString();
}
