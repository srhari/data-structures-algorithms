/* Srihari Dasarathy (sdasarat) 1414679
 * CS12B PA4
 * May 10th, 2015
 * Queue.java, implements Queue Interface
 */
import java.util.*;
public class Queue implements QueueInterface{
    private class Node{ //Node class
	Job job;
	Node next;
	Node(Job job){
	    this.job = job;
	    this.next = null;
	}
    }
    private Node lastNode; //last Node reference
    private Node firstNode; //reference to the first Node in the queue
    private int numJobs; //number of jobs to be done
    public Queue(){ //default constructor for class JobQueue
	lastNode = null;
	firstNode = null;
	numJobs = 0;
    }
    public boolean isEmpty(){
	return firstNode == null;
    }
    public int size(){
	return numJobs;
    }
    public int length(){
	return numJobs;
    }
    public Job peek() throws QueueEmptyException{
	if (isEmpty()) throw new RuntimeException("Queue underflow");
	return firstNode.job;
    }
    public void enqueue(Job job){
	Node N = new Node(job);
	N.job = job;
	if (isEmpty()){
	    firstNode = N;
	    lastNode = N;
	}else{
	    lastNode.next = N;
	    lastNode = N;
	}
	numJobs++;
    }
    public Job dequeue() throws QueueEmptyException{
	if (isEmpty()){
	    throw new RuntimeException("Queue underflow");
	}
	Job job = firstNode.job;
	firstNode = firstNode.next;
	numJobs--;
	if (isEmpty()){
	    lastNode = null;
	}
	return job;
    }
    public void dequeueAll() throws QueueEmptyException{
	if (isEmpty()){
	    throw new RuntimeException("Queue is already empty.");
	}
	numJobs = 0;
	firstNode = null;
	lastNode = null;
    }
    public int getMaxWaitTime() {
	if (isEmpty()){
	    throw new RuntimeException("Queue is already empty.");
	}
	int maxWaitTime = firstNode.job.getWaitTime();
	Node m = firstNode.next;
	while (m != null){
	    if (m.job.getWaitTime() > maxWaitTime) {
		maxWaitTime = m.job.getWaitTime();
	    }
	    m = m.next;
	}
	return maxWaitTime;
    }
    public int getTotalWaitTime() {
	if (isEmpty()){
	    throw new RuntimeException("Queue is already empty.");
	}
	int totalWaitTime = 0;
	Node m = firstNode;
	while (m != null) {
	    totalWaitTime += m.job.getWaitTime(); 
	    m = m.next;
	}
	return totalWaitTime;
    }
    public double getAvgWaitTime() {
	if (isEmpty()){
	    throw new RuntimeException("Queue is already empty.");
	}
	return (double)getTotalWaitTime()/(double)size();
    }
    public void reset(){
	Node m = firstNode;
	while (m != null){
	    m.job.resetFinishTime();
	    m = m.next;
	}
    }
    public String toString(){
	String s = new String("");
	Node m = firstNode;
	while (m != null){
	    s += m.job.toString() + " ";
	    m = m.next;
	}
	return s;
    }
}