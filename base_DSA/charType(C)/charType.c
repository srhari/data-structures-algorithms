/* Srihari Dasarathy
 * sdasarat 1414679
 * CS12M Lab4
 * May 2nd, 2015
 * charType.c, character classification
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<assert.h>
#define MAX_STRING_LENGTH 100

//assert(line != NULL && alpha != NULL && dig != NULL && punc != NULL && white != NULL);
void extract_chars(char* s, char* a, char* d, char* p, char* w);

int main(int argc, char* argv[]){
	FILE* in;
	FILE* out;
	//char* word[256];
	int lineCount = 0;
	char* alpha;
	char* dig;
	char* punc;
	char* white;
	char* line;
	

	if (argc != 3){
		printf("Usage: %s <input file> <output file>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	in = fopen(argv[1], "r");
	if (in == NULL){
		printf("Unable to read from file %s\n", argv[1]);
		exit(EXIT_FAILURE);	
	}

	out = fopen(argv[2], "w");
	if (out == NULL){
		printf("Unable to write to file %s\n", argv[2]);
		exit(EXIT_FAILURE);
	}

	line = calloc(MAX_STRING_LENGTH+1, sizeof(char));
	alpha = calloc(MAX_STRING_LENGTH+1, sizeof(char));
	dig = calloc(MAX_STRING_LENGTH+1, sizeof(char));
	punc = calloc(MAX_STRING_LENGTH+1, sizeof(char));
	white = calloc(MAX_STRING_LENGTH+1, sizeof(char));
	assert(line != NULL && alpha != NULL && dig != NULL && punc != NULL && white != NULL);

	while(fgets(line, MAX_STRING_LENGTH, in) != NULL){
		extract_chars(line, alpha, dig, punc, white);
		fprintf(out, "line %d contains: \n", lineCount);
		if (strlen(alpha) == 1){
			fprintf(out, "%ld alphabetic character: %s\n", strlen(alpha), alpha);
		}else{
			fprintf(out, "%ld alphabetic characters: %s\n", strlen(alpha), alpha);
		}
		if (strlen(dig) == 1){
                        fprintf(out, "%ld numeric character: %s\n", strlen(dig), dig);
		}else{
			fprintf(out, "%ld numeric characters: %s\n", strlen(dig), dig);
		}
		if (strlen(punc) == 1){
                	fprintf(out, "%ld punctuation character: %s\n", strlen(punc), punc);
		}else{
			fprintf(out, "%ld punctuation characters: %s\n", strlen(punc), punc);
		}
		if (strlen(white) == 1){
                	fprintf(out, "%ld whitespace character: %s\n", strlen(white), white);
		}else{
			fprintf(out, "%ld whitespace characters: %s\n", strlen(white), white);
		}
		lineCount++;
	}

	/*while(fscanf(in, " %s", word) != EOF){
		extract_chars(word, alpha, dig, punc, white);
	}*/

	free(line);
	free(alpha);
	free(dig);
	free(punc);
	free(white);

	fclose(in);
	fclose(out);

	return(EXIT_SUCCESS);
}

void extract_chars(char* s, char* a, char* d, char* p, char* w){
	//char str[1024];
	int i = 0, j = 0, k = 0, l = 0, m = 0;
	//while(fgets(str, sizeof(str), in) != NULL){
	while(s[i] != '\0' && i < MAX_STRING_LENGTH){
		//lineCount++;
		if (isdigit((int)s[i])){
			d[j++] = s[i];
			i++;
		}
		else if (isalpha(s[i])){
			a[k++] = s[i];
			i++;
		}
		else if (ispunct(s[i])){
			p[l++] = s[i];
			i++;
		}
		else if (isspace(s[i])){
			w[m++] = s[i];
			i++;
		}
		d[j] = '\0';
		a[k] = '\0';
		p[l] = '\0';
		w[m] = '\0';
		/*printf("line %d contains:\n", lineCount);
		printf("%d alphabetic characters: ", a[j]);
		for (int q = 0; w < aCount; a++){
			printf(a[q] + " ");
		}
		printf("\n%d numeric characters: ", d[j]);
		for (int x = 0; x < dCount; x++){
			printf(d[x] + " ");
		}
		printf("\n%d punctuation characters: ", p[j]);
		for (int y = 0; y < pCount; y++){
			printf(p[y] + " ");
		}
		printf("\n%d whitespace characters: ", w[j]);
		for (int z = 0; z < wCount; z++){
			printf(w[z] + " ");
		}*/
	}
}		
	
	  
