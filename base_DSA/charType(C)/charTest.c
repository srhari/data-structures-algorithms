#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<assert.h>
#define MAX_STRING_LENGTH 100

void extract_chars(char* s, char* a, char* d, char* p, char* w){
	char str[1024];
	int i, j = 0;
	int lineNumber = 0;
	while (fgets(str, sizeof(str), in) != NULL){
                lineNumber++;
                if (isdigit((int)s[i])){
                        d[j++] = s[i];
                        i++;
                }
                else if (isalnum(s[i])){
                        a[j++] = s[i];
                        i++;
                }
                else if (ispunct(s[i])){
                        p[j++] = s[i];
                        i++;
                }
                else if (isspace(s[i])){
                        w[j++] = s[i];
                        i++;
                }
                printf("line %d contains:", lineNumber);
                printf("%d alphabetic characters: ", a[j]);
                printf("%d numeric characters: ", d[j]);
                printf("%d punctuation characters: ", p[j]);
                printf("%d whitespace characters: ", w[j]);
        }
}


int main(int argc, char* argv[]){
 	FILE* in; /* file handle for input */
 	FILE* out; /* file handle for output */
 	char word[256]; /* char array to store words from input file */
    char* lineNumber;
    char* alpha;      
    char* dig;
    char* punc;
    char* white;
 /* check command line for correct number of arguments */

 if( argc != 3 ){
 	printf("Usage: %s <input file> <output file>\n", argv[0]);
 	exit(EXIT_FAILURE);
 	}
 /* open input file for reading */
 in = fopen(argv[1], "r");
 if( in==NULL ){
 	printf("Unable to read from file %s\n", argv[1]);
 	exit(EXIT_FAILURE);
 	}
 /* open output file for writing */
 out = fopen(argv[2], "w");
 if( out==NULL ){
 	printf("Unable to write to file %s\n", argv[2]);
 	exit(EXIT_FAILURE);
	}
 /* read words from input file, print on separate lines to output file*/
    lineNumber = calloc(MAX_STRING_LENGTH+1, sizeof(char));
    alpha = calloc(MAX_STRING_LENGTH+1, sizeof(char));
    dig = calloc(MAX_STRING_LENGTH+1, sizeof(char));
    punc = calloc(MAX_STRING_LENGTH+1, sizeof(char));
    white = calloc(MAX_STRING_LENGTH+1, sizeof(char));
    assert(lineNumber != NULL && alpha != NULL && dig != NULL && punc != NULL && white != NULL);

 while( fscanf(in, " %s", word) != EOF ){
 	extract_chars(word, alpha, dig, punc, white);
 	}

 /* close input and output files */
	fclose(in);
 	fclose(out);
 	return(EXIT_SUCCESS);
}
