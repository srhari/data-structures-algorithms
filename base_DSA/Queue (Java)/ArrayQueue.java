class ArrayQueue{
	
	private int[] items = new int[5];
	private int head = -1;
	private int tail = -1;
 	private int numberOfItems = 0;

	public ArrayQueue(){}

	public ArrayQueue(int size){
		this.items = new int[size];
	}

	public void enqueue(int item){
		if (isFull())
			throw new RuntimeException("Queue is full");
		if (tail == items.length-1){
			tail = -1;
		items[++tail] = item;
		if (head == -1){
			head++;
		numberOfItems++;
	}

	
