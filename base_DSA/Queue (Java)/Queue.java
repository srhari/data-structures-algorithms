class Queue{
	
	private Node firstNode;
	private Node lastNode;
	public int size;

	public Queue(){
		this.firstNode = null;
		this.lastNode = null;
		this.size = 0;
	}

	public boolean isEmpty(){
		return (first == null);
	}

	public int getSize(){
		return this.size;
	}

	public void enqueue(int data){
		Node x = new Node(data);
		if (lastNode == null){
			firstNode = x;
			lastNode = x;
		}else{
			lastNode = lastNode.setNextNode(x);
		}
		size++;
	}
		
	public int dequeue(){
		if (isEmpty())
			throw new NoSuchElementException("Underflow error");
		Node y = firstNode;
		firstNode = y.getNextNode();
		if (y.getNextNode() == null){
			lastNode = null;
		size--;
		return y.getData();
	}
}
