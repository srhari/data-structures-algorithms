class LeastHeightBST{
	
	public static void main(String[] args){
		int[] data = {6,7,28,84,112,212,434,580};
		BinarySearchTree bst = BinarySearchTree.createFromSortedArray(data);
		System.out.println(bst.height());
		bst.traverseInOrder();
	}
}
