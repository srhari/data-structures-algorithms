import java.util.Arrays;

public class Heap{
		
	private int[] heapData;
	private int currPosition = -1;

	public Heap(int size){
		this.heapData = new int[size];
	}

	public void insert(int item){
		if (isFull()) throw new RuntimeException("Heap is full");
		this.heapData[++currPosition] = item;
		fixUp(currPosition);
	}

	public int deleteRoot(){
		int result = heapData[0];
		heapData[0] = heapData[currPosition--];
		heapData[currPosition+1] = null;
		fixDown(0, -1);
		return result;
	}

	private void fixDown(int index, int upto){
		if (upto < 0) upto = currPosition;
		while (index <= upto){
			int leftChild = 2*index+1;
			int rightChild = 2*index+2;
			if (leftChild <= upto){
				int childToSwap;
				if (rightChild > upto)
					childToSwap = leftChild;
				else
					childToSwap = (heapData[leftChild] > heapData[rightChild]) ? leftChild : rightChild;
				if (heapData[index] < heapData[childToSwap]){
					int tmp = heapData[index];
					heapData[index] = heapData[childToSwap];
					heapData[childToSwap] = tmp;
				}else{
					break;
				}
				index = childToSwap;
			}else{
				break;
			}
		}
	}

	private void fixUp(int index){
		int i = (index-1)/2;
		while (i >= 0 && heapData[i] < heapData[index]){
			int tmp = heapData[i];
			heapData[i] = heapData[index];
			heapData[index] = tmp;
			index = i;
			i = (index-1)/2;
		}
	}
	
	private boolean isFull(){
		return currPosition == heapData.length-1;
	}

	public void heapSort(){
		for (int i=0; i < currPosition; i++){
			int tmp = heapData[0];
			heapData[0] = heapData[currPosition-1];
			heapData[currPosition-1] = tmp;
			fixDown(0, currPosition-i-1);
		}
	}

	public String toString(){
		return Arrays.deepToString(this.heapData);
	}
}
