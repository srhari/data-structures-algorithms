public class BinarySearch{
    static int binarySearch(int[] A, int p, int r, int target){ //searches for a number in an array, assuming the array is already sorted.
	int q;
	if (p > r){
	    return -1;
	}else{
	    q = (p+r)/2;
	    if (target == A[q]){
		System.out.println("target found at index " + q);
		return q;
	    }else if (target < A[q]){
	        System.out.println("not " + A[q]);
	        return binarySearch(A, p, q-1, target);
	    }else{
	        System.out.println("not " + A[q]);
	 	return binarySearch(A, q+1, r, target);
	    }
	}
    }

    public static void main(String[] args){
	int[] A = {1,2,3,4,5,6,7,8,9,10};
	int target = 7;
	int p = 0;
	int r = 9;
	binarySearch(A,p,r,target);
    }
}
