public class TreeNode{
		
	private int data;
	private TreeNode leftChild;
	private TreeNode rightChild;

	public TreeNode (int data){
		this.data = data;
	}

	public static TreeNode addSorted(int[] data, int start, int end){
		if (end >= start){
			int mid = (start+end)/2;
			TreeNode newNode = new TreeNode(data[mid]);
			newNode.leftChild = addSorted(data, start, mid-1);
			newNode.rightChild = addSorted(data, mid+1, end);
			return newNode;
		}
		return null;
	}

	public int height(){
		if (isLeaf()) return 1;
		int left = 0;
		int right = 0;
		if (this.leftChild != null) left = this.leftChild.height();
		if (this.rightChild != null) right = this.rightChild.height();
		return (left > right) ? (left + 1) : (right + 1);
	}

	public int numOfLeafNodes(){
		if (isLeaf()) return 1;
		int leftLeaves = 0;
		int rightLeaves = 0;
		if (this.leftChild != null) leftLeaves = leftChild.numOfLeafNodes();
		if (this.rightChild != null) rightLeaves = rightChild.numOfLeafNodes();
		return leftLeaves + rightLeaves;
	}

	public boolean isLeaf(){
		return this.leftChild == null && this.rightChild == null;
	}

	public void traverseInOrder(){
		if (this.leftChild != null) this.leftChild.traverseInOrder();
		System.out.println(this + " ");
		if (this.rightChild != null) this.rightChild.traverseInOrder();
	}

	public TreeNode find (int data){
		if (this.data == data){
			return this;
		}
		else if (data < this.data && leftChild != null){
			return leftChild.find(data);
		}
		else if (rightChild != null){
			return rightChild.find(data);
		}
		return null;
	}

	public void insert (int data){
		if (data >= this.data){
			if (this.rightChild == null){
				this.rightChild = new TreeNode(data);
			}else{
				this.leftChild.insert(data);
			}
		}else{
			if (this.leftChild == null){
				this.leftChild = new TreeNode(data);
			}else{
				this.leftChild.insert(data);
			}
		}
	}

	public int largest(){
		if (this.rightChild == null){
			return this.data;
		return this.rightChild.largest();
	}

	public int smallest(){
		if (this.leftChild == null){
			return this.data;
		return this.leftChild.smallest();
	}

	public int getData(){
		return data;
	}

	public treeNode getLeftChild(){
		return leftChild;
	}

	public treeNode getRightChild(){
		return rightChild;
	}

	public void setLeftChild(TreeNode left){
		this.leftChild = left;
	}

	public void setRightChild(TreeNode right){
		this.rightChild = right;
	}
	
	public String toString(){
		return String.valueOf(this.data);
	}
}
