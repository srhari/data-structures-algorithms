/* Srihari Dasarathy
 * sdasarat #1414679
 * CS12B PA2
 * April 15th, 2015
 * Search.java
 */

import java.io.*;
import java.util.Scanner;

public class Search{
    public static void main(String[] args) throws IOException{
        Scanner in = null;
        String line = null;
        int i, n, lineNumber = 0;

	if (args.length < 2){
	    System.out.println("Usage: Search file target1 [target2 target3 ..]");
	    System.exit(1);
	}

	in = new Scanner(new File(args[0]));
	while (in.hasNextLine()){
	    line = in.nextLine();
	    lineNumber++;
	}
        in.close();
	in = new Scanner(new File(args[0]));
	String[] word = new String[lineNumber];
	int[] counter = new int[lineNumber];
	for (i = 0; i < lineNumber; i++){
	    counter[i] = i+1;
	}
	while (in.hasNextLine()){
	    for (i = 0; i < lineNumber; i++){
		word[i] = in.nextLine();
	    }
	}    

	//int lineNumber = new int[alpha.size];
	/*String[] lineArray = str.split(" ");
	for (int i = 0; i < alpha.length(); i++){
	    lineArray[i] = in.next();
	}*/
	    //text on that line 

	mergeSort(word, counter, 0, word.length-1);
	n = args.length;
	for (i = 1; i < n; i++){
	    if (binarySearch(word, 0, word.length-1, args[i]) == -1){
	        System.out.println(args[i] + " not found");
	    }else{
	        System.out.println(args[i] + " was found on line " + counter[binarySearch(word, 0, word.length-1, args[i])]); /*binarySearch(word, 0, word.length-1, args[i]));*/ 
	    }
	}
	/*for (i = 0; i < counter.length; i++){
	    System.out.print(counter[i] + " ");
	}*/
    }

    public static void mergeSort (String[] word, int[] lineNumber, int p, int r){
        int q;
	if (p < r){
	    q = (p + r)/2;
	    mergeSort(word, lineNumber, p, q);
	    mergeSort(word, lineNumber, q+1, r);
	    merge(word, lineNumber, p, q, r);
	}
    }

    public static void merge (String[] word, int[] lineNumber, int p, int q, int r){
	int n1 = q - p + 1;
	int n2 = r - q;
	String[] L = new String[n1];
	String[] R = new String[n2];
	int[] L1 = new int[n1];
	int[] R1 = new int[n2];
	int i, j, k;

	for (i = 0; i < n1; i++){
	    L[i] = word[p + i];
	    L1[i] = lineNumber[p + i];
	}
	for (j = 0; j < n2; j++){
	    R[j] = word[q + j + 1];
	    R1[j] = lineNumber[q + j + 1];
	}
	i = 0; j = 0;
	for (k = p; k <= r; k++){
	    if (i < n1 && j < n2){
		if (L[i].compareTo(R[j]) < 0){
		    word[k] = L[i];
		    lineNumber[k] = L1[i];
		    i++;
		}else{
		    word[k] = R[j];
		    lineNumber[k] = R1[j];
		    j++;
		}
	    }else if (i < n1){
		word[k] = L[i];
		lineNumber[k] = L1[i];
		i++;
	    }else{
		word[k] = R[j];
		lineNumber[k] = R1[j];
		j++;
	    }
	}
    }
	
    public static int binarySearch (String[] word, int p, int r, String target){
	int q;
	if (p > r){
	    return -1;
	}else{
	    q = (p + r)/2;
            if (target.equals(word[q])){
		return q;
	    }else if (target.compareTo(word[q]) < 0){
	    	return binarySearch(word, p, q-1, target);
	    }else{
		return binarySearch(word, q+1, r, target);
	    }
	}
    }
}
	    









	//determine number of lines in the input file
	    //if text on next line, count++, keeps track of number of lines
	//store the words in an array
	//mergeSort: lexically order the lines (each word)
	    //check first letter (charAt) a>b>c..., etc.
	//store the sorted words in another array
	//search the sorted array for target words by using the lexical ordering search method / binary search
	    //if no such word is found, print statement
	    //if such a word is found, print the word found and the line number 
	
    

