public class ExtremaTest{
	static int maxArray(int[] A, int p, int r){
		if(p == r){
			return A[p];
		}
		else if(p < r){
			int q;
			q = (p+r)/2;
			int max1 = maxArray(A, A[p], A[q]);
			int max2 = maxArray(A, A[q+1], A[r]);
			return max(max1, max2);
			
		}
	}
		

	static int minArray(int[] A, int p, int r){
		if(p == r){
			return A[p];
		}
		else if (p < r){
			int q;
			q = (p+r)/2;
			int min1 = minArray(A, A[p], A[q]);
			int min2 = minArray(A, A[q+1], A[r]);
			return min(min1, min2);
		}
	}
	
	static int max(int a, int b){
		if (a < b){
			return b;
		}
		else{
			return a;
		}
	}
	static int min(int a, int b){
		if (b < a){
			return b;
		}
		else{
			return a;
		}
	}
	
	public static void main(String[] args){
		int[] B = {-1, 2, 6, 3, 9, 2, -3, -2, 11, 5, 7};
		System.out.println( "max = " + maxArray(B, 0, B.length-1) );
		System.out.println( "min = " + minArray(B, 0, B.length-1) );
	}

	
}

