//Srihari Dasarathy
//sdasarat 1414679
//CMPS 12B Pa1
//MaxMin
//Finds the maximum and minimum values in an array by dividing the arrays into two parts recusively.
//

import java.util.*;

public class MaxMin{  //main method, test array & print
    public static void main(String[] args){
	int[] B = {-1,2,6,3,9,2,-3,-2,11,5,7};
	System.out.println( "max = " + maxArray(B, 0, B.length-1));
	System.out.println( "min = " + minArray(B, 0, B.length-1));
    }

    //variables: q = middle index, p = left index, r = right index
    static int maxArray(int[] A, int p, int r){
	int counter;
	if (p > r){ //account for logic error
	    return -1;
	}else if (p == r){
	    return A[p];
	}else if (p < r){
	    int q;
	    q = (p + r) / 2; //assign q based on p and r
	    int alpha_max = maxArray(A,A[p],A[q]);
	    int delta_max = maxArray(A, A[q+1], A[r]);
	    return getMax(alpha_max, delta_max);
	    if (alpha_max < delta_max){
		counter = 1;
	    }else{
		counter = 0;
	}
	if (counter == 1){
	    return alpha_max;
	}else{
	    return delta_max;
	}
    }

    static int minArray(int[] A, int p, int r){
	if (p > r){
	   return -1;
	}else if (p == r){
	    return A[p];
	}else if (A[p] < A[r]){
	    int q;
	    q = (p + r) / 2;
	    int alpha_min = minArray(A, A[p], A[q]);
	    int delta_min = minArray(A, A[q+1], A[r]);
	    return getMin(alpha_min, delta_min);
	    if (alpha_max < delta_max){
	        counter = 1;
	    }else{
	        counter = 0;
	    }
	}
	if (counter == 1){
	    return alpha_min;
	}else{
	    return delta_min;
	}
    }

    static int getMax(int alpha, int delta){
	if (alpha > delta){
	    return alpha;
	}else{
	    return delta;
	}
    }

    static int getMin(int alpha, int delta){
	if (alpha < delta){
	    return alpha;
	}else{
	    return delta;
	}
    }
}



