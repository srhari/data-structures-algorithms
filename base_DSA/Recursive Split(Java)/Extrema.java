/*Srihari Dasarathy
 * sdasarat #1414679
 * April 10th, 2015
 * CS12B PA1
 * Extrema.java: Recursively splits an array to calculate its maximum and minimum values.*/

public class Extrema{
    public static void main(String[] args){
        int[] B = {-1,2,6,3,9,2,-3,-2,11,5,7};
        System.out.println("max = " + maxArray(B,0,B.length-1));
        System.out.println("min = " + minArray(B,0,B.length-1));
    }

    static int maxArray(int[] A, int p, int r){
        int q;
	if (p == r){
	    return A[p];
	}else{
	    q = (p + r)/2;
	    return getMax(maxArray(A,p,q),maxArray(A,q+1,r));
	}
    }

    static int minArray(int[] A, int p, int r){
        int q;	
	if (p == r){
	    return A[p];
	}else{
	    q = (p+r)/2;
	    return getMin(minArray(A,p,q),minArray(A,q+1,r));	     
	}		
    }
	
    static int getMax(int a, int b){
        if (a < b){
	    return b;
	}else{
            return a;			
	}
    }
	
    static int getMin(int a, int b){
        if (b < a){
	    return b;
	}else{
	    return a;
	}
    }
}	
