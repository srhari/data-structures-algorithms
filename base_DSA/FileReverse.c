/* Srihari Dasarathy (sdasarat) 1414679
 * CS12M Lab3
 * April 20th, 2015
 * FileReverse
 * reverses words from input file, prints to output file
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void stringReverse(char* s);

int main (int argc, char* argv[]){
    FILE* in;
    FILE* out;
    char word[256];

    if (argc != 3){
	printf("Usage: %s <input file> <output file>\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    in = fopen(argv[1], "r");
    if (in == NULL){
	printf("Unable to read from file %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }

    out = fopen(argv[2], "w");
    if (out == NULL){
	printf("Unable to write to file %s\n", argv[2]);
	exit(EXIT_FAILURE);
    }

    while (fscanf(in, " %s", word) != EOF){
	stringReverse(word);
	fprintf(out, "%s\n", word);
    }

    fclose(in);
    fclose(out);

    return(EXIT_SUCCESS);
}

void stringReverse(char* s){
    //set two varibales i and j to i = 0, j = strlen(s)-1
    int i = 0;
    int j = strlen(s) - 1;
    //swap the characters s[i] and s[j]
    while (i < j){
        char temp = s[i];
        s[i] = s[j];
        s[j] = temp;
	i++;
	j--;
	//printf("%s\n",s);
    }
}
    //increment i and decrement j
    //stop when i>=j    
