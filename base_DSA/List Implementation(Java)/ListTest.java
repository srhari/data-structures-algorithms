/* Srihari Dasarathy (sdasarat) 1414679
 * CS12M Lab6
 * May 14th, 2015
 * ListTest.java, test file for list ADT
 */

import java.util.*;

public class ListTest
{
   public static void main(String[] args)
   {
		List<String> alpha = new List<String>();
		List<String> bravo = new List<String>();
		List<List<String>> charlie = new List<List<String>>();
		int i, j, k;

		alpha.add(1, "Basketball");
		alpha.add(2, "Football");
		bravo.add(1, "Soccer");
		bravo.add(2, "Tennis");
		charlie.add(1, alpha);
		charlie.add(2, bravo);

		System.out.println("A: "+alpha);//add function tests
		System.out.println("B: "+bravo);  
		System.out.println("C: "+charlie);
	  
		System.out.println("A.equals(A) is "+alpha.equals(alpha));//determine equality
		System.out.println("A.equals(B) is "+alpha.equals(bravo));
		System.out.println("A.equals(C) is "+alpha.equals(charlie));
	  
		System.out.println("A.size() is "+alpha.size());//get size
		System.out.println("B.size() is "+bravo.size());
		System.out.println("C.size() is "+charlie.size());

		alpha.remove(1);//tests removal
	 	bravo.remove(2);

		System.out.println("B.get(1) is "+bravo.get(1));//tests retrieval
		System.out.println("C: "+charlie);
		System.out.println();
		try{
			System.out.println(alpha.get(200));
		}
		catch(ListIndexOutOfBoundsException e){
			System.out.println("Caught Exception: ");
			System.out.println(e);
			System.out.println("Continuing without interuption");
		}
		System.out.println();
	}
}
