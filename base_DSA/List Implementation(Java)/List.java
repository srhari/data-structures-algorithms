/* Srihari Dasarathy (sdasarat) 1414679
 * CS12M Lab6
 * May 15th, 2015
 * List.java, generic list object implementation
 */

public class List<T>{
   //create a Node class
   private class Node{
      T item;
      Node next;
      Node(T a){//constructor
         item = a;
         next = null;
      }
   }
   //instance fields
   private Node head;    
   private int numItems; 
   //List constructor
   public List(){
      head = null;
      numItems = 0;
   }

   //helper function, returns reference to Node specified by index
   private Node find(int index){
      Node N = head;
      for(int i=1; i<index; i++) N = N.next;
      return N;
   }


   // ADT op
   // determines if list is empty
   public boolean isEmpty(){
      return(numItems == 0);
   } 
   //returns number of elements
   public int size() {
      return numItems;
   }
   //returns item at specified index
   public T get(int index) throws ListIndexOutOfBoundsException {

      if( index<1 || index>numItems ){
         throw new ListIndexOutOfBoundsException(
            "IntegerList Error: get() called on invalid index: " + index);
      }
      Node N = find(index);
      return N.item;
   }  
   // inserts newItem into list at specified index, alters list order accordingly
   public void add(int index, T newItem) throws ListIndexOutOfBoundsException{
      if( index<1 || index>(numItems+1) ){
         throw new ListIndexOutOfBoundsException("IntegerList Error: add() called on invalid index: " + index);
      }
      if(index==1){
         Node N = new Node(newItem);
         N.next = head;
         head = N;
      }else{
         Node P = find(index-1); 
         Node O = P.next;
         P.next = new Node(newItem);
         P = P.next;
         P.next = O;
      }
      numItems++;
   }
   // removes item at specified position, renumbers list accordingly
   public void remove(int index) throws ListIndexOutOfBoundsException{
      if( index<1 || index>numItems ){
         throw new ListIndexOutOfBoundsException("IntegerList Error: remove() called on invalid index: " + index);
      }
      if(index==1){
         Node N = head;
         head = head.next;
                  N.next = null;
      }else{
         Node P = find(index-1);
         Node N = P.next;
         P.next = N.next;
         N.next = null;
      }
      numItems--;
   }
   //resets the list
   public void removeAll(){
      head = null;
      numItems = 0;
   }        
   //converts toString, override object case
  public String toString(){
      StringBuffer sb = new StringBuffer();
      Node N = head;
      for( ; N!=null; N=N.next) sb.append(N.item).append(" ");
      return new String(sb);
   }   
   //equals method, determines equality
   public boolean equals(Object rhs){
      boolean eq = false;
      List R = null;
      Node N = null;
      Node M = null;
   
      if(rhs instanceof List){
         R = (List)rhs;
         eq = ( this.numItems == R.numItems );
   
         N = this.head;
         M = R.head;
         while(eq && N!=null){ 
            eq = (N.item == M.item);
            N = N.next;
            M = M.next;
         }
      }
      return eq;
   }
   
}
