#include "Node.h"

using namespace std;
	
Node::Node(int data) {
	this->data = data;
	this->next = NULL;
}

//node destructor
Node::~Node() {
	//cout << "destroying Node ... " << endl;
}

int Node::getValue() {
	return this->data;
}

void Node::setValue(int data) {
	this->data = data;
}

Node* Node::getNextNode() {
	return this->next;
}

void Node::setNextNode(Node* next) {
	this->next = next;
}

Node* Node::copyNode() {
	Node* n = new Node(this->data); // deep copy. b1 and b3 refer to 
	return n;
}