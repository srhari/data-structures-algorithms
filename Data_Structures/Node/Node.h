#include <iostream>

#ifndef Node_H
#define Node_H

class Node {
	//node class: each node has a store (int) and a pointer. 
	
	public:
		int data;
		Node* next;
		
		//node constructor
		Node(int);
		
		//node destructor
		~Node();
	
		int getValue();
		void setValue(int);
		Node* getNextNode();
		void setNextNode(Node*);
		
		Node* copyNode();
};

#endif