#include "BST_BST_Node.h"

using namespace std;
	
BST_BST_Node::BST_BST_Node(int data) {
	this->data = data;
	this->left = NULL;
	this->right = NULL;
}

//BST_Node destructor
BST_Node::~BST_Node() {
	//cout << "destroying BST_Node ... " << endl;
}

int BST_Node::getValue() {
	return this->data;
}

void BST_Node::setValue(int data) {
	this->data = data;
}

BST_Node* BST_Node::getLeftBST_Node() {
	return this->left;
}

void BST_Node::setleftBST_Node(BST_Node* leftChild) {
	this->left = leftChild;
}

BST_Node* BST_Node::getRightBST_Node() {
	return this->right;
}

void BST_Node::setRightBST_Node(BST_Node* rightChild) {
	this->right = rightChild;
}

BST_Node* BST_Node::copyBST_Node() {
	BST_Node* n = new BST_Node(this->data); // deep copy. b1 and b3 refer to 
	return n;
}