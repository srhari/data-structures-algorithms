#include "DoublyLinkedNode.h"

using namespace std;
	
DoublyLinkedNode::DoublyLinkedNode(int data):Node(data) {
	this->data = data;
	this->next = NULL;
	this->previous = NULL;
}

//node destructor
DoublyLinkedNode::~DoublyLinkedNode() {
	cout << "Destroying the doubly linked node" << endl;
}

DoublyLinkedNode* DoublyLinkedNode::getNextNode() {
	return this->next;
}

void DoublyLinkedNode::setNextNode(DoublyLinkedNode* next) {
	this->next = next;
}

DoublyLinkedNode* DoublyLinkedNode::getPreviousNode() {
	return this->previous;
}

void DoublyLinkedNode::setPreviousNode(DoublyLinkedNode* previous) {
	this->previous = previous;
}

DoublyLinkedNode* DoublyLinkedNode::copyNode() {
	DoublyLinkedNode* n = new DoublyLinkedNode(this->data); // deep copy. b1 and b3 refer to 
	return n;
}