#include <iostream>

#ifndef BST_Node_H
#define BST_Node_H

class BST_Node {
	//node class: each node has a store (int) and a pointer. 
	
	public:
		int data;
		Node* left;
		Node* right;
		
		//node constructor
		Node(int);
		
		//node destructor
		~Node();
	
		int getValue();
		void setValue(int);
		Node* getLeftNode();
		void setLeftNode(Node*);
		
		Node* getRightNode();
		void setRightNode(Node*);
		
		Node* copyNode();
};

#endif