#include <iostream>
#include "Node.h"

#ifndef DoublyLinkedNode_H
#define DoublyLinkedNode_H

class DoublyLinkedNode: public Node {
	//node class: each node has a store (int) and a pointer. 
	
	// uses class Node, additionally has a 'previous' instance field and 'previous' functions
	public:
		DoublyLinkedNode* next;
		DoublyLinkedNode* previous;
		
		DoublyLinkedNode(int);
		~DoublyLinkedNode();
		
		DoublyLinkedNode* getNextNode();
		void setNextNode(DoublyLinkedNode*);
	
		DoublyLinkedNode* getPreviousNode();
		void setPreviousNode(DoublyLinkedNode*);
		
		DoublyLinkedNode* copyNode();
};

#endif