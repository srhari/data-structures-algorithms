#include <iostream>

#ifndef BST_H_
#define BST_H_
#ifndef BST_Node_H_
#define BST_Node_H_

using namespace std;

class BST_Node {
	
	public:
		BST_Node(int value) { this->value = value; this->leftChild = NULL; this->rightChild = NULL; }
		BST_Node(int value, BST_Node* leftChild, BST_Node* rightChild) {
			this->value = value;
			this->leftChild = leftChild;
			this->rightChild = rightChild;
		}
		
		~BST_Node(){}
		
		int value;
		BST_Node* leftChild;
		BST_Node* rightChild;	
};

//class BST_Node;
class BST {
	
	public: 
		
		BST() { this->root = NULL; }
		BST(BST_Node* root) { this->root = root; }	
		~BST(){}
		
		BST_Node* inOrderSuccessor(BST_Node*);
		BST_Node* minValue(BST_Node*);
		BST_Node* insert(BST_Node*, int);
		void del(int);
		BST_Node* search(BST_Node*, int);
		
		void printInOrder(BST_Node* root) {
			if (root == NULL) return;
			printInOrder(root->leftChild);
			cout << root->value << " " << endl;
			printInOrder(root->rightChild);
		}
		
		void printPostOrder(BST_Node* root) {
			if (root == NULL) return;
			printPostOrder(root->leftChild);
			printPostOrder(root->rightChild);
			cout << root->value << " " << endl;
		}
		
		void printPreOrder(BST_Node* root) {
			if (root == NULL) return;
			cout << root->value << " " << endl;
			printPreOrder(root->leftChild);
			printPreOrder(root->rightChild);
		}
		
		BST_Node* getRoot() {
			return this->root;
		}
		
		
	private:
		
		BST_Node* root;
};

#endif
#endif
