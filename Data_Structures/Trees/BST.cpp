#include <iostream>
#include "BST.h"

using namespace std;

BST_Node* BST::insert(BST_Node* root, int value) {
	
	if (!root) {
		BST_Node* p = new BST_Node(value);
		if (!this->root) this->root = p;
		return p; 
	}
	
	if (value < root->value) {
		root->leftChild = this->insert(root->leftChild, value);
	} else if (value > root->value) {
		root->rightChild = this->insert(root->rightChild, value);
	}
	
	return root;
}

BST_Node* BST::search(BST_Node* root, int value) {
	if (!root) return NULL;
	if (value == root->value) return root;
	else {
		if (value < root->value) return this->search(root->leftChild, value);
		else return this->search(root->rightChild, value); 
	}
}

void BST::del(int value) {
	BST_Node* traverser = this->root;
	while (traverser) {
		if (value < traverser->value) traverser = traverser->leftChild;
		else if (value > traverser->value) traverser = traverser->rightChild;
		else {
			if (!traverser->leftChild && !traverser->rightChild) traverser = NULL;
			else if ((traverser->leftChild && !traverser->rightChild) || (traverser->rightChild && !traverser->leftChild)) {
				if (traverser->leftChild) {
					BST_Node* p = traverser->leftChild;
					traverser->value = traverser->leftChild->value;
					traverser->leftChild = traverser->leftChild->leftChild;
					traverser->rightChild = traverser->leftChild->rightChild;
				} else if (traverser->rightChild) {
					BST_Node* p = traverser->rightChild;
					traverser->value = traverser->rightChild->value;
					traverser->leftChild = traverser->rightChild->leftChild;
					traverser->rightChild = traverser->rightChild->rightChild;
				}
			} else {
				BST_Node* p = this->inOrderSuccessor(traverser);
				traverser->value = p->value;
				delete[] p;
			} 
		}
	}
	return;
}

// given a node in a tree, returns its in order successor.
BST_Node* BST::inOrderSuccessor(BST_Node* n) {
	// if the right subtree is not null, the successor is the minimum of the right subtree.
	if (n->rightChild) return this->minValue(n->rightChild);
	
	// if the right subtree is null, the successor is an ancestor.
	if (!n->rightChild) {
		BST_Node* traverser = traverser;
		BST_Node* successor = NULL;
		while (traverser) {
			if (n->value > traverser->value) traverser = traverser->rightChild;
			else if (n->value < traverser->value) {
				successor = traverser;
				traverser = traverser->leftChild;
			} else break;
		}
		return successor;
	}
	return NULL;
}

// given the root of a tree, returns the node with the minimum value in the tree.
BST_Node* BST::minValue(BST_Node* root) {
	while (root->leftChild) root = root->leftChild;
	return root;
}



