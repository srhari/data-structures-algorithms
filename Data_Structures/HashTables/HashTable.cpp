#include <iostream>
#include <cmath>
#include "HashTable.h"

using namespace std;

void HashTable::insert(string s) {
	int h = Hash(s);
	if (this->buckets[h].head == NULL) this->buckets[Hash(s)].head = new HashNode(s);
	else {
		this->buckets[h].tail->next = new HashNode(s);
		this->buckets[h].tail = this->buckets[h].tail->next;
	}
	if (this->buckets[h].tail == NULL) this->buckets[Hash(s)].tail = this->buckets[h].head;	
}

void HashTable::del(string s) {
	int h = Hash(s);
	for (HashNode* n = this->buckets[h].head; n != NULL; n = n->next) {
		if (n->val == s) {
			cout << n->val << endl;
			if (n == this->buckets[h].head || n == this->buckets[h].tail) {
				if (n == this->buckets[h].head) {
					this->buckets[h].head = this->buckets[h].head->next;
					if (this->buckets[h].head && this->buckets[h].head->previous) this->buckets[h].head->previous = NULL;
				}
				if (n == this->buckets[h].tail) {
					this->buckets[h].tail = this->buckets[h].tail->previous;
					if (this->buckets[h].tail && this->buckets[h].tail->next) this->buckets[h].tail->next = NULL;
				}
			}
			else {
				n->previous->next = n->next;
				n->next->previous = n->previous;
			}
		}
	}
}

string HashTable::lookup(string s) {
	int h = Hash(s);
	for (HashNode* n = this->buckets[h].head; n != NULL; n = n->next) if (n->val == s) return n->val;
	return string("Not found.");
}


// intakes a string, and returns a hashed integer value.
int HashTable::Hash(string s) {
	return abs(static_cast<int>(hash<string>()(s))) % 25;
}





