#include <iostream>
#include <string>
#include <vector>

#ifndef HashMap_H
#define HashMap_H
#ifndef Item_H
#define Item_H
#ifndef DynamicArray_H
#define DynamicArray_H

using namespace std;

class Item {
	public:
		// Item constructor, destructor
		Item() {}
		Item(int key, string val) {
			this->key = key;
			this->val = val;
			this->deleted = false;
		}
		~Item() {}
		
		int key;
		string val;
		bool deleted;
};

class DynamicArray {
	public:
		DynamicArray(size_t size) {
			this->size = size;
			this->used = 0;
			this->items = (Item*)malloc(size * (sizeof(Item)));
		}
		
		~DynamicArray() {}
		
		void put(Item item, int index) {
			if (this->items[index].val == "") this->used++;
			this->items[index] = item;
			if (this->used+1 >= (this->size*0.7)) this->expand();
		}
		
		void remove(int index) {
			if (this->items[index].val != "") this->used--;
			this->items[index].key = 0;
			this->items[index].val = "";
		}
		
		Item* get(int index) {
			if (index < this->arraySize()) return &this->items[index];
			else return NULL;
		}
		
		size_t arraySize() {
			return this->size;
		}
		
		size_t arrayUsed() {
			return this->used;
		}
		
	private:		
		Item* items;
		size_t used;
		size_t size;
		
		void expand() {
			cout << this->size << endl;
			this->size *= 2;
			this->items = (Item*)realloc(this->items, this->size * sizeof(Item));
		}		
};

// class hash map stores maps int keys to string values in a hashed map data structure.
// collision resolution method: open addressing w/linear probing
class HashMap {
	public:
		// hash map constructor and destructor
		HashMap(size_t size) : items(size) {
			//this->items(size);
		}
		
		~HashMap() {}
		
		
		void insert(int, string);
		void del(int);
		string lookup(int);
	
	private:
		
		// every hash map is simply a dynamically sizable array of item
		DynamicArray items;
		
		// cannot be a static function, because it depends on the size of the Hash Map instance.
		int Hash(int);
};

#endif
#endif
#endif