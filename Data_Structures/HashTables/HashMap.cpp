#include <iostream>
#include <cmath>
#include "HashMap.h"

using namespace std;

// inserts a key-value pair into the hash map
void HashMap::insert(int key, string val) {
	int index = this->Hash(key); // hash the key to compute the index
	Item itm(key, val);
	for (int i = index; i < this->items.arraySize(); i++) {

		if (this->items.get(i)->key == 0 || this->items.get(i)->key == key) {
			this->items.put(itm, i);
			return;
		}
	}
}

// deletes a key-value pair from the hash map
void HashMap::del(int key) {
	int index = this->Hash(key);
	Item* it = this->items.get(index);
	it->val = ""; it->key = 0; it->deleted = true;
}

// returns the associated value of a key in the hash map.
// starts at the appropriate index and iterates through all slots following that in the hash map. If the key is found, return it. If an empty slot is found, the key cannot exist in the hash map.
string HashMap::lookup(int key) {
	int index = this->Hash(key);
	for (int i = index; i < this->items.arraySize(); i++) {
		if (this->items.get(i)->key == 0 && this->items.get(i)->deleted) return "";
		else if (this->items.get(i)->key == key) return this->items.get(i)->val;
	}
	return "This key does not exist.";
}

// hashes the integer key into an index in the vector.
int HashMap::Hash(int key) {
	return key % this->items.arraySize();
}