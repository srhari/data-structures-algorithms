#include <iostream>
#include <string>

#ifndef HashTable_H
#define HashTable_H
#ifndef Bucket_H
#define Bucket_H
#ifndef HashNode_H
#define HashNode_H

using namespace std;

class HashNode {
	public:
		// HashNode constructor, destructor
		HashNode(string s) {
			this->val = s;
			this->next = NULL;
			this->previous = NULL;
		};
		~HashNode() {}
		
		string val;
		HashNode* next;
		HashNode* previous;
};

class Bucket {
	public:
		// bucket contructor, destructor
		Bucket() {
			this->head = NULL;
			this->tail = NULL;
		}
		~Bucket() {}
		
		// every bucket is a linked list. Has a head and a tail.
		HashNode* head;
		HashNode* tail;
		
		
};

// class hash table stores strings in a hash table.
// collision resolution method: separate chaining (uses a linked list to store strings that hash to the same bucket)
class HashTable {
	public:
		// hash table constructor and destructor
		HashTable() {}
		~HashTable() {}
		
		void insert(string);
		void del(string);
		string lookup(string);
	
	//private:
		
		// every hash table has a set of buckets.
		Bucket buckets[25];
		
		static int Hash(string);
};

#endif
#endif
#endif

