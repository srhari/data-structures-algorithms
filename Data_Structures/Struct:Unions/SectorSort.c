/* variable_sort: Reads a C program and prints in alphabetical order each group of words that are identical 
in the first 6 characters, but can be different thereafter (words less than # characters long will be discarded). 
Strings and comments are not counted, and the # of characters needed can be set from the command line. */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXWORD 100

bool isInt(char a[]);
int getword(char *, int);
int getch(void);
void ungetch(int);
struct node* addTree(char*, struct node*);
char *my_strdup(char *);
void traverseTree(struct node*);
void treeprint(struct node*);

//construct type Node to store unique words and their counts. Each Node has the word, word count, group number, and pointers to left and right children. 
typedef struct node{
	char *word;
	int count;
	int group;
	struct node *left;
	struct node *right;
}Node;

int num;
Node* root = NULL;
Node* par = NULL;

int main(int argc, char *argv[0]) {
	int flag = 1; //declare flag
	char word[MAXWORD]; //declare 'word' character array, in which individual words will be stored. 
	if (argc != 2){ //2 arguments detected?
		printf("Error. Incorrect format call. SectorSort [number of characters to group]");
		return -1;
	}else if (isInt(argv[1]) == false){ //if second argument is not an integer, auto set it to 6.
		printf("ERROR: No integer detected. Default::set to 6"); 
		num = 6;
	}else{ //else num = integer
		num = abs(atoi(argv[1]));
		printf("num = %d\n", num);
	}
			
	//Node *root = NULL; //initialize empty 'head' of BST
	//Node *par = NULL;
	while (getword(word, MAXWORD) != EOF){ //get next word from program input, until EOF is reached
		//printf("%s ", word);
		if (isalpha(word[0]) && strlen(word) >= num){
			if (flag == 1){
				//printf("FLAG: %d\n", flag);
				root = addTree(word, root);
				//printf("ROOT WORD: %s, ROOT COUNT: %d, ROOT GROUP: %d, ROOT POINTERS: %p %p\n", root->word, root->count, root->group, root->left, root->right);
				flag = 0;
				//printf("FLAG: %d\n", flag);
			}else{
				//printf("par...\n");
				//printf("ROOT WORD: %s, ROOT COUNT: %d, ROOT GROUP: %d, ROOT POINTERS: %p %p\n", root->word, root->count, root->group, root->left, root->right);
				par = addTree(word, root); //check if valid word, if so add into tree
				//printf("ROOT WORD: %s, ROOT COUNT: %d, ROOT GROUP: %d, ROOT POINTERS: %p %p\n", root->word, root->count, root->group, root->left, root->right);
			}
			//printf("WORD: %s %p %p\n", root->word, root->left, root->right);
		}	
	}
	
	//printf("ROOT WORD: %s, ROOT COUNT: %d, ROOT GROUP: %d, ROOT POINTERS: %p %p\n", root->word, root->count, root->group, root->left, root->right);
	
	traverseTree(root); //traverse through entire tree and assign group numbers based on matching first [number] words
	treeprint(root); //print entire tree, if separate different group numbers by lines. 
}


//appends word into a struct, and returns new struct. if word already exists in the tree, increments the word count instead. 
struct node* addTree(char* word, struct node* p){
	//printf("STRUCT CREATED!");
	int cond;
	if (p == NULL){
		//printf("null p!\n");
		p = (struct node *) malloc(sizeof(struct node)); //initialize memory for new struct 
		p->word = my_strdup(word);//word; //assign value pointed to by *word into char array within struct
		p->count = 1; //word count for a new word is now 1
		p->group = 1; //initialize group number
		p->left = p->right = NULL; //initialize left and right children to NULL
		//printf("null p!\n");
	}else if ((cond = strcmp(word, p->word)) == 0){
		p->count++; //if word match, increment count
	}
	else if (cond < 0){
		//printf("p->left!\n");
		p->left = addTree(word, p->left); //else call addTree on either left or right child
	}else{
		//printf("p->right: %p!\n", p->right);
		p->right = addTree(word, p->right);
		//printf("p->right: %p!\n", p->right);
	}
	return p; //return the struct
}

//traverses through entire tree and assigns group numbers.
/*void traverseTree(struct node* p){
	traverse through the tree. for each node, use NUM to compare first NUM letters with surrounding nodes. If no match, increment the group number of the following 
	int group_num = 0; //initalize group# count
	if (p != NULL){
		if (p->left == NULL && p->right == NULL) return;
		if (p->left == NULL && p->right != NULL){
			if (strncmp(p->word, p->right->word, num) == 0) traverseTree(p->right);
			else {
				p->right->group = ++group_num;
				traverseTree(p->right);
			}
		}else if (p->left != NULL && p->right == NULL){
			if (strncmp(p->word, p->left->word, num) == 0) traverseTree(p->left);
			else {
				p->left->group = ++group_num;
				traverseTree(p->left);
			}
		}else{
			if (strncmp(p->word, p->left->word, num) != 0) p->left->group = ++group_num;
			if (strncmp(p->word, p->right->word, num) != 0) p->right->group = ++group_num;
			traverseTree(p->left);
			traverseTree(p->right);
		}
	}	
}*/

//traverses through entire tree and assigns group numbers.
void traverseTree(struct node* p){
	/*traverse through the tree. for each node, use NUM to compare first NUM letters with surrounding nodes. If no match, increment the group number of the following */
	if (p != NULL){
		if (p->left == NULL && p->right == NULL) return; //node has no children; return
		if (p->left == NULL && p->right != NULL){ //right child only. check group number and recur right. 
			if (strncmp(p->word, p->right->word, num) != 0) p->right->group++;
			traverseTree(p->right);
		}else if (p->left != NULL && p->right == NULL){ //left child only. check group number and recur left. 
			if (strncmp(p->word, p->left->word, num) != 0) p->left->group++;
			traverseTree(p->left);
		}else{ //both left and right children. check group numbers
			if (strncmp(p->word, p->left->word, num) != 0) p->left->group++;
			if (strncmp(p->word, p->right->word, num) != 0) p->right->group++;
			traverseTree(p->left);
			traverseTree(p->right);
		}
	}	
}

void treeprint(struct node *p){
	if(p != NULL) {
		treeprint(p->left);
		printf("Word: %s Count: %d Group: %d\n", p->word, p->count, p->group);
		//treeprint(p->left);
		treeprint(p->right);
	}
}

/* checks if command line argument is integer or not, returns a boolean value */
bool isInt(char a[]){
	char *p = a; //assign pointer p to point to first character of array a
	if (*p == '-' || *p == '+') //ignore leading -'s and +'s
		p++; //increment pointer
	for ( ; *p != '\0'; p++) //use pointer to iterate through all characters, check if each character is digit
		if (!isdigit(*p))
			return false;
	return true;
}

//duplicate word extracted from array
char *my_strdup(char *s){ 
	char *p;
	p = (char *) malloc(strlen(s)+1); // +1 for '\0'
	if (p != NULL) strcpy(p, s);
	return p;
}

#define IN 1
#define OUT 0
/* getword: extracts the next word from program input and sets into array 'word'. */
/* getword(word, MAXWORD), limit determines space left in file */
int getword(char *word, int lim){
	int c, d, s_comment, m_comment, string, directive;
	char *w = word; //char pointer to first element of word array
	
	s_comment = m_comment = string = directive = OUT;
	
	while (isspace(c = getch())) ; //skip leading spaces
	
	if (c == '/'){
		//printf("FORWARD SLASH\n");
		d = getch();
		if (d == '*'){
			//printf("MULTILINE COMMENT DETECTED\n");
			m_comment = IN;
		}
		else {
			m_comment = OUT;
			ungetch(d);
		}
		
		if (d == '/'){
			//printf("SINGLE LINE COMMENT DETECTED\n");
			s_comment = IN;
		}
		else {
			s_comment = OUT;
			ungetch(d);
		}
	}
	
	if (c == '\"') string = IN; //set string to IN '\"'
	
	if (c == '#') directive = IN; //set directive to IN
	
	if (c == '\\') c = getch(); //ignore the \\ character
	
	if (s_comment == OUT && m_comment == OUT && string == OUT && directive == OUT){
		if (c != EOF) *w++ = c; //add new character to word array
		if (!isalnum(c) && c != '_'){ //non alpha-numeric char detected, word finished. exit
			*w = '\0';
			return c;
		}
		for ( ; --lim > 0; w++){ //load up the word into array. 
			*w = getch();
			if (!isalnum(*w) && *w != '_'){ //check for reasonable characters. 
				ungetch(*w);
				break;
			}
		}
		*w = '\0';
		return word[0];
	}else if (m_comment == IN){
		//*w++ = c;
		//*w++ = d;
		c = getch();
		d = getch();
		while (/**w++ = */(c != '*' && d != '/')){
			if ((c = getch()) == '*'){
				if ((d = getch()) == '/'){
					//*w++ = c;
					m_comment = OUT;
					//break;
				}else{
					ungetch(d);
				}
			}else ;
		}
		*w = '\0';
	}else if (s_comment == IN){
		//c = getch();
		while ((c = getch()) != '\n') ;
		s_comment = OUT;
	}else if (string == IN){
		//printf("STRING");
		//*w++ = c;
		while ((c = getch()) != '\"'){ // '\"'
			if (c == '\\') c = getch();
		}
		string = OUT;
		*w = '\0';
	}else if (directive == IN){
		//printf("DIRECTIVE");
		//*w++ = c;
		while ((c = getch()) != '\n'){
			if (c == '\\') c = getch();
		}
		directive = OUT;
		*w = '\0';
	}
	
	if (string == OUT) printf("Not in string...\n");
	
	return c;
}

#define BUFSIZE 100

char buf[BUFSIZE]; // buffer for ungetch
int bufp = 0; //next free position in buffer

/* get a character that can possible be pushed back */
int getch(void){
	return (bufp > 0) ? buf[bufp--] : getchar();
}

/* push character back onto input */
void ungetch(int c){
	if (bufp >= BUFSIZE)
		printf("ERROR: BUFFER OVERFLOW");
	else
		buf[bufp++] = c;
} 