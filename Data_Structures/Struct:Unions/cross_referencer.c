/* Write a cross-referencer that prints a list of all words in a document, and, for each word, a list of the line numbers on which it occurs. Remove noise words such as 'the', 'and' and so on. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

#define MAXWORD 100
#define MAXLINES 1000

int getword(char*, int);
int getch();
void ungetch(int);
struct node* addTree(char*, struct node*);
void treeprint(struct node*);
bool isInt(char[]);
char* my_strdup(char*);
bool wordCheck(char*);


//Create constant array of noise words that program should recognize and ignore
const char *noise[51] = {"the", "and", "if", "but", "about", "all", "also", "an", "are", "as", "at", "be", "because", "been", "being", "both", "by", "can", "could", "did", "do", "for", "get", "got", "has", "had", "have", "how", "is", "it", "like", "make", "me", "my", "on", "or", "our", "should", "some", "such", "than", "that", "the", "then", "to", "very", "was", "well", "would", "a", "I"};

//initialize (construct) structure with properties: word, word count, array of line numbers
typedef struct node{
	const char *word; //single word stored as string
	int count; //word count
	int lines[MAXLINES]; //line numbers stored in array, initialize array of 0s
	struct node *left; //pointers to left/right nodes
	struct node *right;
}Node;

Node* root = NULL;
Node* par = NULL;
	
int main(int argc, char *argv[]) {
	int flag = 1;
	char word[MAXWORD]; //array that stores all words
	while (getword(word, MAXWORD) != EOF){ //while word in document
		if (wordCheck(word) == true) continue; //skip a noise word
		if (flag == 1){
			root = addTree(word, root); //assign root to first word in tree
			flag = 0;
		}else par = addTree(word, root); //add word into tree, should either create new struct with initial line # or increment word count and line # of existing struct
		//printf("%s ", word);
	}
	treeprint(root);//print contents of every struct in tree
}

//appends word into a struct, and returns new struct. if word already exists in the tree, increments the word count instead. 
struct node* addTree(char* word, struct node* p){
	//printf("STRUCT CREATED!");
	int cond;
	if (p == NULL){
		//printf("null p!\n");
		p = (struct node *) malloc(sizeof(struct node)); //initialize memory for new struct 
		p->word = my_strdup(word);//word; //assign value pointed to by *word into char array within struct
		p->count = 1; //word count for a new word is now 1
		p->lines[0] = __LINE__; //first line number
		p->left = p->right = NULL; //initialize left and right children to NULL
		//printf("null p!\n");
	}else if ((cond = strcmp(word, p->word)) == 0){
		p->count++; //if word match, increment count and add another line number
		p->lines[p->count-1] = __LINE__;
	}else if (cond < 0){
		//printf("p->left!\n");
		p->left = addTree(word, p->left); //else call addTree on either left or right child
	}else{
		//printf("p->right: %p!\n", p->right);
		p->right = addTree(word, p->right);
		//printf("p->right: %p!\n", p->right);
	}
	return p; //return the struct
}

/*print out entire tree*/
void treeprint(struct node *p){
	if(p != NULL) {
		treeprint(p->left);
		printf("WORD: %s COUNT: %d LINES: ", p->word, p->count);
		for (int i = 0; i < MAXLINES && p->lines[i] != 0; i++) printf("%d ", p->lines[i]);
		printf("\n");
		treeprint(p->right);
	}
}

/* checks if command line argument is integer or not, returns a boolean value */
bool isInt(char a[]){
	char *p = a; //assign pointer p to point to first character of array a
	if (*p == '-' || *p == '+') //ignore leading -'s and +'s
		p++; //increment pointer
	for ( ; *p != '\0'; p++) //use pointer to iterate through all characters, check if each character is digit
		if (!isdigit(*p))
			return false;
	return true;
}

//duplicate word extracted from array
char *my_strdup(char *s){ 
	char *p;
	p = (char *) malloc(strlen(s)+1); // +1 for '\0'
	if (p != NULL) strcpy(p, s);
	return p;
}

bool wordCheck(char* a){
	for (int i = 0; i < 51; i++)
		if (strcmp(noise[i], a) == 0) return true;
	return false;
}

/* getword: extracts the next word from program input and sets into array 'word'. */
/* getword(word, MAXWORD), limit determines space left in file */
int getword(char *word, int lim){
	int c;
	char *w = word; //char pointer to first element of word array
		
	while (isspace(c = getch())) ; //skip leading spaces
			
	if (isalpha(c)){
		*w = c;
		w++;
		//c = getch();
	}else{
		*w = '\0';
		return c;
	}
	
	for ( ; --lim > 0; w++){ //load up the word into array, increment pointer. 
		*w = getch(); //c = getch();
		if (!isalpha(*w)){
			//printf("ALPHA\n");
			ungetch(*w); //set value of pointer to c.
			break;
		}  
	}
	*w = '\0';
	return word[0];
	
	//return c;
}

#define BUFSIZE 100

char buf[BUFSIZE]; // buffer for ungetch
int bufp = 0; //next free position in buffer

//get a character that can possible be pushed back
int getch(void){
	return (bufp > 0) ? buf[bufp--] : getchar();
}

//push character back onto input 
void ungetch(int c){
	if (bufp >= BUFSIZE)
		printf("ERROR: BUFFER OVERFLOW");
	else
		buf[bufp++] = c;
}
