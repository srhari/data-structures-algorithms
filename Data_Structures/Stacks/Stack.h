#include "../Node/Node.h"

using namespace std;

#ifndef Stack_H
#define Stack_H

class Stack {
	
	
	public:
		
		// push an item onto the stack
		void push(Node*);
		
		// reveal the top most item in the stack
		Node* peek();
		
		// take off the top most item in the stack
		Node* pop();
		bool isEmpty();

		// prints the contents of the stack
		void dumpStack();
				
		// stack constructor
		Stack();
		
		// stack destructor
		~Stack();
			
	private:
		
		Node* top; // a pointer to the top most item in the stack.
		bool empty; // stack is empty by default

};



#endif
