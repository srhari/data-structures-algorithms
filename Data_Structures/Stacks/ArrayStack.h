#include "../Node/Node.h"

using namespace std;

#ifndef ArrayStack_H
#define ArrayStack_H

class ArrayStack {
	
	
	public:
		
		// push an item onto the stack	
		bool push(int);
		
		// reveal the top most item in the stack
		int peek();
		
		// take off the top most item in the stack
		int pop();
		bool isEmpty();

		// prints the contents of the stack
		void dumpStack();
		
		int* getStackLocation() { return this->stack; }
				
		// stack constructor
		ArrayStack(size_t);
		
		// stack destructor
		~ArrayStack();
			
	private:
		
		int* top; // a pointer to the top most item in the stack.
		bool empty; // stack is empty by default
		size_t stackSize; // size of the stack
		int* stack; // pointer to start of stack memory location
		
		

};



#endif