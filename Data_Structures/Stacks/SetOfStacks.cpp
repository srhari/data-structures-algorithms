#include "SetOfStacks.h"

SetOfStacks::SetOfStacks(size_t size) : ArrayStack(size) {
	ArrayStack as(size);
	this->stackSet.push_back(as);
	this->empty = true;
	this->nextFreeLocation = as.getStackLocation();
	this->stackSize = size;
}

SetOfStacks::~SetOfStacks() {}

bool SetOfStacks::push(int item) {
	*this->nextFreeLocation = item;
	this->nextFreeLocation++;
	if (this->nextFreeLocation - this->stackSet[this->stackSet.size()-1].getStackLocation() >= this->stackSize) {
		ArrayStack as(this->stackSize);
		this->stackSet.push_back(as);
		this->nextFreeLocation = as.getStackLocation();
	}
	this->empty = false;
	return true;
}

bool SetOfStacks::isEmpty() {
	return this->empty ? true : false;
}

void SetOfStacks::dumpSetOfStacks() {
	for (int i = 0; i < this->stackSet.size(); i++) this->stackSet[i].dumpStack();
}

int SetOfStacks::pop() {
	if (this->empty) return 0;
	if (this->stackSet.size() == 1 && this->nextFreeLocation-1 == this->stackSet[this->stackSet.size()-1].getStackLocation()) this->empty = true;
	return *(--this->nextFreeLocation);
}

int SetOfStacks::peek() {
	return this->empty ? 0 : *(this->nextFreeLocation-1);
}




