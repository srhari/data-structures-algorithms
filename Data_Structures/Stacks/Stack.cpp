#include "Stack.h"

// adds a node to the stack
void Stack::push(Node* n) {
	if (this->isEmpty()) n = this->top;
	else {
		Node* p;
		for (p = this->top; p != NULL; p = p->getNextNode()) ;
		p = n;
	}
	this->empty = false;
}

// peeks at the top of the stack
Node* Stack::peek() {
	if (this->isEmpty()) return NULL;
	return this->top;
}

// pop off the top of the stack
Node* Stack::pop() {
	if (this->isEmpty()) return NULL;
	Node* outcast = this->top;
	this->top = this->top->getNextNode();
	if (this->top == NULL) this->empty = true;
	return outcast;
}

// checks if the stack is empty
bool Stack::isEmpty() {
	return this->empty;
}

// print the stack contents
void Stack::dumpStack() {
	cout << "Dumping stack..." << this->top << endl;
	/*for (Node* n = this->top; n != NULL; n = n->getNextNode()) {
		printf("Hello\n");
	}*/
}

// constructs the stack
Stack::Stack() {
	this->top = NULL;
	this->empty = true;
}


// destroys the stack
Stack::~Stack() {
	
}