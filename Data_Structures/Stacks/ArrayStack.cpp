#include "ArrayStack.h"

ArrayStack::ArrayStack(size_t size) {
	this->stack = (int*)malloc(sizeof(int)*size);
	this->stackSize = size;
	this->empty = true;
	this->top = NULL;
}

ArrayStack::~ArrayStack(){}

bool ArrayStack::isEmpty() {
	return this->empty ? true : false;
}

void ArrayStack::dumpStack() {
	int* p = this->stack;
	for (int i = 0; i < stackSize; i++, p++) cout << *p << " ";
	cout << endl;
}

int ArrayStack::pop() {
	if (this->isEmpty()) return 0;
	else {
		int* p = this->top; 
		if (this->top == this->stack) { this->top = NULL; this->empty = true; }
		else this->top--;
		return *p;
	}
}

bool ArrayStack::push(int item) {
	if (this->stack + (this->stackSize-1) == this->top) { cout << "Stack full. Cannot push." << endl; return false; }
	if (this->isEmpty()) { *this->stack = item; this->top = this->stack; this->empty = false; }
	else { this->top++; *this->top = item; }
	return true;
}

int ArrayStack::peek() {
	return this->top ? *this->top : 0; 
}

