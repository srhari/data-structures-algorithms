#include "ArrayStack.h"
#include <vector>

using namespace std;

#ifndef SetOfStacks_H
#define SetOfStacks_H

class SetOfStacks : public ArrayStack {
	
	public:
		SetOfStacks(size_t);
		~SetOfStacks();
		int popAt(int);
		bool push(int);
		int pop();
		int peek();
		bool isEmpty();
		void dumpSetOfStacks();
				
	private:
		
		vector<ArrayStack> stackSet; // vector store a set of stacks
		bool empty;
		int* nextFreeLocation; // points to the next free location on the stack(s)
		size_t stackSize;
		
};

#endif