#include <iostream>
#include <vector>
#include <algorithm>

//#include "../Node/BST_Node.h"

#ifndef MaxBinaryHeap_H_
#define MaxBinaryHeap_H_

using namespace std;

class MaxBinaryHeap {
	
	public:
		
		MaxBinaryHeap() {
			this->used = 0;
			this->empty = true;
			this->full = false;
		}
		
		~MaxBinaryHeap(){}
		
		void swap(int* a, int* b) {
			int temp = *a;
			*a = *b;
			*b = temp;
		}
		
		void rebalance(int changedPosition) {
			
			// calculate parent position
			int parentPosition = (changedPosition-1)/2;
			
			// terminate if root reached.
			if (parentPosition < 0) return;
			
			// else swap, and keep swapping if rule applies.
			if (this->heap[changedPosition] > this->heap[parentPosition]) {
				swap(&this->heap[changedPosition], &this->heap[parentPosition]);
				rebalance(parentPosition);
			} else return;
		}
		
		void insert(int);
		void del(int);
		void delAll(int);
		
		vector<int> search(int);
		void printHeap();
		
		int findMax();
		void pop();
		
		int* sort();
		
		int getSize() {
			return sizeof(this->heap)/sizeof(int);
		}
		
		bool isEven(int a) {
			if (a % 2 == 0) return true;
			else return false;
		}
		
	private:
		
		int heap[50];
		size_t used;
		bool full;
		bool empty;
};

#endif