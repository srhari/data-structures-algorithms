#include <iostream>
#include "BinHeap.h"

using namespace std;

// inserts into the heap at the next slot
void MaxBinaryHeap::insert(int num) {
	
	// if heap is full, ...
	if (this->full) return;
	else {
		this->empty = false;
		this->heap[used++] = num;
		this->rebalance(this->used-1);
		if (this->used == this->getSize()) this->full = true;
	}
}

// deletes the first occurrence of num in the heap
void MaxBinaryHeap::del(int num) {
	if (this->empty) return;
	vector<int> v = this->search(num);
	if (v.empty()) return;
	
	// num is last element in the heap
	if (v[0] == this->used-1) {
		this->heap[this->used-1] = 0;
		this->used--;
	} else {
		int swapper = v[0];
		//cout << "swapper index: " << swapper << endl;
		// not the last element in the heap, also the node has children.
		//cout << "HERE:" << v[0]*2+1 << " " << this->used-1 << endl;
		if (v[0]*2+1 <= this->used-1) {
			int leftChildIndex = v[0]*2+1, rightChildIndex = v[0]*2+2;
			cout << this->heap[leftChildIndex] << this->heap[rightChildIndex] << endl;
			cout << rightChildIndex << " " << this->used << endl;
			if (rightChildIndex >= this->used) {
				//cout << "swapper is leftChildIndex!" << endl;
				swapper = leftChildIndex;
			} else {
				if (this->heap[leftChildIndex] > this->heap[rightChildIndex]) swapper = leftChildIndex;
				else swapper = rightChildIndex;
				//swapper = max(this->heap[leftChildIndex], this->heap[rightChildIndex]);
				//cout << "swapper is max of two!" << endl;
			}
			//cout << "SWAPPER index: " << swapper << endl;
			swap(&this->heap[swapper], &this->heap[v[0]]);
		}
		for (int i = swapper+1; i < this->used; i++) {
			this->heap[i-1] = this->heap[i];
			rebalance(i-1);
		}
		this->heap[this->used-1] = 0;
		this->used--;
	}
	this->full = false;
	
}

void MaxBinaryHeap::delAll(int num) {
	if (this->empty) return;
	vector<int> v = this->search(num);
	if (v.empty()) return;
	
	for (int i = 0; i < v.size(); i++) {
		// num is last element in the heap
		if (v[i] == this->used-1) {
			this->heap[this->used-1] = 0;
			this->used--;
		} else {
			int swapper = v[i];
			cout << "swapper index: " << swapper << endl;
			// not the last element in the heap, also the node has children.
			cout << "HERE:" << v[i]*2+1 << " " << this->used-1 << endl;
			if (v[i]*2+1 <= this->used-1) {
				int leftChildIndex = v[i]*2+1, rightChildIndex = v[i]*2+2;
				cout << this->heap[leftChildIndex] << this->heap[rightChildIndex] << endl;
				cout << rightChildIndex << " " << this->used << endl;
				if (rightChildIndex >= this->used) {
					cout << "swapper is leftChildIndex!" << endl;
					swapper = leftChildIndex;
				} else {
					if (this->heap[leftChildIndex] > this->heap[rightChildIndex]) swapper = leftChildIndex;
					else swapper = rightChildIndex;
					//swapper = max(this->heap[leftChildIndex], this->heap[rightChildIndex]);
					cout << "swapper is max of two!" << endl;
				}
				cout << "SWAPPER index: " << swapper << endl;
				swap(&this->heap[swapper], &this->heap[v[i]]);
			}
			for (int i = swapper+1; i < this->used; i++) {
				this->heap[i-1] = this->heap[i];
				rebalance(i-1);
			}
			this->heap[this->used-1] = 0;
			this->used--;
		}
		this->full = false;	
	}
}

// searches the heap for a particular number, and pushes the occurring indexes onto a vector.
vector<int> MaxBinaryHeap::search(int num) {
	vector<int> v;
	for (int i = 0; i < this->used; i++) if (this->heap[i] == num) v.push_back(i);
	return v;
}

// returns the max value of the heap
int MaxBinaryHeap::findMax() {
	return this->heap[0];
}

// pops the max value off the heap and reconfigures the heap.
void MaxBinaryHeap::pop() {
	int max = this->heap[0];
	this->del(this->findMax());
}

// returns a sorted array of the items in the heap.
int* MaxBinaryHeap::sort() {
	int* sorted_list = (int*)malloc(sizeof(int)*this->used);
	while (this->used > 0) {
		*sorted_list = this->findMax();
		this->pop();
		sorted_list++;
	}
	return sorted_list;
}

void MaxBinaryHeap::printHeap() {
	for (int i = 0; i < this->used; i++) cout << this->heap[i] << " ";
	cout << endl;
}







