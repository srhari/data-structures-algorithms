#include <stdio.h>

typedef struct {
	int* head; //pointer to head of the buffer
	int* queue; //pointer to another storage location to store order of insertion
} CircularBuffer;

CircularBuffer* createBuffer(CircularBuffer*, int);
void push(CircularBuffer*, int);
void pop(CircularBuffer*);
void multipop(CircularBuffer*, int);
void get_head(CircularBuffer*);
void print(CircularBuffer*);

int main() {
	
}

