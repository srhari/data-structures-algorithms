#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	int* array;
	size_t used;
	size_t size;
} DynamicArray;

DynamicArray* dynamicArray(DynamicArray* da, size_t size);
void push(DynamicArray* da, int a);
void pull(DynamicArray* da, int a);
int get(DynamicArray* da, int a);
void freeDA(DynamicArray* da);
void printDA(DynamicArray* da);

int main(int argc, char *argv[]) {
	DynamicArray a; //create new struct DynamicArray
	DynamicArray* da_ptr = dynamicArray(&a, 5); //create DynamicArray pointer to point to address of struct
	push(da_ptr, 1);
	printf("%zu %zu\n", da_ptr->used, da_ptr->size);
	//printDA(da_ptr);
	push(da_ptr, 2);
	push(da_ptr, 3);
	push(da_ptr, 4);
	push(da_ptr, 5);
	printDA(da_ptr);
}

DynamicArray* dynamicArray(DynamicArray* da, size_t size) {
	da->array = (int*)malloc(size * (sizeof(int)));
	printf("da->array points to: %p\n", (da->array));
	da->size = size;
	da->used = 0;
	return da;
}

void push(DynamicArray* da, int a) {
	printf("%zu %zu\n", da->used, da->size);
	if (da->used == (da->size-1)) {
		da->array = (int*)realloc(da->array, da->size*2);
		da->size *= 2;
	}
	da->array[da->used++] = a;
	//da.used++;
	printf("%zu %zu\n", da->used, da->size);
	//printf("da.array points to: %p\n", (da.array));
}

void pull(DynamicArray* da, int a) {
	void* ptr = da->array+a;
	if (a >= da->used) {
		perror("NULL\n");
	} else if (a >= da->size) {
		perror("ERROR -- delete index out of bounds\n");
	} else {
		//(int*)ptr+=a;
		ptr = memmove(ptr, (void*)da->array+a+1, (da->size-1) - a);
		da->used--;
	}
}

int get(DynamicArray* da, int a) {
	if (a >= da->used) {
		perror("NULL\n");
		return -66;
	} else if (a >= da->size) {
		perror("ERROR -- delete index out of bounds\n");
		return -66;
	} else {
		return *(da->array+a);
	}
}

void freeDA(DynamicArray* da) {
	free(da->array);
	da->array = NULL;
	da->size = da->used = 0;
}

void printDA(DynamicArray* da) {
	printf("%zu %zu\n", da->used, da->size);
	for (int i = 0; i < da->used; i++) {
		printf("%d ", da->array[i]);
	}
	printf("\n");
}





