#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NULL_CODE -50000

typedef struct {
	int** root; //top level of the tree
	int** used_dir; //top level of used
	int* used; //pointer to final location used
	size_t length; //length of the top level directory
	size_t leaf_depth; //depth of the leaves
	
} HashedArrayTree;

HashedArrayTree* createTree(HashedArrayTree*, int);
void insert(HashedArrayTree*, int);
void delete(HashedArrayTree*, int, int);
int get(HashedArrayTree*, int, int);
void printTree(HashedArrayTree*);
void freeTree(HashedArrayTree*);

int main() {
	HashedArrayTree ht; //initialize the struct
}

HashedArrayTree* createTree(HashedArrayTree* ht, int length) {
	ht->root = malloc(sizeof(int*)*length); //allocate space for (length) int*s
	ht->length = length;
	ht->leaf_depth = length;
	ht->used = NULL;
	int** temp = ht->root; //set another pointer to point to root
	for (int i = 0; i < length; i++) { //iterate through length of root, and allocate memory at each slot
		*temp = malloc(sizeof(int)*length);
		if (ht->used == NULL) {
			ht->used_dir = temp;
			ht->used = *temp;
		} 
		temp++;
	}
	return ht;
}

void freeTree(HashedArrayTree* ht) {
	for (int i = 0; i < ht->length; i++) free(ht->root[i]);
	free(ht->root);
}

void printTree(HashedArrayTree* ht) {
	int** temp = ht->root; //set another pointer to point to root
	for (int i = 0; i < ht->length; i++, temp++) {
		for (int j = 0; j < ht->leaf_depth; j++, (*temp)++) {
			printf("%d ", **temp);
		}
		printf("\n");
	}
}

int get(HashedArrayTree* ht, int a, int b) {
	if (a > ht->length || b > ht->leaf_depth) printf("ERROR -- out of bounds index for tree\n");
	else {
		return ht->root[a][b];
	}
	return NULL_CODE;
}

void insert(HashedArrayTree* ht, int a) {
	int* ptr = &ht->root[ht->length-1][ht->leaf_depth-1]; //points to last element of the tree
	if (&ht->used == &ptr) { //if used points to same location, allocate new memory and insert item
		for (int i = 0; i < ht->length; i++) ht->root[i] = realloc(ht->root[i], sizeof(int)*(ht->leaf_depth+1));
		ht->root[0][ht->leaf_depth] = a;
		ht->used = &ht->root[0][ht->leaf_depth];
		ht->leaf_depth++;
	} else { //there must be a free location -> insert
		*ht->used = a;
		ht->used = ht->used_dir[ht->leaf_depth];
	}
}

void delete(HashedArrayTree* ht, int a, int b) {
	if ((a > ht->length-1) || b > (ht->leaf_depth-1)) printf("ERROR -- out of bounds index for tree\n");
	else if (a == (ht->length-1) && b == (ht->leaf_depth-1)){
		ht->root[a][b] = *(int*)(memset(&ht->root[a][b], 0, 4));
	} else {
		int c = a+1, d = a;
		for (int* x = &ht->root[a][b], *y = &ht->root[c][b]; y != &ht->root[ht->length-1][ht->leaf_depth-1]+1; x = &ht->root[d++][b], y = &ht->root[c++][b]) {
			if (d == ht->length-1) {
				d = 0;
				b++;
			}
			if (c == ht->length-1) {
				c = 0;
				b++;
			}
			*x = *y;
		}
			
			
	}
}


