#include <stdio.h>
#include <stdlib.h>

int* array_gen(int, int);
void insert(int*, int[], int[], int[], int, int, int);
void delete(int*, int[], int[], int[], int, int);

int m = 10, n = 10, i = 0;

int main() {
	int* sparse_matrix = array_gen(m, n); //create the sparse matrix
	int row_table[m*n]; //initialize memory to store row locations, column locations, and data
	int column_table[m*n];
	int value_table[m*n];
	printf("First element of the sparse matrix: %d\n", *sparse_matrix);
}

int* array_gen(int m, int n) {
	int* sparse_matrix = calloc(m * n, sizeof(int*)); //allocate memory for the array
	return sparse_matrix;
}

void insert(int* sparse_matrix, int row_table[], int column_table[], int value_table[], int item, int row, int column) {
	int slot = row * column; //find the correct slot in memory
	int* temp = sparse_matrix + (row * column); //appoint pointer to slot
	*temp = item; //set value
	row_table[i++] = row; //update row and column tables
	column_table[i++] = column;
	value_table[i++] = item;
	printf("New item %d inserted at row %d column %d in sparse matrix %p.\n", item, row, column, sparse_matrix);
}

void delete(int* sparse_matrix, int row_table[], int column_table[], int value_table[], int row, int column) {
	if ((row > (sizeof(sparse_matrix)) / n) || (column > (sizeof(sparse_matrix)) / m)) {
		printf("This is out of the bounds of the matrix.\n");
		return;
	} else {
		int slot = row * column;
		int* temp = sparse_matrix + (row * column);
		*temp = 0;
		i--;
		for(int j = 0; j < sizeof(row_table); j++) {
			if ((row_table[j] == row) && (column_table[j] == column)) {
				int* nullptr = NULL;
				row_table[j] = *nullptr;
				column_table[j] = *nullptr;
				value_table[j] = *nullptr;
			}
		}
	}
}

int read(int* sparse_matrix, int row_table[], int column_table[], int value_table[], int row, int column) {
	for (int k = 0; k < sizeof(row_table); k++) {
		if ((row_table[k] == row) && (column_table[k] == column)) {
			return value_table[k];
		}
	}
	return 0;
}

