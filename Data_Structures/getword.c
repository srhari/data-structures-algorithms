#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXWORD 100
#define IN 1
#define OUT 0

int getword(char *, int);
int getch(void);
void ungetch(int);

int main() {
	char word[MAXWORD];
	/*word[0] = 'g';
	word[1] = 'h';
	char *w = word;
	*w++;
	*w++;
	printf("Value pointed to by pointer *w: %c\n", *w);
	printf("Address of the pointer itself: %p\n", &w);
	printf("Address pointed to by pointer *w: %p\n", w);
	*/
	while (getword(word, MAXWORD) != EOF){ //get next word from program input, until EOF is reached
		printf("%s ", word);
	}
		
}

int getword(char *word, int lim){
	int c, d, s_comment, m_comment, string, directive;
	char *w = word; //char pointer to first element of word array
	
	s_comment = m_comment = string = directive = OUT;
	
	while (isspace(c = getch())) ; //skip leading spaces
	
	if (c == '/'){
		printf("FORWARD SLASH\n");
		d = getch();
		if (d == '*'){
			printf("MULTILINE COMMENT DETECTED\n");
			m_comment = IN;
		}
		else {
			m_comment = OUT;
			ungetch(d);
		}
		
		if (d == '/'){
			printf("SINGLE LINE COMMENT DETECTED\n");
			s_comment = IN;
		}
		else {
			s_comment = OUT;
			ungetch(d);
		}
	}
	
	if (c == '\"') string = IN; //set string to IN '\"'
	
	if (c == '#') directive = IN; //set directive to IN
	
	if (c == '\\') c = getch(); //ignore the \\ character
	
	if (s_comment == OUT && m_comment == OUT && string == OUT && directive == OUT){
		if (c != EOF) *w++ = c; //add new character to word array
		if (!isalnum(c) && c != '_'){ //non alpha-numeric char detected, word finished. exit
			*w = '\0';
			return c;
		}
		for ( ; --lim > 0; w++){ //load up the word into array. 
			*w = getch();
			if (!isalnum(*w) && *w != '_'){ //check for reasonable characters. 
				ungetch(*w);
				break;
			}
		}
		*w = '\0';
		return word[0];
	}else if (m_comment == IN){
		//*w++ = c;
		//*w++ = d;
		c = getch();
		d = getch();
		while (/**w++ = */(c != '*' && d != '/')){
			if ((c = getch()) == '*'){
				if ((d = getch()) == '/'){
					//*w++ = c;
					m_comment = OUT;
					//break;
				}else{
					ungetch(d);
				}
			}else ;
		}
		*w = '\0';
	}else if (s_comment == IN){
		//c = getch();
		while ((c = getch()) != '\n') ;
		s_comment = OUT;
	}else if (string == IN){
		printf("STRING");
		//*w++ = c;
		while ((c = getch()) != '\"'){ // '\"'
			if (c == '\\') c = getch();
		}
		string = OUT;
		*w = '\0';
	}else if (directive == IN){
		printf("DIRECTIVE");
		//*w++ = c;
		while ((c = getch()) != '\n'){
			if (c == '\\') c = getch();
		}
		directive = OUT;
		*w = '\0';
	}
	
	if (string == OUT) printf("Not in string...\n");
	
	return c;
}

#define BUFSIZE 100

char buf[BUFSIZE]; // buffer for ungetch
int bufp = 0; //next free position in buffer

/* get a character that can possible be pushed back */
int getch(void){
	return (bufp > 0) ? buf[bufp--] : getchar();
}

/* push character back onto input */
void ungetch(int c){
	if (bufp >= BUFSIZE)
		printf("ERROR: BUFFER OVERFLOW");
	else
		buf[bufp++] = c;
} 