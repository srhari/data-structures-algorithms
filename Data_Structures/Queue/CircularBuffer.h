#include "../Node/Node.h"
#include <cstdlib>

using namespace std;

#ifndef CircularBuffer_H
#define CircularBuffer_H

class CircularBuffer {
	
	public: 
		
		CircularBuffer(size_t size) {
			int* buffer = (int*)malloc(sizeof(int)*size); this->buffer = buffer;
			this->size = size; this->items = 0;
			this->front = NULL; this->rear = NULL;
		}
		
		// pushes an item to the back of the buffer
		bool enqueue(int);
		
		// pops an item off the front of the buffer
		bool dequeue();
		
		// is the queue full
		bool isFull();
		
		// is the queue empty
		bool isEmpty();
		
		// get the front and rear of the buffer
		int getFront();
		int getRear();
		
		// print the buffer contents
		void printBuffer() {
			if (this->front == NULL && this->rear == NULL) cout << "FRONT: NULL, REAR: NULL" << endl;
			else {
				cout << "FRONT: " << *this->front << " REAR: " << *this->rear << endl;
				cout << "# ITEMS: " << this->items << endl;
				int* a = this->front;
				for (int i = 0; i < this->items; i++) {
					cout << *a << " ";
					if (a == this->buffer+(this->size-1)) a = this->buffer;
					else a++;
				}
				cout << endl;
			}
		}
				
		~CircularBuffer();
		
	private:
		
		int* buffer;
		int* front;
		int* rear;
		size_t size;
		size_t items;
};

#endif