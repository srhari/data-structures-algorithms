#include "Queue.h"

using namespace std;

// construct the queue
Queue::Queue(size_t size) {
	this->size = size;
	this->items = 0;
}

// destroy the queue
Queue::~Queue() {
	
}

// push an item to the back of the queue
void enqueue(Node* n) {
	if (this->isFull()) cout << "Error inserting item: Queue overflow" << endl;
	else {
		if (this->isEmpty()) {
			this->front = n;
			this->rear = n;
		} else { 
			this->rear->setNextNode(n);
			this->rear = n;
		}
	}
}

// pop an item off the rear of the queue
Node* dequeue() {
	if (this->isEmpty()) {
		cout << "Error popping item: Queue empty" << endl;
		return NULL;
	} else {
		Node* item = this->front;
		this->front = this->front->getNextNode();
		item->setNextNode(NULL);
		return item;
	}
}

// peeks at the front of the queue
Node* peek() {
	if (this->isEmpty()) {
		cout << "Error popping item: Queue empty" << endl;
		return NULL;
	} else return this->front;
}

bool isFull() {
	if (items == this->size) return true;
	return false;
}

bool isEmpty() {
	if (items == 0) return true;
	return false;
}