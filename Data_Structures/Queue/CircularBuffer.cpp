#include "CircularBuffer.h"

using namespace std;

// cbuffer destructor
CircularBuffer::~CircularBuffer() {}

// pushes an item to the back of the buffer
bool CircularBuffer::enqueue(int item) {
	if (this->isFull()) { cout << "Queue is full" << endl; return false; }
	if (this->isEmpty()) { this->rear = buffer; this->front = buffer; *this->front = item; this->items++; return true; }
	else { this->rear++; *this->rear = item; this->items++; }
	return true;
}

// pops an item off the front of the buffer
bool CircularBuffer::dequeue() {
	if (this->isEmpty()) { cout << "Queue is empty. Nothing to dequeue." << endl; return false; }
	int value = *this->front;
	if (this->items == 1) { this->front = NULL; this->rear = NULL; this->items--; return true; }
	this->items--; this->front++;
	this->front = this->buffer + (this->front-this->buffer)%this->size;
	return true;
}

// is the queue full
bool CircularBuffer::isFull() {
	return (((this->rear-this->buffer)+1)%this->size == this->front-this->buffer) ? true : false;
}

// is the queue empty
bool CircularBuffer::isEmpty() {
	return !this->front ? true : false;
}

// get the front and rear of the buffer
int CircularBuffer::getFront() {
	return this->front ? *this->front : 0;
}

int CircularBuffer::getRear() {
	return this->rear ? *this->rear : 0;
}

