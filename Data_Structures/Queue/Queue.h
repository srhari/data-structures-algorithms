#include "../Node/Node.h"

using namespace std;

#ifndef Queue_H
#define Queue_H

class Queue {
	
	public: 
		
		// pushes an item to the back of the queue
		void enqueue(Node*);
		
		// pops an item off the front of the queue
		Node* dequeue();
		
		// returns the item at the front of the queue but does not
		// remove it from the queue
		Node* peek();
		
		// is the queue full
		bool isFull();
		
		// is the queue empty
		bool isEmpty();
		
		Queue();
		
		~Queue();
		
	private:
		
		Node* front;
		Node* rear;
		size_t size;
		size_t items;
	
	
};


