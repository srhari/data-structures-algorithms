#include "../Node/DoublyLinkedNode.h"
#include "DoublyLinkedList.h"

//DoublyLinkedNode* head;

// add a node to the start of the list
void DoublyLinkedList::addFirst(DoublyLinkedNode* firstNode) {
	if (this->head == NULL) {
		this->head = firstNode; //if the list is empty, add this node as the head
	} else {
		firstNode->setNextNode(this->head); //list is not empty --> add pointer to current head as next node
		this->head->setPreviousNode(firstNode); // set previous node of head
		this->head = firstNode; //set the new head
	}
}

//adds a node to the end of the list
void DoublyLinkedList::addLast(DoublyLinkedNode* lastNode) {
	if (this->head == NULL) this->head = lastNode; //if the list is empty, add this node as the head
	else {
		DoublyLinkedNode* n = this->head;
		for (; n->next != NULL; n = n->next) ;
		n->setNextNode(lastNode);
		lastNode->setPreviousNode(n);
	}
}

//find and return the first node with the specified data
DoublyLinkedNode* DoublyLinkedList::find(int data) {
	for (DoublyLinkedNode* n = this->head; n != NULL; n = n->next) {
		if (n->data == data) return n;
	}
	return NULL;
}


//find and return all nodes with the specified data as a linked list.
DoublyLinkedNode* DoublyLinkedList::findAll(int data) {
	DoublyLinkedList lst;
	for (DoublyLinkedNode* n = this->head; n != NULL; n = n->next) {
		if (n->data == data) {
			if (lst.head == NULL) lst.head = n;
			else lst.addLast(n);
		}
	}
	return lst.head;
}


// finds a distinct piece of data in the list and inserts a node right after that node.
// if there are multiple nodes that have the same data, the new node will be inserted after the first match only.
void DoublyLinkedList::insert_after(int data, DoublyLinkedNode* guest) {
	if (this->head == NULL) printf("This linked list is empty.\n");
	DoublyLinkedNode* n = find(data);
	if (n != NULL) {
		if (n->getNextNode() == NULL) {
			n->setNextNode(guest);
			guest->setPreviousNode(n);
		} else {
			guest->setNextNode(n->getNextNode());
			n->getNextNode()->setPreviousNode(guest);
			n->setNextNode(guest);
			guest->setPreviousNode(n);
		}
	} else {
		printf("No nodes found with the specified data.\n");
	}
}

//same as insert after, except the new node is inserted prior to the found node.
void DoublyLinkedList::insert_before(int data, DoublyLinkedNode* guest) {
	if (this->head == NULL) printf("This linked list is empty.\n");
	else if (this->head->getNextNode() == NULL) {
		if (this->head->data == data) addFirst(guest);
		else printf("No nodes found with the specified data.\n");
	} else {
		DoublyLinkedNode* n;
		for (n = this->head; n != NULL; n = n->next) {
			if (n->data == data) {
				DoublyLinkedNode* p = n->getPreviousNode();
				p->setNextNode(guest);
				guest->setPreviousNode(p);
				guest->setNextNode(n);
				n->setPreviousNode(guest);
			}
		}	
		if (n == NULL) printf("No nodes found with the specified data.\n");
	}
}


// finds the node with the specified data and destroys it, then reconfigures the linked list.
void DoublyLinkedList::deleteNode(int data) {
	if (this->head == NULL) printf("This linked list is empty.\n");
	DoublyLinkedNode* n = this->head;
	if (n->getValue() == data) {
		this->head = n->getNextNode();
		n->setNextNode(NULL);
		n->getNextNode()->setPreviousNode(NULL);
		//delete(n);
		return;
	}
	for (; n != NULL; n = n->next) {
		if (n->getValue() == data) {
			if (n->getPreviousNode() != NULL) {
				n->getPreviousNode()->setNextNode(n->getNextNode());
			}
			
			if (n->getNextNode() != NULL) {
				n->getNextNode()->setPreviousNode(n->getPreviousNode());
			}
			n->setPreviousNode(NULL);
			n->setNextNode(NULL);
			return;
		}
	}
	
	if (n == NULL) printf("No nodes found with the specified data.\n");
}


// finds all nodes with the specified data and destroys them, then reconfigures the linked list.
void DoublyLinkedList::deleteAll(int data) {
	if (this->head == NULL) printf("This linked list is empty.\n");
	DoublyLinkedNode* n = this->head;
	if (n->getValue() == data) {
		this->head = n->getNextNode();
		n->setNextNode(NULL);
		n->getNextNode()->setPreviousNode(NULL);
		//delete(n);
	}
	for (; n != NULL; n = n->next) {
		if (n->getValue() == data) {
			n->getPreviousNode()->setNextNode(n->getNextNode());
			n->getNextNode()->setPreviousNode(n->getPreviousNode());
			n->setNextNode(NULL);
			n->setPreviousNode(NULL);
		}
	}
	
	if (n == NULL) printf("No nodes found with the specified data.\n");
}

void DoublyLinkedList::printList() {
	if (this->head == NULL) printf("NULL\n");
	else {
		for (DoublyLinkedNode* n = this->head; n != NULL; n = n->next) {
			printf("%d-->", n->data);
		}
		printf("NULL\n");
	}
}


DoublyLinkedList::DoublyLinkedList() {
	this->head = NULL;
}

//linked list constructor: set head of this list to the specified node
DoublyLinkedList::DoublyLinkedList(DoublyLinkedNode* head) {
	this->head = head;
}

//linked list destructor
DoublyLinkedList::~DoublyLinkedList() {
	
}