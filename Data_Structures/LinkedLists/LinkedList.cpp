#include "../Node/Node.h"
#include "LinkedList.h"

using namespace std;
Node* head;

//add a node to the start of the list
void LinkedList::addFirst(Node* firstNode) {
	if (this->head == NULL) this->head = firstNode; //if the list is empty, add this node as the head
	else {
		firstNode->next = this->head; //list is not empty --> add pointer to current head
		this->head = firstNode; //set the new head
	}
}

//adds a node to the end of the list
void LinkedList::addLast(Node* lastNode) {
	if (this->head == NULL) this->head = lastNode; //if the list is empty, add this node as the head
	else {
		Node* n = this->head;
		for (; n->next != NULL; n = n->next) ;
		n->next = lastNode;
	}
}

//find and return the first node with the specified data
Node* LinkedList::find(int data) {
	for (Node* n = this->head; n != NULL; n = n->next) {
		if (n->data == data) return n;
	}
	return NULL;
}

//find and return all nodes with the specified data as a linked list.
LinkedList LinkedList::findAll(int data) {
	LinkedList lst;
	for (Node* n = this->head; n != NULL; n = n->next) {
		printf("Address of node in list: %p\n", n);
		if (n->data == data) {
			Node* c = n->copyNode();
			lst.addLast(c);
			printf("Match found! Address of node: %p, address of copy: %p\n", n, c);
			//lst.addLast(c);
		}
	}
	return lst;
}

// finds a distinct piece of data in the list and inserts a node right after that node.
// if there are multiple nodes that have the same data, the new node will be inserted after the first match only.
void LinkedList::insert_after(int data, Node* guest) {
	if (this->head == NULL) printf("This linked list is empty.\n");
	else {
		Node* n = find(data);
		if (n != NULL) {
			if (n->next == NULL) n->next = guest;
			else {
				Node* p = n->next;
				n->next = guest;
				guest->next = p;
			}
		} else {
			printf("No nodes found with the specified data.\n");
		}
	}
}

//same as insert after, except the new node is inserted prior to the found node.
void LinkedList::insert_before(int data, Node* guest) {
	if (this->head == NULL) printf("This linked list is empty.\n");
	else if (this->head->data == data) {
		guest->next = this->head;
		this->head = guest;
	} else if (this->head->next == NULL) {
		if (this->head->data == data) this->addFirst(guest);
		else printf("No nodes found with the specified data.\n");
	} else {
		Node* n = this->head;
		for (; n->next != NULL; n = n->next) {
			if (n->next->data == data) {
				Node* p = n->next;
				n->next = guest;
				guest->next = p;
				return;
			}
		}
		
		if (n->next == NULL) printf("No nodes found with the specified data.\n");
	}
}

// finds the node with the specified data and destroys it, then reconfigures the linked list.
void LinkedList::deleteNode(int data) {
	printf("DELETE\n");
	if (this->head == NULL) printf("This linked list is empty.\n");
	else {
		Node* n = this->head;
		//Node* n = this->head;
		if (n->data == data) {
			printf("node to delete found! .. \n");
			this->head = n->next;
			//delete(n);
			return;
		}
		for (; n != NULL; n = n->next) {
			if (n->next->data == data) {
				Node* p = n->next;
				n->next = n->next->next;
				//delete(p);
				return;
			} else if (n->next == NULL) {
				n = NULL;
				return;
			}
		}
	}
	
}

// finds all nodes with the specified data and destroys them, then reconfigures the linked list.
Node* LinkedList::deleteAll(int data) {
	while(this->head && this->head->data==data) this->head=this->head->next;
	if(this->head == NULL) return NULL;
	this->head->next = this->deleteAll(data);        
	return head;
}

void LinkedList::printList() {
	if (this->head == NULL) printf("NULL\n");
	else {
		for (Node* n = this->head; n != NULL; n = n->next) {
			printf("%d-->", n->data);
		}
		printf("NULL\n");
	}
}

void LinkedList::printAddresses() {
	if (this->head == NULL) printf("NULL\n");
	else {
		for (Node* n = this->head; n != NULL; n = n->next) {
			printf("%p-->", n);
		}
		printf("NULL\n");
	}
}


void LinkedList::setCycle(Node* a, Node* b) {
	Node* n = this->head, *m = this->head;
	for (; n != a && n != NULL; n = n->next) ;
	if (n == NULL) {
		printf("Node (a) not found in this linked list.\n");
		return;
	}
	for (; m != b && m != NULL; m = m->next) ;
	if (m == NULL) {
		printf("Node (b) not found in this linked list.\n");
		return;
	}
	n->next = m;
}

bool LinkedList::hasCycle() {
	if (this->head == NULL || this->head->next == NULL) return false;
	else {
		Node* slow = this->head;
		Node* fast = this->head->next;
		while (fast != NULL && fast->next != NULL) {
			if (fast == slow) return true;
			fast = fast->next->next;
			slow = slow->next;
		}
		return false;
	}
}

Node* LinkedList::detectCycle() {
	if (this->head == NULL || this->head->next == NULL) return NULL;
    else {
	    Node* slow = this->head;
	    Node* fast = this->head->next;
	    while (fast != NULL && fast->next != NULL) {
			if (fast == slow) {
				slow = head;
				while (fast->next != slow) {
					slow = slow->next;
					fast = fast->next;
				}
				return slow;
			}
		    fast = fast->next->next;
		    slow = slow->next;
	    }
		return NULL;
    }
}

Node* LinkedList::getIntersection(Node* headA, Node* headB) {
	// case 1: both heads are the same node
	if (headA == headB) return headA;
	
	// case 2: NULL list(s), or both lists have separate heads and one node each
	if (headA == NULL || headB == NULL || (headA->next == NULL && headB->next == NULL)) return NULL;
	
	// case 3: the non-NULL lists intersect at some node
	int l1 = 0, l2 = 0;
	Node* p1 = headA, *p2 = headB;

	while (p1 != NULL) { // length of list 1
		l1++;
		p1 = p1->next;
	}
	while (p2 != NULL) { // length of list 2
		l2++;
		p2 = p2->next;
	}

	p1 = headA, p2 = headB;
	while (l1 > l2) {
		p1 = p1->next;
		l1--;
	}

	while (l2 > l1) {
		p2 = p2->next;
		l2--;
	}

	for (; p1 != NULL && p2 != NULL; p1 = p1->next, p2 = p2->next) if (p1 == p2) return p1;
	return NULL;}

Node* LinkedList::findNodeFront(int index) {
	if (this->head == NULL) return NULL;
	Node* n = this->head; int i;
	for (i = 1; n != NULL && i < index; n = n->next, i++) printf("%p-->", n);
	if (n == NULL) return NULL;
	else return n;
}

Node* LinkedList::findNodeRear(int index) {
	if (this->head == NULL) return NULL;
	int i = index; Node* first = this->head, *second = this->head;
	while (i != 0) {
		if (second == NULL) return NULL;
		second = second->next;
		i--;
	}
	for (; second != NULL; first = first->next, second = second->next) ;
	return first;
}

int LinkedList::getLength() {
	if (this->head == NULL) return 0;
	else {
		int count = 0;
		for (Node* n = this->head; n != NULL; n = n->next, count++) ;
		return count;
	}
}

Node* LinkedList::reverseList(Node* head) {
	if (head == NULL || head->next == NULL) {
		return head;
	}
	// Definition for head on sub list without the current one.
	Node *subHead = head->next;
	// Get result of sub list
	Node *res = reverseList(subHead);
	// The old Head is tail of new list. 
	head->next = NULL;
	subHead->next = head;
	return res;
}

Node* LinkedList::odd_even() {
	// case: NULL list
	if (!this->head) return NULL;
	
	// case: head only list
    if (!this->head->next) return head;
	
	// case: list length = 2
	if (!this->head->next->next) return head;
	
	// case: list length > 2. re-arrangement of nodes required.
    Node* n = head, *p = head->next, *q = p;
	while (n->next != NULL) {
		n->next = n->next->next;
		if (p->next != NULL) p->next = p->next->next;
		if (n->next != NULL) n = n->next;
		if (p->next != NULL) p = p->next;
	}
	n->next = q;
	return this->head;
}

bool LinkedList::isPalindrome() {
	if (this->head == NULL) return false;
	if (this->head->next == NULL) return true;
	
	// O(n) time, O(1) space??
	// calculate length of the linked list, then determine where to start a reversal. Then iterate to the middle node to start the reversal.
	Node* reverseHead = this->head, *origin = this->head, *clone = origin, *headClone, *firstTail;
	int i, mid, length = this->getLength();
	if (length % 2 == 1) mid = length/2 + 1;
	if (length % 2 == 0) mid = length/2;
	i = mid;
	printf("Mid: %d\n", mid);
	while (i > 0) {
		if (i == 1) firstTail = this->head;
		this->head = this->head->next;
		i--;
	}
	
	// reverse the second half of the list
	Node* newHead = this->reverseList(this->head);
	headClone = newHead;
	
	while (headClone != NULL) {
		if (clone->data != headClone->data) {
			this->reverseList(this->head);
			this->head = origin;
			return false;
		}
		clone = clone->next;
		headClone = headClone->next;
	}
	this->reverseList(this->head);
	this->head = origin;
	return true;	
}

Node* LinkedList::mergeLists(Node* headOne, Node* headTwo) {
	if (headOne == NULL && headTwo == NULL) return NULL;
	if (headOne == NULL) return headTwo;
	if (headTwo == NULL) return headOne;
	Node* head, *tail;
	if (headOne->data >= headTwo->data) {
		head = headOne;
		headOne = headOne->next;
	} else {
		head = headTwo;
		headTwo = headTwo->next;
	}
	tail = head;
	while (headOne != NULL && headTwo != NULL) {
		if (headOne >= headTwo) {
			tail->next = headTwo;
			tail = headTwo;
			headTwo = headTwo->next;
		} else {
			tail->next = headOne;
			tail = headOne;
			headOne = headOne->next;
		}
	}
	if (headOne == NULL) {
		while (headTwo != NULL) {
			tail->next = headTwo;
			tail = headTwo;
			headTwo = headTwo->next;
		}
		tail->next = NULL;
	} else if (headTwo == NULL) {
		while (headOne != NULL) {
			tail->next = headOne;
			tail = headOne;
			headOne = headOne->next;
		}
		tail->next = NULL;
	}
	return head;
}

Node* LinkedList::sum(Node* headOne, Node* headTwo) {
	if (headOne == NULL && headTwo == NULL) return NULL;
	if (headOne == NULL) return headTwo;
	if (headTwo == NULL) return headOne;
	Node* head = NULL, *tail = NULL;
	int carry = 0, value;
	while (headOne != NULL && headTwo != NULL) {
		value = headOne->data + headTwo->data + carry;
		if (value >= 10) {
			value = value % 10;
			carry = 1;
		} else carry = 0;
		Node a(value);
		if (head == NULL) head = &a;
		if (tail == NULL) tail = head;
		else {
			tail->next = &a;
			tail = &a;
		}
		headOne = headOne->next;
		headTwo = headTwo->next;
	}
	
	if (headOne == NULL) {
		while (headTwo != NULL) {
			value = headTwo->data + carry;
			if (value >= 10) {
				value = value % 10;
				carry = 1;
			} else carry = 0;
			Node a(value);
			tail->next = &a;
			tail = &a;
		}
	} else if (headTwo == NULL) {
		while (headOne != NULL) {
			value = headOne->data + carry;
			if (value >= 10) {
				value = value % 10;
				carry = 1;
			} else carry = 0;
			Node a(value);
			tail->next = &a;
			tail = &a;
		}
	}
	
	if (carry != 0) {
		Node a(carry);
		tail->next = &a;
		tail = &a;
	}
	tail->next = NULL;
	return head;
}

Node* LinkedList::rotate(int r) {
	// bad rotation index or single node list
	if (r <= 0 || !this->head->next) { if (r < 0) printf("Rotation degree must be non-negative.\n"); return this->head; }
	// NULL list
	else if (!this->head) return NULL;
	
	// else, valid rotation is necessary.
	else {
		// first determine what the new head of the list should be by finding the rth node from the rear. 
		// If the list has less than r nodes, this function will loop back to the head.
		int i = r; Node* pivot = this->head, *pivot_lead = this->head;
		cout << "i: " << i << endl;
		while (i > 0) { if (!pivot_lead->next) pivot_lead = this->head; else pivot_lead = pivot_lead->next; i--; }
		
		// if the pivot ends up being the same as the pivot (currently the head), no actual rotation needs to be done as a rotation would result in an identical list.
		if (pivot == pivot_lead) return this->head;
		while (pivot_lead) { pivot_lead = pivot_lead->next; pivot = pivot->next; }
		
		cout << "pivot value: " << pivot->data << endl;
		
		Node* n = pivot, *m = this->head; while (n->next) n = n->next; cout << "n value: " << n->data << endl;
		n->next = this->head;
		while (m->next != pivot) m = m->next;
		m->next = NULL;
		this->head = pivot;
	}
	
	return this->head;
	
}

// removes duplicates from an unsorted linked list
Node* LinkedList::removeDuplicates() {
	
	// case 1: NULL list or length 1: duplicates impossible
	if (!head) return NULL;
	if (!head->next) return head;
	
	Node* n = this->head, *p = this->head, *q; HashTable ht;
	while (n) {
		if (ht.lookup(to_string(n->data)) == "Not found.") {
			ht.insert(to_string(n->data));
			p->data = n->data;
			q = p;
			p = p->next;
		}
		n = n->next;
	}
	q->next = NULL;
	return this->head;
}

// deletes a single node without having to iterate through the whole list, by replacing values.
Node* LinkedList::deleteSingleNode(Node* n) {
	if (!n || !this->head) return this->head;
	if (n == this->head) { this->head = this->head->next; return this->head; }
	Node* p;
	while (n->next) { n->data = n->next->data; p = n; n = n->next; }
	p->next = NULL;
	return this->head;
}

/*Node* LinkedList::partition(int partition) {
	if (!this->head) return NULL;
	if (!this->head->next) return this->head;
	
}*/

//linked list constructor: set head of this list to the specified node
LinkedList::LinkedList(Node* head) {
	this->head = head;
}

//linked list destructor
LinkedList::~LinkedList() {
	
}