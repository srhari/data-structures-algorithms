#include <iostream>
#include "../Node/Node.h"
#include "LinkedList.h"

#ifndef Doubly_LinkedList_H
#define Doubly_LinkedList_H

class DoublyLinkedList: public LinkedList{
	
	public:
			DoublyLinkedNode* head;
			
			//add a node to the start of the list
			void addFirst(DoublyLinkedNode*);
			
			//find and return the first node with the specified data
			DoublyLinkedNode* find(int);
			
			//find and return all nodes with the specified data as a linked list.
			DoublyLinkedNode* findAll(int);
			
			//adds a node to the end of the list
			void addLast(DoublyLinkedNode*);
			
			// finds a distinct piece of data in the list and inserts a node right after that node.
			// if there are multiple nodes that have the same data, the new node will be inserted after the first match only.
			void insert_after(int, DoublyLinkedNode*);
			
			//same as insert after, except the new node is inserted prior to the found node.
			void insert_before(int, DoublyLinkedNode*);
			
			// finds the node with the specified data and destroys it, then reconfigures the linked list.
			void deleteNode(int);
			
			// finds all nodes with the specified data and destroys them, then reconfigures the linked list.
			void deleteAll(int);
			
			//prints the contents of the linked list
			void printList();
					
			DoublyLinkedList();
			
			//linked list constructor: set head of this list to the specified node
			DoublyLinkedList(DoublyLinkedNode*);
			
			//linked list destructor
			~DoublyLinkedList();

};



#endif