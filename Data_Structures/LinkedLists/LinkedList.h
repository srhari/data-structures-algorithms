#include <iostream>
#include "../Node/Node.h"
#include "../HashTables/HashTable.h"

#ifndef LinkedList_H
#define LinkedList_H

class LinkedList {
	
	public:
			Node* head;
			
			//add a node to the start of the list
			void addFirst(Node*);
			
			//find and return the first node with the specified data
			Node* find(int);
			
			//find and return all nodes with the specified data as a linked list.
			LinkedList findAll(int);
			
			//adds a node to the end of the list
			void addLast(Node*);
			
			// finds a distinct piece of data in the list and inserts a node right after that node.
			// if there are multiple nodes that have the same data, the new node will be inserted after the first match only.
			void insert_after(int, Node*);
			
			//same as insert after, except the new node is inserted prior to the found node.
			void insert_before(int, Node*);
			
			// finds the node with the specified data and destroys it, then reconfigures the linked list.
			void deleteNode(int);
			
			// finds all nodes with the specified data and destroys them, then reconfigures the linked list.
			Node* deleteAll(int);
			
			//prints the contents of the linked list
			void printList();
			
			// sets a cycle in the linked list, given two nodes in the linked list
			void setCycle(Node*, Node*);
			
			// checks for a cycle in the linked list
			bool hasCycle();
			
			// returns the Node (if any) where the cycle begins
			Node* detectCycle();
			
			// returns the node which begins the intersection of two linked lists, or NULL if there is no intersection
			static Node* getIntersection(Node*, Node*);
			
			// returns ith node from the beginning of the list
			Node* findNodeFront(int);
			
			// returns ith node from the end of the list
			Node* findNodeRear(int);
			
			// returns the length of the linked list
			int getLength();
			
			// prints address locations of every node in the linked list
			void printAddresses();
			
			// reverses this linked list, and returns a pointer to the head of the new list
			Node* reverseList(Node*);
			
			// groups all odd and even nodes separately, and reorders the list. (For example, after reordering, 1st, 3rd, and 5th nodes will be before 2nd, 4th, and 6th nodes). Odd/Even values do not matter. 
			Node* odd_even();
			
			// determine if the linked list is a palindrome
			bool isPalindrome();
			
			// merge two sorted linked lists and return a pointer to the head of the new list.
			static Node* mergeLists(Node*, Node*);
			
			// returns the head of a linked list containing the sum of two linked lists. 
			static Node* sum(Node*, Node*);
			
			// specify an integer to rotate the linked list that many places. 
			Node* rotate(int);
			
			// removes duplicates from an unsorted linked list
			Node* removeDuplicates();
			
			// given access to a single node in a linked list (not the head), delete that node.
			Node* deleteSingleNode(Node* n);
			
			// partitions a linked list around a given value.
			Node* partition(int partition);
					
			LinkedList();
			
			//linked list constructor: set head of this list to the specified node
			LinkedList(Node*);
			
			//linked list destructor
			~LinkedList();

};



#endif